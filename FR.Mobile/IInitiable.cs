﻿namespace FR.Mobile
{
    public interface IInitiable
    {
        bool CanInit { get; }

        void Init();
    }
}