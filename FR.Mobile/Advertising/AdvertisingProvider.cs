﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FR.Mobile.Advertising
{
    public class AdvertisingProvider : IAdvertisingProvider
    {
        private readonly object advertisingProviderContext = new object();

        private readonly Lazy<IAdvertisingRegistry> advertisingRegistry =
            new Lazy<IAdvertisingRegistry>(Container.Resolve<IAdvertisingRegistry>);

        private bool advertisingInProcess;
        private Task advertisingSequenceTask;
        private CancellationTokenSource cancellationTokenSource;

        public void Start()
        {
            lock (advertisingProviderContext)
            {
                if (advertisingInProcess) return;

                cancellationTokenSource = new CancellationTokenSource();
                advertisingSequenceTask = Task.Run(() => AdvertisingSequence(), cancellationTokenSource.Token);

                advertisingInProcess = true;
            }
        }

        public void Stop()
        {
            lock (advertisingProviderContext)
            {
                if (!advertisingInProcess) return;

                cancellationTokenSource.Cancel();
                advertisingSequenceTask.Wait();

                advertisingInProcess = false;
            }
        }

        public event EventHandler<AdvertisingScenario> MoveNext;

        private void AdvertisingSequence()
        {
            var i = 0;
            var advertisingScenarios = advertisingRegistry.Value.GetScenarios().ToArray();

            while (!cancellationTokenSource.IsCancellationRequested)
                try
                {
                    if (++i > advertisingScenarios.Length - 1)
                        i = 0;

                    var currentScenario = advertisingScenarios[i];

                    MoveNext?.Invoke(this, currentScenario);

                    Task.Delay(currentScenario.Duration).Wait(cancellationTokenSource.Token);
                }
                catch (OperationCanceledException)
                {
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }
        }
    }
}