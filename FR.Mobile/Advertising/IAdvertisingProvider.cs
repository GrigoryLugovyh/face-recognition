﻿using System;

namespace FR.Mobile.Advertising
{
    public interface IAdvertisingProvider
    {
        void Start();

        void Stop();

        event EventHandler<AdvertisingScenario> MoveNext;
    }
}