﻿using System.Collections.Generic;
using System.Linq;

namespace FR.Mobile.Advertising
{
    public class AdvertisingRegistry : IAdvertisingRegistry
    {
        public IEnumerable<AdvertisingScenario> GetScenarios()
        {
            return Enumerable.Empty<AdvertisingScenario>();
        }
    }
}