﻿using System;

namespace FR.Mobile.Advertising
{
    public class AdvertisingScenario
    {
        public string FilePath { get; set; }

        public TimeSpan Duration { get; set; }
    }
}