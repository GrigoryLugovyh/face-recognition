﻿using System.Collections.Generic;

namespace FR.Mobile.Advertising
{
    public interface IAdvertisingRegistry
    {
        IEnumerable<AdvertisingScenario> GetScenarios();
    }
}