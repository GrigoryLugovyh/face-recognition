﻿using System;
using System.Linq;
using System.Reflection;

namespace FR.Mobile.Extensions
{
    public static class ExceptionExtension
    {
        public static string Expand(this Exception exception)
        {
            if (exception == null)
                return "null exception";
            var loadException = exception as ReflectionTypeLoadException;

            var innerExceptionText = exception.InnerException == null
                ? string.Empty
                : "\r\nInnerException: " + exception.InnerException.Expand();

            var loaderExceptionText = loadException == null
                ? string.Empty
                : string.Join("\r\n",
                    loadException.LoaderExceptions.Select(
                        exception1 => "\r\nLoaderException: " + exception1.Expand()));

            return
                $"{exception.GetType().Name}: {exception.Message}\r\n{exception.StackTrace}\r\n--------------------------{innerExceptionText}{loaderExceptionText}";
        }
    }
}