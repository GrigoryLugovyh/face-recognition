﻿using System.Collections.Generic;

namespace FR.Mobile.Extensions
{
    public static class CollectionExtension
    {
        public static IEnumerable<T> Yield<T>(this T value)
        {
            yield return value;
        }
    }
}