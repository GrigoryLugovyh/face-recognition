﻿namespace FR.Mobile.Extensions
{
    public static class StringExtensions
    {
        public static double ToDouble(this string input)
        {
            return double.TryParse(input.Replace(".", ","), out var value) ? value : 0.0d;
        }

        public static decimal ToDecimal(this string input)
        {
            return decimal.TryParse(input.Replace(".", ","), out var value) ? value : 0;
        }

        public static bool HasValue(this string input)
        {
            return !string.IsNullOrEmpty(input);
        }
    }
}