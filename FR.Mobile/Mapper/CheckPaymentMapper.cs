﻿using FR.Mobile.Models;

namespace FR.Mobile.Mapper
{
    public class CheckPaymentMapper : Mapper,
        IMapper<string, MCheckPayment>
    {
        public MCheckPayment Map(string value)
        {
            return new MCheckPayment {Payment = value};
        }
    }
}