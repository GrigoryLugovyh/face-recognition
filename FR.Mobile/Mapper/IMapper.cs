﻿namespace FR.Mobile.Mapper
{
    public interface IMapper<in TIn, out TOut>
    {
        TOut Map(TIn value);
    }
}