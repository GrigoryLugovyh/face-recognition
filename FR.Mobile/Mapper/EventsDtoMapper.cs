﻿using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Models;
using FR.Model.Dto;

namespace FR.Mobile.Mapper
{
    public class EventsDtoMapper : Mapper, IMapper<IEnumerable<MEvent>, IEnumerable<EventDto>>
    {
        public IEnumerable<EventDto> Map(IEnumerable<MEvent> value)
        {
            return value.Select(@event => new EventDto
            {
                DeviceTimeStamp = @event.DeviceTimeStamp.ToString("s"),
                SessionId = @event.SessionId,
                DeviceId = @event.DeviceId,
                Message = @event.Message,
                Level = @event.Level,
                Scope = @event.Scope
            });
        }
    }
}