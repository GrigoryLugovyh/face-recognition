﻿using System;
using FR.Mobile.Services;

namespace FR.Mobile.Mapper
{
    public abstract class Mapper
    {
        protected Lazy<IDateTimeService> dateTimeService =
            new Lazy<IDateTimeService>(Container.Resolve<IDateTimeService>);
    }
}