﻿using FR.Mobile.Cash;
using FR.Mobile.Models;
using FR.Model.Dto;

namespace FR.Mobile.Mapper
{
    public class CheckPositionDtoMapper : Mapper, IMapper<MCheckPosition, CheckPositionDto>
    {
        public CheckPositionDto Map(MCheckPosition position)
        {
            return new CheckPositionDto
            {
                Name = position.Name,
                Barcode = position.Barcode,
                Code = position.Code,
                Quantity = position.Quantity,
                Price = position.Price,
                TotalPrice = position.TotalPrice,
                Changed = position.Changed,
                Deleted = position.Deleted,
                AddTimeStamp = position.AddTimeStamp.ToString("s"),
                AddTimeStampUtc = position.AddTimeStampUtc.ToString("s"),
                ChangeTimeStamp = position.ChangeTimeStamp.ToString("s"),
                ChangeTimeStampUtc = position.ChangeTimeStampUtc.ToString("s"),
                DeleteTimeStamp = position.DeleteTimeStamp.ToString("s"),
                DeleteTimeStampUtc = position.DeleteTimeStampUtc.ToString("s")
            };
        }
    }
}