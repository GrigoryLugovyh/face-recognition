﻿using FR.Mobile.Cash;
using FR.Mobile.Models;

namespace FR.Mobile.Mapper
{
    public class CheckPositionMapper : Mapper, IMapper<CheckPosition, MCheckPosition>
    {
        public MCheckPosition Map(CheckPosition value)
        {
            return new MCheckPosition
            {
                Name = value.Name,
                Barcode = value.Barcode,
                Code = value.Code,
                Quantity = value.Quantity,
                Price = value.Price,
                TotalPrice = value.TotalPrice,
                Changed = value.Changed,
                Deleted = value.Deleted,
                AddTimeStamp = value.AddTimeStamp,
                AddTimeStampUtc = value.AddTimeStampUtc,
                ChangeTimeStamp = value.ChangeTimeStamp,
                ChangeTimeStampUtc = value.ChangeTimeStampUtc,
                DeleteTimeStamp = value.DeleteTimeStamp,
                DeleteTimeStampUtc = value.DeleteTimeStampUtc
            };
        }
    }
}