﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Cash;
using FR.Mobile.Models;

namespace FR.Mobile.Mapper
{
    public class CheckMapper : Mapper,
        IMapper<Check, (MCheck, IEnumerable<MCheckPosition>, IEnumerable<MCheckPayment>, IEnumerable<MCheckBonusCard>, IEnumerable<MCheckDiscountCard>)>
    {
        private readonly Lazy<IMapper<string, MCheckBonusCard>> bonuscardMapper =
            new Lazy<IMapper<string, MCheckBonusCard>>(Container.Resolve<IMapper<string, MCheckBonusCard>>);

        private readonly Lazy<IMapper<string, MCheckDiscountCard>> discountcardMapper =
            new Lazy<IMapper<string, MCheckDiscountCard>>(Container.Resolve<IMapper<string, MCheckDiscountCard>>);

        private readonly Lazy<IMapper<string, MCheckPayment>> paymentMapper =
            new Lazy<IMapper<string, MCheckPayment>>(Container.Resolve<IMapper<string, MCheckPayment>>);

        private readonly Lazy<IMapper<CheckPosition, MCheckPosition>> positionMapper =
            new Lazy<IMapper<CheckPosition, MCheckPosition>>(Container.Resolve<IMapper<CheckPosition, MCheckPosition>>);

        public (MCheck, IEnumerable<MCheckPosition>, IEnumerable<MCheckPayment>, IEnumerable<MCheckBonusCard>,
            IEnumerable<MCheckDiscountCard>) Map(Check value)
        {
            var check = new MCheck
            {
                SessionId = value.SessionId,
                PosName = value.PosName,
                CashierName = value.CashierName,
                TotalSum = value.TotalSum,
                TotalDiscount = value.TotalDiscount,
                OpenTimeStamp = value.OpenTimeStamp,
                OpenTimeStampUtc = value.OpenTimeStampUtc,
                CloseTimeStamp = value.CloseTimeStampUtc,
                CloseTimeStampUtc = value.CloseTimeStampUtc,
                DurationSeconds = value.DurationTimeSpan.Seconds
            };

            var positions = value.Positions?.Select(position => positionMapper.Value.Map(position)) ??
                            Enumerable.Empty<MCheckPosition>();

            var payments = value.Payments?.Select(payment => paymentMapper.Value.Map(payment)) ??
                           Enumerable.Empty<MCheckPayment>();

            var bonuscards = value.BonusCards?.Select(bonuscard => bonuscardMapper.Value.Map(bonuscard)) ??
                             Enumerable.Empty<MCheckBonusCard>();

            var discountcards =
                value.DiscountCards?.Select(discountcard => discountcardMapper.Value.Map(discountcard)) ??
                Enumerable.Empty<MCheckDiscountCard>();

            return (check, positions, payments, bonuscards, discountcards);
        }
    }
}