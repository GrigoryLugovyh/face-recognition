﻿using FR.Mobile.Models;

namespace FR.Mobile.Mapper
{
    public class CheckDiscountCardMapper : Mapper, IMapper<string, MCheckDiscountCard>
    {
        public MCheckDiscountCard Map(string value)
        {
            return new MCheckDiscountCard {DiscountCard = value};
        }
    }
}