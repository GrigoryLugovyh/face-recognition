﻿using FR.Mobile.Models;

namespace FR.Mobile.Mapper
{
    public class CheckBonusCardMapper : Mapper, IMapper<string, MCheckBonusCard>
    {
        public MCheckBonusCard Map(string value)
        {
            return new MCheckBonusCard {BonusCard = value};
        }
    }
}