﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Models;
using FR.Model.Dto;

namespace FR.Mobile.Mapper
{
    public class CheckDtoMapper : Mapper,
        IMapper<(MCheck Check, IEnumerable<MCheckPosition> Positions, IEnumerable<MCheckPayment> Payments,
            IEnumerable<MCheckBonusCard> BonusCards, IEnumerable<MCheckDiscountCard> DiscountCards), CheckDto>
    {
        private readonly Lazy<IMapper<MCheckPosition, CheckPositionDto>> positionMapper =
            new Lazy<IMapper<MCheckPosition, CheckPositionDto>>(Container
                .Resolve<IMapper<MCheckPosition, CheckPositionDto>>);

        public CheckDto Map(
            (MCheck Check, IEnumerable<MCheckPosition> Positions, IEnumerable<MCheckPayment> Payments,
                IEnumerable<MCheckBonusCard> BonusCards, IEnumerable<MCheckDiscountCard> DiscountCards) value)
        {
            return new CheckDto
            {
                SessionId = value.Check.SessionId,
                DiscountCards = value.DiscountCards.Select(card => card.DiscountCard).ToArray(),
                BonusCards = value.BonusCards.Select(card => card.BonusCard).ToArray(),
                CashierName = value.Check.CashierName,
                TotalSum = value.Check.TotalSum,
                TotalDiscount = value.Check.TotalDiscount,
                ActivePositionsSum =
                    value.Positions.Where(position => !position.Deleted).Sum(position => position.Price),
                DeletedPositionsSum =
                    value.Positions.Where(position => position.Deleted).Sum(position => position.Price),
                OpenTimeStamp = value.Check.OpenTimeStamp.ToString("s"),
                OpenTimeStampUtc = value.Check.OpenTimeStampUtc.ToString("s"),
                CloseTimeStamp = value.Check.CloseTimeStamp.ToString("s"),
                CloseTimeStampUtc = value.Check.CloseTimeStampUtc.ToString("s"),
                DurationSeconds = value.Check.DurationSeconds,
                Payments = value.Payments.Select(payment => payment.Payment).ToArray(),
                SendTimeStamp = dateTimeService.Value.Now.ToString("s"),
                SendTimeStampUtc = dateTimeService.Value.UtcNow.ToString("s"),
                Positions = value.Positions.Select(position => positionMapper.Value.Map(position)).ToArray()
            };
        }
    }
}