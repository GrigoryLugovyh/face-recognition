﻿namespace FR.Mobile.Infrastructure.Speech
{
    public interface ITextToSpeech : IManage
    {
        void Speak(string text);
    }
}