﻿using System;

namespace FR.Mobile.Infrastructure.Speech
{
    public class ValueWordsBuilder
    {
        public enum TextCase
        {
            Nominative /*Кто? Что?*/,
            Genitive /*Кого? Чего?*/,
            Dative /*Кому? Чему?*/,
            Accusative /*Кого? Что?*/,
            Instrumental /*Кем? Чем?*/,
            Prepositional /*О ком? О чём?*/
        }

        public static class RuDateAndMoneyConverter
        {
            private const string Zero = "ноль";
            private const string FirstMale = "один";

            private static readonly string[] monthNamesGenitive =
            {
                "", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября",
                "ноября", "декабря"
            };

            public static readonly string FirstFemale = "одна";
            public static readonly string FirstFemaleAccusative = "одну";
            public static readonly string FirstMaleGenetive = "одно";
            public static readonly string SecondMale = "два";
            public static readonly string SecondFemale = "две";
            public static readonly string SecondMaleGenetive = "двух";
            public static readonly string SecondFemaleGenetive = "двух";

            private static readonly string[] from3Till19 =
            {
                "", "три", "четыре", "пять", "шесть",
                "семь", "восемь", "девять", "десять", "одиннадцать",
                "двенадцать", "тринадцать", "четырнадцать", "пятнадцать",
                "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"
            };

            private static readonly string[] from3Till19Genetive =
            {
                "", "трех", "четырех", "пяти", "шести",
                "семи", "восеми", "девяти", "десяти", "одиннадцати",
                "двенадцати", "тринадцати", "четырнадцати", "пятнадцати",
                "шестнадцати", "семнадцати", "восемнадцати", "девятнадцати"
            };

            private static readonly string[] tens =
            {
                "", "двадцать", "тридцать", "сорок", "пятьдесят",
                "шестьдесят", "семьдесят", "восемьдесят", "девяносто"
            };

            private static readonly string[] tensGenetive =
            {
                "", "двадцати", "тридцати", "сорока", "пятидесяти",
                "шестидесяти", "семидесяти", "восьмидесяти", "девяноста"
            };

            private static readonly string[] hundreds =
            {
                "", "сто", "двести", "триста", "четыреста",
                "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"
            };

            private static readonly string[] hundredsGenetive =
            {
                "", "ста", "двухсот", "трехсот", "четырехсот",
                "пятисот", "шестисот", "семисот", "восемисот", "девятисот"
            };

            private static readonly string[] thousands =
            {
                "", "тысяча", "тысячи", "тысяч"
            };

            private static readonly string[] thousandsAccusative =
            {
                "", "тысячу", "тысячи", "тысяч"
            };

            private static readonly string[] millions =
            {
                "", "миллион", "миллиона", "миллионов"
            };

            private static readonly string[] billions =
            {
                "", "миллиард", "миллиарда", "миллиардов"
            };

            private static readonly string[] trillions =
            {
                "", "трилион", "трилиона", "триллионов"
            };

            private static readonly string[] rubles =
            {
                "", "рубль", "рубля", "рублей"
            };

            private static readonly string[] copecks =
            {
                "", "копейка", "копейки", "копеек"
            };

            /// <summary>
            ///     «07» января 2004 [+ _year(:года)]
            /// </summary>
            /// <param name="date"></param>
            /// <param name="year"></param>
            /// <returns></returns>
            public static string DateToTextLong(DateTime date, string year)
            {
                return $"«{date.Day:D2}» {MonthName(date.Month, TextCase.Genitive)} {date.Year}" +
                       (year.Length != 0 ? " " : "") + year;
            }

            /// <summary>
            ///     «07» января 2004
            /// </summary>
            /// <param name="date"></param>
            /// <returns></returns>
            public static string DateToTextLong(DateTime date)
            {
                return $"«{date.Day:D2}» {MonthName(date.Month, TextCase.Genitive)} {date.Year}";
            }

            /// <summary>
            ///     II квартал 2004
            /// </summary>
            /// <param name="date"></param>
            /// <returns></returns>
            public static string DateToTextQuarter(DateTime date)
            {
                return NumeralsRoman(DateQuarter(date)) + " квартал " + date.Year;
            }

            /// <summary>
            ///     07.01.2004
            /// </summary>
            /// <param name="date"></param>
            /// <returns></returns>
            public static string DateToTextSimple(DateTime date)
            {
                return $"{date:dd.MM.yyyy}";
            }

            public static int DateQuarter(DateTime date)
            {
                return (date.Month - 1) / 3 + 1;
            }

            private static bool IsPluralGenitive(int digits)
            {
                if (digits >= 5 || digits == 0)
                    return true;

                return false;
            }

            private static bool IsSingularGenitive(int digits)
            {
                if (digits >= 2 && digits <= 4)
                    return true;

                return false;
            }

            private static int LastDigit(long value)
            {
                var amount = value;

                if (amount >= 100)
                    amount = amount % 100;

                if (amount >= 20)
                    amount = amount % 10;

                return (int) amount;
            }

            /// <summary>
            ///     Десять тысяч рублей 67 копеек
            /// </summary>
            /// <param name="amount"></param>
            /// <param name="firstCapital"></param>
            /// <returns></returns>
            public static string CurrencyToTxt(double amount, bool firstCapital)
            {
                //Десять тысяч рублей 67 копеек
                var rublesAmount = (long) Math.Floor(amount);
                var copecksAmount = (long) Math.Round(amount * 100) % 100;
                var lastRublesDigit = LastDigit(rublesAmount);
                var lastCopecksDigit = LastDigit(copecksAmount);

                var s = NumeralsToTxt(rublesAmount, TextCase.Nominative, true, firstCapital) + " ";

                if (IsPluralGenitive(lastRublesDigit))
                    s += rubles[3] + " ";
                else if (IsSingularGenitive(lastRublesDigit))
                    s += rubles[2] + " ";
                else
                    s += rubles[1] + " ";

                s += $"{copecksAmount:00} ";

                if (IsPluralGenitive(lastCopecksDigit))
                    s += copecks[3] + " ";
                else if (IsSingularGenitive(lastCopecksDigit))
                    s += copecks[2] + " ";
                else
                    s += copecks[1] + " ";

                return s.Trim();
            }

            /// <summary>
            ///     10 000 (Десять тысяч) рублей 67 копеек
            /// </summary>
            /// <param name="amount"></param>
            /// <param name="firstCapital"></param>
            /// <returns></returns>
            public static string CurrencyToTxtFull(double amount, bool firstCapital)
            {
                //10 000 (Десять тысяч) рублей 67 копеек
                var rublesAmount = (long) Math.Floor(amount);
                var copecksAmount = (long) Math.Round(amount * 100) % 100;
                var lastRublesDigit = LastDigit(rublesAmount);
                var lastCopecksDigit = LastDigit(copecksAmount);

                var s = $"{rublesAmount:N0} ({NumeralsToTxt(rublesAmount, TextCase.Nominative, true, firstCapital)}) ";

                if (IsPluralGenitive(lastRublesDigit))
                    s += rubles[3] + " ";
                else if (IsSingularGenitive(lastRublesDigit))
                    s += rubles[2] + " ";
                else
                    s += rubles[1] + " ";

                s += $"{copecksAmount:00} ";

                if (IsPluralGenitive(lastCopecksDigit))
                    s += copecks[3] + " ";
                else if (IsSingularGenitive(lastCopecksDigit))
                    s += copecks[2] + " ";
                else
                    s += copecks[1] + " ";

                return s.Trim();
            }

            /// <summary>
            ///     10 000 рублей 67 копеек
            /// </summary>
            /// <param name="amount"></param>
            /// <param name="firstCapital"></param>
            /// <returns></returns>
            public static string CurrencyToTxtShort(double amount, bool firstCapital)
            {
                var rublesAmount = (long) Math.Floor(amount);
                var copecksAmount = (long) Math.Round(amount * 100) % 100;
                var lastRublesDigit = LastDigit(rublesAmount);
                var lastCopecksDigit = LastDigit(copecksAmount);

                var s = $"{rublesAmount:N0} ";

                if (IsPluralGenitive(lastRublesDigit))
                    s += rubles[3] + " ";
                else if (IsSingularGenitive(lastRublesDigit))
                    s += rubles[2] + " ";
                else
                    s += rubles[1] + " ";

                s += $"{copecksAmount:00} ";

                if (IsPluralGenitive(lastCopecksDigit))
                    s += copecks[3] + " ";
                else if (IsSingularGenitive(lastCopecksDigit))
                    s += copecks[2] + " ";
                else
                    s += copecks[1] + " ";

                return s.Trim();
            }

            private static string MakeText(int digits, string[] hundreds, string[] tens, string[] from3Till19,
                string second, string first, string[] power)
            {
                var s = "";

                if (digits >= 100)
                {
                    s += hundreds[digits / 100] + " ";
                    digits = digits % 100;
                }
                if (digits >= 20)
                {
                    s += tens[digits / 10 - 1] + " ";
                    digits = digits % 10;
                }

                if (digits >= 3)
                    s += from3Till19[digits - 2] + " ";
                else if (digits == 2)
                    s += second + " ";
                else if (digits == 1)
                    s += first + " ";

                if (digits != 0 && power.Length > 0)
                {
                    digits = LastDigit(digits);

                    if (IsPluralGenitive(digits))
                        s += power[3] + " ";
                    else if (IsSingularGenitive(digits))
                        s += power[2] + " ";
                    else
                        s += power[1] + " ";
                }

                return s;
            }

            /// <summary>
            ///     реализовано для падежей: именительный (nominative), родительный (Genitive),  винительный (accusative)
            /// </summary>
            /// <param name="sourceNumber"></param>
            /// <param name="_case"></param>
            /// <param name="isMale"></param>
            /// <param name="firstCapital"></param>
            /// <returns></returns>
            public static string NumeralsToTxt(long sourceNumber, TextCase _case, bool isMale, bool firstCapital)
            {
                var s = "";
                var number = sourceNumber;
                int remainder;
                var power = 0;

                if (number >= (long) Math.Pow(10, 15) || number < 0)
                    return "";

                while (number > 0)
                {
                    remainder = (int) (number % 1000);
                    number = number / 1000;

                    switch (power)
                    {
                        case 12:
                            s = MakeText(remainder, hundreds, tens, from3Till19, SecondMale, FirstMale, trillions) + s;
                            break;
                        case 9:
                            s = MakeText(remainder, hundreds, tens, from3Till19, SecondMale, FirstMale, billions) + s;
                            break;
                        case 6:
                            s = MakeText(remainder, hundreds, tens, from3Till19, SecondMale, FirstMale, millions) + s;
                            break;
                        case 3:
                            switch (_case)
                            {
                                case TextCase.Accusative:
                                    s = MakeText(remainder, hundreds, tens, from3Till19, SecondFemale,
                                            FirstFemaleAccusative, thousandsAccusative) + s;
                                    break;
                                default:
                                    s = MakeText(remainder, hundreds, tens, from3Till19, SecondFemale, FirstFemale,
                                            thousands) + s;
                                    break;
                            }
                            break;
                        default:
                            string[] powerArray = { };
                            switch (_case)
                            {
                                case TextCase.Genitive:
                                    s = MakeText(remainder, hundredsGenetive, tensGenetive, from3Till19Genetive,
                                            isMale ? SecondMaleGenetive : SecondFemaleGenetive,
                                            isMale ? FirstMaleGenetive : FirstFemale, powerArray) + s;
                                    break;
                                case TextCase.Accusative:
                                    s = MakeText(remainder, hundreds, tens, from3Till19,
                                            isMale ? SecondMale : SecondFemale,
                                            isMale ? FirstMale : FirstFemaleAccusative, powerArray) + s;
                                    break;
                                default:
                                    s = MakeText(remainder, hundreds, tens, from3Till19,
                                            isMale ? SecondMale : SecondFemale, isMale ? FirstMale : FirstFemale,
                                            powerArray) + s;
                                    break;
                            }
                            break;
                    }

                    power += 3;
                }

                if (sourceNumber == 0)
                    s = Zero + " ";

                if (s != "" && firstCapital)
                    s = s.Substring(0, 1).ToUpper() + s.Substring(1);

                return s.Trim();
            }

            public static string NumeralsDoubleToTxt(double sourceNumber, int @decimal, TextCase _case,
                bool firstCapital)
            {
                var decNum = (long) Math.Round(sourceNumber * Math.Pow(10, @decimal)) % (long) Math.Pow(10, @decimal);

                var s =
                    $" {NumeralsToTxt((long) sourceNumber, _case, true, firstCapital)} целых {NumeralsToTxt(decNum, _case, true, false)} сотых";
                return s.Trim();
            }

            /// <summary>
            ///     название м-ца
            /// </summary>
            /// <param name="month">с единицы</param>
            /// <param name="@case"></param>
            /// <param name="case"></param>
            /// <returns></returns>
            public static string MonthName(int month, TextCase @case)
            {
                var s = "";

                if (month > 0 && month <= 12)
                    switch (@case)
                    {
                        case TextCase.Genitive:
                            s = monthNamesGenitive[month];
                            break;
                    }

                return s;
            }

            public static string NumeralsRoman(int number)
            {
                var s = "";

                switch (number)
                {
                    case 1:
                        s = "I";
                        break;
                    case 2:
                        s = "II";
                        break;
                    case 3:
                        s = "III";
                        break;
                    case 4:
                        s = "IV";
                        break;
                }

                return s;
            }
        }
    }
}