﻿namespace FR.Mobile.Infrastructure.Speech
{
    public class DefaultTextToSpeech : ITextToSpeech
    {
        public void Speak(string text)
        {
        }

        public bool CanInit => true;

        public void Init()
        {
        }

        public void Release()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }
    }
}