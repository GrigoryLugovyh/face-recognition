﻿namespace FR.Mobile.Infrastructure.Speech
{
    public interface ITextToSpeechProvider
    {
        void SpeakAsSubtotal(string rawCurrency);
    }
}