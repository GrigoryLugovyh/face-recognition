﻿using System;
using FR.Mobile.Storage;

namespace FR.Mobile.Infrastructure.Speech
{
    public class TextToSpeechProvider : ITextToSpeechProvider
    {
        private readonly Lazy<IPreferences> preferences = new Lazy<IPreferences>(Container.Resolve<IPreferences>);

        private readonly Lazy<ITextToSpeech> textToSpeech = new Lazy<ITextToSpeech>(Container.Resolve<ITextToSpeech>);

        public void SpeakAsSubtotal(string rawCurrency)
        {
            if(!preferences.Value.VoiceOverEnable) return;

            if (!double.TryParse(rawCurrency, out var currency))
            {
                return;
            }

            textToSpeech.Value.Speak(
                $"С вас {ValueWordsBuilder.RuDateAndMoneyConverter.CurrencyToTxt(currency, true)}");
        }
    }
}