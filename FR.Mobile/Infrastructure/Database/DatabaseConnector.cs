﻿using System;
using FR.Mobile.Infrastructure.FileSystem;
using SQLite;

namespace FR.Mobile.Infrastructure.Database
{
    public class DatabaseConnector : IDatabaseConnector
    {
        public static readonly object ConnectorLockContext = new object();

        private readonly Lazy<IFileSystem> fileSystem = new Lazy<IFileSystem>(Container.Resolve<IFileSystem>);

        private SQLiteConnection connection;

        public SQLiteConnection Connection
        {
            get
            {
                lock (ConnectorLockContext)
                {
                    return connection ??
                           (connection = new SQLiteConnection(fileSystem.Value.ApplicationDatabasePath,
                               SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.FullMutex |
                               SQLiteOpenFlags.SharedCache));
                }
            }
        }

        public void Dispose()
        {
            connection?.Close();
        }
    }
}