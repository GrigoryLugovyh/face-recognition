﻿using System;
using SQLite;

namespace FR.Mobile.Infrastructure.Database
{
    public interface IDatabaseConnector : IDisposable
    {
        SQLiteConnection Connection { get; }
    }
}