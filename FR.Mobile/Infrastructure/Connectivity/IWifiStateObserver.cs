﻿using System;

namespace FR.Mobile.Infrastructure.Connectivity
{
    public interface IWifiStateObserver
    {
        event EventHandler Connected;

        event EventHandler Disconnected;

        void RaiseConnected();

        void RaiseDisconnected();
    }
}