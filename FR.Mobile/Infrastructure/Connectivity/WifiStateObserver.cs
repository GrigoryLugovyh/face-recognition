﻿using System;

namespace FR.Mobile.Infrastructure.Connectivity
{
    public class WifiStateObserver : IWifiStateObserver
    {
        public event EventHandler Connected;

        public event EventHandler Disconnected;

        public void RaiseConnected()
        {
            Logger.Info("Network connected");

            Connected?.Invoke(this, EventArgs.Empty);
        }

        public void RaiseDisconnected()
        {
            Logger.Warn("Network disconnected");

            Disconnected?.Invoke(this, EventArgs.Empty);
        }
    }
}