﻿namespace FR.Mobile.Infrastructure.FileSystem
{
    public interface IFileSystem
    {
        string CreateDirectory(string directory);

        string ApplicationDirectory { get; }

        string ApplicationDatabasePath { get; }

        string ApplicationDataDirectory { get; }

        string ApplicationImagesDirectory { get; }

        string ApplicationPhotoImagesDirectory { get; }

        string ApplicationAdvertisingImagesDirectory { get; }
    }
}