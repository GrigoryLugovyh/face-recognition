﻿using System;
using System.IO;

namespace FR.Mobile.Infrastructure.FileSystem
{
    public class FileSystem : IFileSystem
    {
        private const string DatabaseName = "fr.db";

        private const string CustomerDisplayDirectoryName = "CustomerDisplay";

        private const string ApplicationImagesDirectoryName = "Iamges";
        private const string ApplicationPhotoImagesDirectoryName = "Photo";
        private const string ApplicationAdvertisingImagesDirectoryName = "Advertising";

        public string CreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            return directory;
        }

        public string ApplicationDirectory =>
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        public string ApplicationDatabasePath => Path.Combine(CreateDirectory(ApplicationDataDirectory), DatabaseName);

        public string ApplicationDataDirectory =>
            CreateDirectory(Path.Combine(CreateDirectory(ApplicationDirectory), CustomerDisplayDirectoryName));

        public string ApplicationImagesDirectory =>
            CreateDirectory(Path.Combine(CreateDirectory(ApplicationDataDirectory), ApplicationImagesDirectoryName));

        public string ApplicationPhotoImagesDirectory =>
            CreateDirectory(Path.Combine(ApplicationImagesDirectory, ApplicationPhotoImagesDirectoryName));

        public string ApplicationAdvertisingImagesDirectory =>
            CreateDirectory(Path.Combine(ApplicationPhotoImagesDirectory, ApplicationAdvertisingImagesDirectoryName));
    }
}