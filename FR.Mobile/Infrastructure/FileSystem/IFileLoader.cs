﻿namespace FR.Mobile.Infrastructure.FileSystem
{
    public interface IFileLoader
    {
        byte[] Download(string filePath);
    }
}