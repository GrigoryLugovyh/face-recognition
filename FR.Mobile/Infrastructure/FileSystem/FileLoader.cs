﻿using System.IO;

namespace FR.Mobile.Infrastructure.FileSystem
{
    public class FileLoader : IFileLoader
    {
        public byte[] Download(string filePath)
        {
            return File.Exists(filePath) ? File.ReadAllBytes(filePath) : new byte[0];
        }
    }
}