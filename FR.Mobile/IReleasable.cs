﻿namespace FR.Mobile
{
    public interface IReleasable
    {
        void Release();
    }
}