﻿using FR.Mobile.Executors.Commands;

namespace FR.Mobile.Executors
{
    public interface ICommandExecutor
    {
        void Execute(IFrCommand command);
    }

    public interface ICommandExecutor<in TCommand> : ICommandExecutor where TCommand : IFrCommand
    {
        ICommandSender<TCommand> Sender { get; }

        ICommandSaver<TCommand> Saver { get; }
    }
}