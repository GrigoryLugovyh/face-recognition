﻿namespace FR.Mobile.Executors
{
    public interface ICommandGenerator
    {
        void Enqueue(object value);
    }
}