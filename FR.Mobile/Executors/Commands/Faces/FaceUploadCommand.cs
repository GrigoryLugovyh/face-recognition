﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace FR.Mobile.Executors.Commands.Faces
{
    public class FaceUploadCommand : FrCommand, IFaceUploadCommand
    {
        public override string Name => "Отправка фото";

        protected override string Resource => "faces";

        public byte[] ImageData { get; set; }

        public override double Priority => 10.0d;

        protected override async Task<bool> ExecuteInternal(HttpClient client)
        {
            Logger.Info("Sending image ...");

            using (var content = new MultipartFormDataContent($"Upload----{Guid.NewGuid()}"))
            {
                content.Add(new StreamContent(new MemoryStream(ImageData)));

                SetHttpContentHeaders(content.Headers);

                using (var response = await client.PostAsync(RequestUri, content))
                {
                    Logger.Info($"Sending image result: {response.IsSuccessStatusCode}");

                    return response.IsSuccessStatusCode;
                }
            }
        }
    }
}