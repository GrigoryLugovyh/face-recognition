﻿namespace FR.Mobile.Executors.Commands.Faces
{
    public interface IFaceUploadCommand : IFrCommand
    {
        byte[] ImageData { get; set; }
    }
}