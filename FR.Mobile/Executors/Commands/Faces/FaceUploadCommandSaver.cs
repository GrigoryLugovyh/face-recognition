﻿using System;
using System.IO;
using FR.Mobile.Infrastructure.FileSystem;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Executors.Commands.Faces
{
    public class FaceUploadCommandSaver : ICommandSaver<FaceUploadCommand>
    {
        private readonly Lazy<IFileSystem> fileSystem = new Lazy<IFileSystem>(Container.Resolve<IFileSystem>);

        private readonly Lazy<IMImageRepository> imageRepository =
            new Lazy<IMImageRepository>(Container.Resolve<IMImageRepository>);

        public bool Save(FaceUploadCommand command)
        {
            var imageFileName = Guid.NewGuid().ToString();
            var imageFilePath = Path.Combine(fileSystem.Value.ApplicationPhotoImagesDirectory, imageFileName);

            File.WriteAllBytes(imageFilePath, command.ImageData);

            Logger.Info($"Image file was saved on device with path \"{imageFilePath}\"");

            if (!File.Exists(imageFilePath))
            {
                Logger.Warn($"Unable to save file by specified path \"{imageFilePath}\"");
                return false;
            }

            imageRepository.Value.Insert(new MImage
            {
                SessionId = command.SessionId,
                Name = imageFileName
            });

            return true;
        }
    }
}