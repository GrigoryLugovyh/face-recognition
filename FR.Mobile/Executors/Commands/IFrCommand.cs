﻿using System.Threading.Tasks;

namespace FR.Mobile.Executors.Commands
{
    public interface IFrCommand
    {
        string Name { get; }

        double Priority { get; }

        string SessionId { get; set; }

        bool TryExecuteImmediately { get; set; }

        Task<bool> Execute();

        bool CanExecute();
    }
}