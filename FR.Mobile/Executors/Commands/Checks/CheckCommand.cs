﻿using System.Collections.Generic;
using FR.Mobile.Models;
using FR.Model.Dto;

namespace FR.Mobile.Executors.Commands.Checks
{
    public class CheckCommand : FrStringContentCommand<(MCheck Check, IEnumerable<MCheckPosition> Positions,
        IEnumerable<MCheckPayment> Payments,
        IEnumerable<MCheckBonusCard> Bonuscards, IEnumerable<MCheckDiscountCard> DiscountCards), CheckDto>
    {
        protected override string Resource => "checks";

        public override double Priority => 5.0d;

        public override string Name => "Отправка данных продаж";
    }
}