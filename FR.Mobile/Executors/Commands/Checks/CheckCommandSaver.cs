﻿using System;
using FR.Mobile.Repositories;

namespace FR.Mobile.Executors.Commands.Checks
{
    public class CheckCommandSaver : ICommandSaver<CheckCommand>
    {
        private readonly Lazy<IMCheckBonusCardRepository> checkBonuscardRepository =
            new Lazy<IMCheckBonusCardRepository>(Container.Resolve<IMCheckBonusCardRepository>);

        private readonly Lazy<IMCheckDiscountCardRepository> checkDiscountcardRepository =
            new Lazy<IMCheckDiscountCardRepository>(Container.Resolve<IMCheckDiscountCardRepository>);

        private readonly Lazy<IMCheckPaymentRepository> checkPaymentRepository =
            new Lazy<IMCheckPaymentRepository>(Container.Resolve<IMCheckPaymentRepository>);

        private readonly Lazy<IMCheckPositionRepository> checkPositionRepository =
            new Lazy<IMCheckPositionRepository>(Container.Resolve<IMCheckPositionRepository>);

        private readonly Lazy<IMCheckRepository> checkRepository =
            new Lazy<IMCheckRepository>(Container.Resolve<IMCheckRepository>);

        public bool Save(CheckCommand command)
        {
            Logger.Info("Saving check ...");

            checkRepository.Value.Insert(command.Value.Check);

            var checkId = command.Value.Check.Id;

            foreach (var position in command.Value.Positions)
            {
                position.CheckId = checkId;
                checkPositionRepository.Value.Insert(position);
            }

            foreach (var payment in command.Value.Payments)
            {
                payment.CheckId = checkId;
                checkPaymentRepository.Value.Insert(payment);
            }

            foreach (var bonusCard in command.Value.Bonuscards)
            {
                bonusCard.CheckId = checkId;
                checkBonuscardRepository.Value.Insert(bonusCard);
            }

            foreach (var discountCard in command.Value.DiscountCards)
            {
                discountCard.CheckId = checkId;
                checkDiscountcardRepository.Value.Insert(discountCard);
            }

            Logger.Info("Check saved");

            return true;
        }
    }
}