﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FR.Mobile.Mapper;
using Newtonsoft.Json;

namespace FR.Mobile.Executors.Commands
{
    public abstract class FrStringContentCommand<TInValue, TOutValue> : FrCommand
    {
        public TInValue Value { get; set; }

        public virtual Encoding Encoding => Encoding.UTF8;

        protected virtual IMapper<TInValue, TOutValue> Mapper => Container.Resolve<IMapper<TInValue, TOutValue>>();

        protected override HttpClient HttpClientInstance => new HttpClient(new HttpClientHandler
        {
            AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
        });

        protected override async Task<bool> ExecuteInternal(HttpClient client)
        {
            if (Value == null)
                throw new ArgumentNullException(nameof(Value));

            var content = new StringContent(JsonConvert.SerializeObject(Mapper.Map(Value)), Encoding.UTF8,
                "application/json");

            SetHttpContentHeaders(content.Headers);

            using (var response = await client.PostAsync(RequestUri, content))
            {
                return response.IsSuccessStatusCode;
            }
        }
    }
}