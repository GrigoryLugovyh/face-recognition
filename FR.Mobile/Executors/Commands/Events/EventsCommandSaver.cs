﻿using System;
using FR.Mobile.Repositories;

namespace FR.Mobile.Executors.Commands.Events
{
    // тут не должно быть логирования
    public class EventsCommandSaver : ICommandSaver<EventsCommand>
    {
        private readonly Lazy<IMEventRepository> logRepository =
            new Lazy<IMEventRepository>(Container.Resolve<IMEventRepository>);

        public bool Save(EventsCommand command)
        {
            return logRepository.Value.InsertAll(command.Value, false) > -1;
        }
    }
}