﻿using System.Collections.Generic;
using FR.Mobile.Models;
using FR.Model.Dto;

namespace FR.Mobile.Executors.Commands.Events
{
    public class EventsCommand : FrStringContentCommand<IEnumerable<MEvent>, IEnumerable<EventDto>>
    {
        protected override string Resource => "events";

        public override string Name => "Отправка логов";
    }
}