﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using FR.Mobile.Storage;

namespace FR.Mobile.Executors.Commands
{
    public abstract class FrCommand : IFrCommand
    {
        private readonly Lazy<IPreferences> preferences = new Lazy<IPreferences>(Container.Resolve<IPreferences>);

        private int attempts;

        protected virtual IEnumerable<(string Name, string Value)> Headers
        {
            get
            {
                yield return ("session-id", SessionId);
                yield return ("device-id", preferences.Value.DeviveId);
                yield return ("client-datetime", DateTime.UtcNow.ToString("s"));
            }
        }

        protected abstract string Resource { get; }

        protected virtual string BaseUri => preferences.Value.ServerAddress;

        protected virtual string RequestUri => $"{BaseUri}/{Resource}";

        protected virtual HttpClient HttpClientInstance => new HttpClient();

        protected virtual int HttpClientTimeoutSeconds => 90;

        protected virtual int AllowAttempts => 1;

        public abstract string Name { get; }

        public virtual double Priority => 1.0d;

        public string SessionId { get; set; }

        public bool TryExecuteImmediately { get; set; }

        public virtual async Task<bool> Execute()
        {
            try
            {
                using (var client = HttpClientInstance)
                {
                    client.Timeout = TimeSpan.FromSeconds(HttpClientTimeoutSeconds);
                    return await ExecuteInternal(client);
                }
            }
            catch (HttpRequestException e)
            {
                Logger.Warn(e.Message);
                return false;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return false;
            }
            finally
            {
                attempts++;
            }
        }

        public bool CanExecute()
        {
            return AllowAttempts > attempts;
        }

        protected virtual void SetHttpContentHeaders(HttpContentHeaders headers)
        {
            foreach (var (name, value) in Headers)
                headers.Add(name, value);
        }

        protected abstract Task<bool> ExecuteInternal(HttpClient client);
    }
}