﻿using FR.Mobile.Executors.Commands;

namespace FR.Mobile.Executors
{
    public interface ICommandQueueHandler : IStartable, IStopable
    {
        void Handle(IFrCommand command);
    }
}