﻿using FR.Mobile.Executors.Commands;

namespace FR.Mobile.Executors
{
    public interface ICommandSender<in TCommand> where TCommand : IFrCommand
    {
        bool Send(TCommand command);
    }
}