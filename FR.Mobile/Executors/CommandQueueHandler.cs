﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DataStructures;
using FR.Mobile.Executors.Commands;
using FR.Mobile.Executors.Commands.Checks;
using FR.Mobile.Executors.Commands.Events;
using FR.Mobile.Executors.Commands.Faces;

namespace FR.Mobile.Executors
{
    public class CommandQueueHandler : ICommandQueueHandler
    {
        private const int MaintainenceTimeoutMilliseconds = 8;
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        private readonly Dictionary<Type, ICommandExecutor> commandExecutors = new Dictionary<Type, ICommandExecutor>
        {
            [typeof(FaceUploadCommand)] = Container.Resolve<ICommandExecutor<FaceUploadCommand>>(),
            [typeof(EventsCommand)] = Container.Resolve<ICommandExecutor<EventsCommand>>(),
            [typeof(CheckCommand)] = Container.Resolve<ICommandExecutor<CheckCommand>>()
        };

        private readonly ConcurrentPriorityQueue<IFrCommand> commandQueue =
            new ConcurrentPriorityQueue<IFrCommand>(new ConcurrentPriorityQueueComparer());

        private Task maintainenceTask;

        public void Handle(IFrCommand command)
        {
            while (!commandQueue.TryAdd(command))
                Task.Delay(MaintainenceTimeoutMilliseconds).Wait();
        }

        public void Start()
        {
            if (maintainenceTask != null && !maintainenceTask.IsCanceled)
                Stop();

            maintainenceTask = Task.Run((Action) Maintainence, cancellationTokenSource.Token);
        }

        public void Stop()
        {
            cancellationTokenSource.Cancel();
        }

        private void Maintainence()
        {
            while (!cancellationTokenSource.IsCancellationRequested)
                try
                {
                    if (commandQueue.TryTake(out var commandCandidate) &&
                        commandExecutors.TryGetValue(commandCandidate.GetType(), out var commandExecutor))
                        commandExecutor.Execute(commandCandidate);
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }
                finally
                {
                    Task.Delay(MaintainenceTimeoutMilliseconds).Wait();
                }
        }

        internal class ConcurrentPriorityQueueComparer : IComparer<IFrCommand>
        {
            public int Compare(IFrCommand x, IFrCommand y)
            {
                if (x?.Priority > y?.Priority) return 1;
                if (x?.Priority < y?.Priority) return -1;
                return 0;
            }
        }
    }
}