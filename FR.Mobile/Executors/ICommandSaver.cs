﻿using FR.Mobile.Executors.Commands;

namespace FR.Mobile.Executors
{
    public interface ICommandSaver<in TCommand> where TCommand : IFrCommand
    {
        bool Save(TCommand command);
    }
}