﻿using System;
using System.Collections.Generic;
using FR.Mobile.Cash;
using FR.Mobile.Executors.Commands;
using FR.Mobile.Executors.Commands.Checks;
using FR.Mobile.Executors.Commands.Events;
using FR.Mobile.Executors.Commands.Faces;
using FR.Mobile.Extensions;
using FR.Mobile.Mapper;
using FR.Mobile.Models;
using FR.Mobile.Session;

namespace FR.Mobile.Executors
{
    public class CommandGenerator : ICommandGenerator
    {
        private readonly Dictionary<Type, Func<object, IFrCommand>> commandGeneratorsOwner =
            new Dictionary<Type, Func<object, IFrCommand>>
            {
                [typeof(byte[])] = image => CreateFaceUploadCommand((byte[]) image), // todo type for byte[] image
                [typeof(MEvent)] = @event => CreateEventsCommand((MEvent) @event),
                [typeof(Check)] = check => CreateCheckCommand((Check) check)
            };

        private readonly Lazy<ICommandQueueHandler> commandQueueHandler =
            new Lazy<ICommandQueueHandler>(Container.Resolve<ICommandQueueHandler>);

        private readonly Lazy<ISessionManager> sessionManager =
            new Lazy<ISessionManager>(Container.Resolve<ISessionManager>);

        public void Enqueue(object value)
        {
            var valueType = value.GetType();

            if (commandGeneratorsOwner.ContainsKey(valueType))
            {
                var command = commandGeneratorsOwner[valueType](value);
                command.SessionId = sessionManager.Value.GetCurrentSessionId;
                commandQueueHandler.Value.Handle(command);
                return;
            }

            Logger.Warn($"Cannot create command for type \"{valueType.Name}\"");
        }

        private static EventsCommand CreateEventsCommand(MEvent @event)
        {
            return new EventsCommand
            {
                Value = @event.Yield()
            };
        }

        private static CheckCommand CreateCheckCommand(Check check)
        {
            return new CheckCommand
            {
                Value = Container
                    .Resolve<IMapper<Check, (MCheck, IEnumerable<MCheckPosition>, IEnumerable<MCheckPayment>,
                        IEnumerable<MCheckBonusCard>, IEnumerable<MCheckDiscountCard>)>>().Map(check)
            };
        }

        private static FaceUploadCommand CreateFaceUploadCommand(byte[] image)
        {
            // TryExecuteImmediately нужно выставлять в true если есть доступ в интернет (ConnectivityManager скажет)
            // но в связи с переделкой схемы распознования на сервере (PersonLargeGroup) возможно это и не понадобиться
            return new FaceUploadCommand
            {
                ImageData = image,
                TryExecuteImmediately = false
            };
        }
    }
}