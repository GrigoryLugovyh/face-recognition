﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Executors.Commands;

namespace FR.Mobile.Executors
{
    // тут не должно быть логирования
    public class CommandExecutor<TCommand> : ICommandExecutor<TCommand> where TCommand : IFrCommand
    {
        private readonly List<(Func<TCommand, bool> Condition, Action<TCommand> Behavior)> commnadExecuteBehaviorOwner;

        public CommandExecutor()
        {
            commnadExecuteBehaviorOwner = new List<(Func<TCommand, bool>, Action<TCommand>)>
            {
                (command => command.TryExecuteImmediately, command =>
                {
                    if (!Sender.Send(command)) Saver.Save(command);
                }),
                (command => true, command => { Saver.Save(command); })
            };
        }

        public virtual ICommandSender<TCommand> Sender => Container.Resolve<ICommandSender<TCommand>>();

        public virtual ICommandSaver<TCommand> Saver => Container.Resolve<ICommandSaver<TCommand>>();

        public void Execute(IFrCommand command)
        {
            var commandT = (TCommand) command;
            commnadExecuteBehaviorOwner.First(option => option.Condition(commandT)).Behavior(commandT);
        }
    }
}