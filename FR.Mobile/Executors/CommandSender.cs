﻿using FR.Mobile.Executors.Commands;

namespace FR.Mobile.Executors
{
    public class CommandSender<TCommand> : ICommandSender<TCommand> where TCommand : IFrCommand
    {
        public bool Send(TCommand command)
        {
            if (command.CanExecute())
            {
                var commandResult = command.Execute().Result;

                return commandResult;
            }

            return false;
        }
    }
}