﻿using System;

namespace FR.Mobile.Events
{
    public interface IGlobalEvents
    {
        event EventHandler<bool> CameraPreviewEnable;

        event EventHandler<bool> HelloScreenVisible; 

        void RaiseCameraPreviewEnable(bool enable);

        void RaiseHelloScreenVisible(bool visible);
    }
}