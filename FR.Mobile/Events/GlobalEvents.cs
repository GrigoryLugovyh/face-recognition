﻿using System;

namespace FR.Mobile.Events
{
    public class GlobalEvents : IGlobalEvents
    {
        private readonly object eventLockContext = new object();

        public event EventHandler<bool> CameraPreviewEnable;

        public event EventHandler<bool> HelloScreenVisible;

        public void RaiseCameraPreviewEnable(bool enable)
        {
            lock (eventLockContext)
            {
                CameraPreviewEnable?.Invoke(null, enable);
            }
        }

        public void RaiseHelloScreenVisible(bool visible)
        {
            lock (eventLockContext)
            {
                HelloScreenVisible?.Invoke(null, visible);
            }
        }
    }
}