﻿using FR.Mobile.App;

namespace FR.Mobile.Listener
{
    public interface IExternalEventDispatcher<out TExternalEventDistributor, out TExternalEventInterpretator> : IExternalEventDispatcher
        where TExternalEventDistributor : IExternalEventDistributor, IManage, new()
        where TExternalEventInterpretator : IExternalEventInterpretator, new()
    {
        TExternalEventDistributor ExternalEventDistributor { get; }

        TExternalEventInterpretator ExternalEventInterpretator { get; }
    }
}