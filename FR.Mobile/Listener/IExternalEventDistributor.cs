﻿using System;

namespace FR.Mobile.Listener
{
    public interface IExternalEventDistributor : IManage
    {
        event EventHandler<byte[]> RawExternalEvent;
    }
}