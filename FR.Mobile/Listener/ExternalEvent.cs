﻿using System;

namespace FR.Mobile.Listener
{
    public class ExternalEvent
    {
        public string Key { get; set; }

        public string Mode { get; set; }

        public string Name { get; set; }

        public string Barcode { get; set; }

        public string Code { get; set; }

        public string Price { get; set; }

        public string Count { get; set; }

        public string Total { get; set; }

        public bool Delete { get; set; }

        public string Payment { get; set; }

        public DateTime EventTimeStamp { get; set; }
    }
}