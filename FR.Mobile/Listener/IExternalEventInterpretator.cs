﻿using FR.Mobile.Listener.Prisma;

namespace FR.Mobile.Listener
{
    public interface IExternalEventInterpretator
    {
        ExternalEvent Interpret(byte[] rawValue);
    }
}