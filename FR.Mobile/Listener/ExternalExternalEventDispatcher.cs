﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FR.Mobile.Listener
{
    public class ExternalEventSalesKey
    {
        public const string AddSaleKey = nameof(AddSaleKey);
        public const string StartSalesKey = nameof(StartSalesKey);
        public const string SubtotSalesKey = nameof(SubtotSalesKey);
        public const string FinishSalesKey = nameof(FinishSalesKey);
    }

    public class
        ExternalEventDispatcher<TExternalEventDistributor, TExternalEventInterpretator> : IExternalEventDispatcher<
            TExternalEventDistributor, TExternalEventInterpretator>
        where TExternalEventDistributor : class, IExternalEventDistributor, IManage, new()
        where TExternalEventInterpretator : class, IExternalEventInterpretator, new()
    {
        private IDictionary<string, Action<ExternalEvent>> eventsBehaviorOwner;

        public event EventHandler<ExternalEvent> ChangeSale;

        public event EventHandler<ExternalEvent> StartSales;

        public event EventHandler<ExternalEvent> SubtotSales;

        public event EventHandler<ExternalEvent> FinishSales;

        public bool CanInit => true;

        public void Init()
        {
            ExternalEventDistributor = Container.Resolve<TExternalEventDistributor>();
            ExternalEventInterpretator = Container.Resolve<TExternalEventInterpretator>();

            ExternalEventDistributor.Init();

            eventsBehaviorOwner = new Dictionary<string, Action<ExternalEvent>>
            {
                [ExternalEventSalesKey.AddSaleKey] = e => { ChangeSale?.Invoke(this, e); },
                [ExternalEventSalesKey.StartSalesKey] = e => { StartSales?.Invoke(this, e); },
                [ExternalEventSalesKey.SubtotSalesKey] = e => { SubtotSales?.Invoke(this, e); },
                [ExternalEventSalesKey.FinishSalesKey] = e => { FinishSales?.Invoke(this, e); }
            };
        }

        public void Start()
        {
            ExternalEventDistributor.Start();
            ExternalEventDistributor.RawExternalEvent += ExternalEventDistributorOnRawExternalEvent;
        }

        public void Stop()
        {
            ExternalEventDistributor.Stop();
            ExternalEventDistributor.RawExternalEvent -= ExternalEventDistributorOnRawExternalEvent;
        }

        public void Release()
        {
            ExternalEventDistributor.Release();
            Stop();
        }

        public TExternalEventDistributor ExternalEventDistributor { get; private set; }

        public TExternalEventInterpretator ExternalEventInterpretator { get; private set; }

        private void ExternalEventDistributorOnRawExternalEvent(object sender, byte[] bytes)
        {
            if (bytes == null || !bytes.Any())
                return;

            var value = ExternalEventInterpretator.Interpret(bytes);

            if (string.IsNullOrEmpty(value?.Mode))
            {
                Logger.Warn("ExternalEvent mode is empty");
                return;
            }

            if (!eventsBehaviorOwner.ContainsKey(value.Mode))
            {
                Logger.Warn($"ExternalEvent mode doesn't support, mode \"{value.Mode}\"");
                return;
            }

            eventsBehaviorOwner[value.Mode](value);

            Logger.Info("ExternalEvent was raised");
        }
    }
}