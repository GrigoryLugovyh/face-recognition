﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FR.Mobile.Listener.Prisma
{
    public class PrismaEventInterpretator : IExternalEventInterpretator
    {
        private const string StopCheckKey = "5";
        private const string StartCheckKey = "4";
        private const string ChangeItemPriceKey = "15";
        private const string AddItemToCheckKey = "6";
        private const string ChangeItemQuantityKey = "12";

        private const string AddItemByArticulKey = "65";
        private const string AddItemByBarcodeManualyKey = "66";
        private const string AddItemByMeansOfScannerKey = "67";
        private const string AddItemFromListKey = "68";
        private const string AddItemWithPriceKey = "69";

        private const string CancelCheckKey = "25";
        private const string RemoveItemFromCheckKey = "18";

        private const string CashPaymentKey = "37";
        private const string NoncashPaymentKey = "38";

        private readonly Dictionary<string, string> externalEventSalesKeyMapper = new Dictionary<string, string>
        {
            [AddItemToCheckKey] = ExternalEventSalesKey.AddSaleKey,
            [ChangeItemQuantityKey] = ExternalEventSalesKey.AddSaleKey,
            [ChangeItemPriceKey] = ExternalEventSalesKey.AddSaleKey,
            [StartCheckKey] = ExternalEventSalesKey.StartSalesKey,
            [StopCheckKey] = ExternalEventSalesKey.FinishSalesKey,

            [AddItemByArticulKey] = ExternalEventSalesKey.AddSaleKey,
            [AddItemByBarcodeManualyKey] = ExternalEventSalesKey.AddSaleKey,
            [AddItemByMeansOfScannerKey] = ExternalEventSalesKey.AddSaleKey,
            [AddItemFromListKey] = ExternalEventSalesKey.AddSaleKey,
            [AddItemWithPriceKey] = ExternalEventSalesKey.AddSaleKey,

            [CancelCheckKey] = ExternalEventSalesKey.FinishSalesKey,
            [RemoveItemFromCheckKey] = ExternalEventSalesKey.AddSaleKey,

            [CashPaymentKey] = ExternalEventSalesKey.SubtotSalesKey,
            [NoncashPaymentKey] = ExternalEventSalesKey.SubtotSalesKey
        };

        public ExternalEvent Interpret(byte[] rawValue)
        {
            var stringValue = Encoding.GetEncoding(866).GetString(rawValue);

            Logger.Info($"It was received data: \"{stringValue}\"");

            var externalEvent = new ExternalEvent
            {
                EventTimeStamp = DateTime.UtcNow
            };

            string Substring(int startIndex, int lenght)
            {
                return stringValue.Substring(startIndex, lenght).Trim();
            }

            string MapSalesKey(string key)
            {
                return externalEventSalesKeyMapper.ContainsKey(key) ? externalEventSalesKeyMapper[key] : string.Empty;
            }

            var parcer = new List<Action>
            {
                () => externalEvent.Mode = MapSalesKey(Substring(9, 4)),
                () => externalEvent.Key = Substring(9, 4),
                () => externalEvent.Barcode = Substring(76, 13),
                () => externalEvent.Code = Substring(89, 4),
                () => externalEvent.Name = Substring(111, 30),
                () => externalEvent.Count = Substring(164, 15),
                () => externalEvent.Price = Substring(179, 15),
                () => externalEvent.Total = Substring(194, 15)
            };

            parcer.ForEach(action => action());

            externalEvent.Delete = externalEvent.Key == RemoveItemFromCheckKey;

            switch (externalEvent.Key)
            {
                case CashPaymentKey:
                    externalEvent.Payment = "cash";
                    break;
                case NoncashPaymentKey:
                    externalEvent.Payment = "noncash";
                    break;
            }

            Logger.Info($"Created external event with mode \"{externalEvent.Mode}\"");

            return externalEvent;
        }
    }
}