﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using FR.Mobile.Infrastructure.Connectivity;

namespace FR.Mobile.Listener.Prisma
{
    public class PrismaEventDistributor : IExternalEventDistributor
    {
        private const int ListenerPort = 12345;
        private readonly object prismaLockContext = new object();
        private volatile byte[] buffer;

        private bool connected;

        private IWifiStateObserver stateObserver;

        private Socket udpSocket;

        public event EventHandler<byte[]> RawExternalEvent;

        public bool CanInit => true;

        public void Init()
        {
            buffer = new byte[512];

            stateObserver = Container.Resolve<IWifiStateObserver>();
        }

        public void Start()
        {
            stateObserver.Connected += StateObserverOnConnected;
            stateObserver.Disconnected += StateObserverOnDisconnected;

            StateObserverOnConnected(this, EventArgs.Empty);

            Logger.Info($"{GetType().Name} started");
        }

        public void Stop()
        {
            stateObserver.Connected -= StateObserverOnConnected;
            stateObserver.Disconnected -= StateObserverOnDisconnected;

            Release();

            Logger.Info($"{GetType().Name} stoped");
        }

        public void Release()
        {
            lock (prismaLockContext)
            {
                if (udpSocket != null)
                {
                    if (udpSocket.Connected)
                        udpSocket.Disconnect(false);

                    udpSocket.Dispose();
                    udpSocket = null;
                }

                connected = false;
            }
        }

        private void StateObserverOnConnected(object sender, EventArgs eventArgs)
        {
            lock (prismaLockContext)
            {
                if (connected) return;

                EndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                udpSocket.Bind(new IPEndPoint(IPAddress.Any, ListenerPort));
                udpSocket.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref endPoint, ReceiveFrom,
                    udpSocket);

                connected = true;
            }
        }

        private void StateObserverOnDisconnected(object sender, EventArgs eventArgs)
        {
            lock (prismaLockContext)
            {
                Release();
            }
        }

        private void ReceiveFrom(IAsyncResult asyncResult)
        {
            try
            {
                lock (prismaLockContext)
                {
                    Logger.Info("Prisma data recieving ...");

                    var recvSock = (Socket) asyncResult.AsyncState;

                    EndPoint clientEp = new IPEndPoint(IPAddress.Any, 0);

                    var msgLen = recvSock.EndReceiveFrom(asyncResult, ref clientEp);
                    var localMsg = new byte[msgLen];

                    Array.Copy(buffer, localMsg, msgLen);

                    EndPoint newClientEp = new IPEndPoint(IPAddress.Any, 0);

                    udpSocket.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref newClientEp, ReceiveFrom,
                        udpSocket);

                    var data = new byte[buffer.Length];
                    buffer.CopyTo(data, 0);

                    Task.Run(() => RawExternalEvent?.Invoke(this, data));

                    Logger.Info("Prisma data recieved");
                }
            }
            catch (ObjectDisposedException)
            {
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }
    }
}