﻿using SQLite;

namespace FR.Mobile.Models
{
    [Table("logs")]
    public class MEvent : MEntity
    {
        public string Scope { get; set; }

        public string Level { get; set; }

        public string Message { get; set; }
    }
}