﻿namespace FR.Mobile.Models
{
    public abstract class MCheckEntity : MEntity
    {
        public int CheckId { get; set; }
    }
}