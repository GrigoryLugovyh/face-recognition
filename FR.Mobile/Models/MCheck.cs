﻿using System;
using SQLite;

namespace FR.Mobile.Models
{
    [Table("checks")]
    public class MCheck : MEntity
    {
        public string PosName { get; set; }

        public string CashierName { get; set; }

        public decimal TotalSum { get; set; }

        public decimal TotalDiscount { get; set; }

        public DateTime OpenTimeStamp { get; set; }

        public DateTime OpenTimeStampUtc { get; set; }

        public DateTime CloseTimeStamp { get; set; }

        public DateTime CloseTimeStampUtc { get; set; }

        public int DurationSeconds { get; set; }
    }
}