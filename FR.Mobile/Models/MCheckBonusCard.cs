﻿using SQLite;

namespace FR.Mobile.Models
{
    [Table("checkBonusCards")]
    public class MCheckBonusCard : MCheckEntity
    {
        public string BonusCard { get; set; }
    }
}