﻿using SQLite;

namespace FR.Mobile.Models
{
    [Table("checkPayments")]
    public class MCheckPayment : MCheckEntity
    {
        public string Payment { get; set; }
    }
}