﻿using System;
using SQLite;

namespace FR.Mobile.Models
{
    public abstract class MEntity
    {
        [AutoIncrement, PrimaryKey]
        public int Id { get; set; }

        public string DeviceId { get; set; }

        public string SessionId { get; set; }

        public DateTime DeviceTimeStamp { get; set; }

        public DateTime CreatedTimeStamp { get; set; }

        public DateTime UpdatedTimeStamp { get; set; }
    }
}