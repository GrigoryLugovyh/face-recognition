﻿using SQLite;

namespace FR.Mobile.Models
{
    [Table("images")]
    public class MImage : MEntity
    {
        public string Name { get; set; }
    }
}