﻿using SQLite;

namespace FR.Mobile.Models
{
    [Table("checkDiscountCards")]
    public class MCheckDiscountCard : MCheckEntity
    {
        public string DiscountCard { get; set; }
    }
}