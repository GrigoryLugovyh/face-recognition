﻿using System;
using SQLite;

namespace FR.Mobile.Models
{
    [Table("checkPositions")]
    public class MCheckPosition : MCheckEntity
    {
        public string Name { get; set; }

        public string Barcode { get; set; }

        public string Code { get; set; }

        public double Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal TotalPrice { get; set; }

        public bool Changed { get; set; }

        public bool Deleted { get; set; }

        public DateTime AddTimeStamp { get; set; }

        public DateTime AddTimeStampUtc { get; set; }

        public DateTime ChangeTimeStamp { get; set; }

        public DateTime ChangeTimeStampUtc { get; set; }

        public DateTime DeleteTimeStamp { get; set; }

        public DateTime DeleteTimeStampUtc { get; set; }
    }
}