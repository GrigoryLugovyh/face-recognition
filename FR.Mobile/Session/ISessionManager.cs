﻿namespace FR.Mobile.Session
{
    public interface ISessionManager
    {
        string GetCurrentSessionId { get; }

        string StartNew();

        string StartNewIfNeed();

        void FinishCurrent();
    }
}