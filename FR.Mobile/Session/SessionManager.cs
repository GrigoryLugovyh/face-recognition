﻿using System;
using FR.Mobile.Storage;

namespace FR.Mobile.Session
{
    public class SessionManager : ISessionManager
    {
        private const string SessionKey = nameof(SessionKey);

        private readonly Lazy<IPreferences> preferences = new Lazy<IPreferences>(Container.Resolve<IPreferences>);

        private readonly object sessionLockContext = new object();

        public string StartNew()
        {
            lock (sessionLockContext)
            {
                var sessionId = Guid.NewGuid().ToString();
                preferences.Value.SetValue(SessionKey, sessionId);
                return sessionId;
            }
        }

        public string StartNewIfNeed()
        {
            lock (sessionLockContext)
            {
                var currentSessionId = GetCurrentSessionId;
                if (!string.IsNullOrEmpty(currentSessionId))
                    return currentSessionId;

                return StartNew();
            }
        }

        public void FinishCurrent()
        {
            lock (sessionLockContext)
            {
                preferences.Value.SetValue(SessionKey, string.Empty);
            }
        }

        public string GetCurrentSessionId
        {
            get
            {
                lock (sessionLockContext)
                {
                    return preferences.Value.GetValue(SessionKey);
                }
            }
        }
    }
}