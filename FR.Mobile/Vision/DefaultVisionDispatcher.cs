﻿namespace FR.Mobile.Vision
{
    public class DefaultVisionDispatcher : IVisionDispatcher
    {
        public bool IsActive { get; set; }

        public bool CanInit => true;

        public void Init()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Release()
        {
        }
    }
}