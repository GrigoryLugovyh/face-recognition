﻿using System;
using FR.Mobile.Listener;

namespace FR.Mobile.Cash
{
    public interface ICheckManager : IManage
    {
        Check Check { get; }

        void Open();

        void Close();

        void SubTotal(ExternalEvent externalEvent);

        void ChangePosition(ExternalEvent externalEvent);

        event EventHandler<Check> CheckOpened;

        event EventHandler<Check> CheckClosed;

        event EventHandler<Check> CheckChanged;
    }
}