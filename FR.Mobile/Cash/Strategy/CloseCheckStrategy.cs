﻿using System;
using System.Linq;
using FR.Mobile.Listener;

namespace FR.Mobile.Cash.Strategy
{
    public class CloseCheckStrategy : CheckStrategy, ICloseCheckStrategy
    {
        protected override CheckManager.CheckState ExpectedState => CheckManager.CheckState.Close;

        public event EventHandler<Check> CheckClosed;

        protected override bool TryModify(ref Check check, ExternalEvent externalEvent)
        {
            Logger.Info("Closing check ...");

            if (check == null ||
                new[] {CheckManager.CheckState.Idle, CheckManager.CheckState.Close}.Contains(check
                    .State))
            {
                Logger.Warn("Check already closed");
                return false;
            }

            check.CloseTimeStamp = dateTimeService.Value.Now;
            check.CloseTimeStampUtc = dateTimeService.Value.UtcNow;

            CheckClosed?.Invoke(this, check);

            Logger.Info("Check closed");

            return true;
        }
    }
}