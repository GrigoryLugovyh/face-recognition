﻿using System;
using FR.Mobile.Listener;
using FR.Mobile.Session;

namespace FR.Mobile.Cash.Strategy
{
    public class OpenCheckStrategy : CheckStrategy, IOpenCheckStrategy
    {
        private readonly Lazy<ISessionManager> sessionManager =
            new Lazy<ISessionManager>(Container.Resolve<ISessionManager>);

        protected override CheckManager.CheckState ExpectedState => CheckManager.CheckState.Open;

        public event EventHandler<Check> CheckOpened;

        protected override bool TryModify(ref Check check, ExternalEvent externalEvent)
        {
            Logger.Info("Check opening ...");

            if (check == null || check.State == CheckManager.CheckState.Close)
                check = new Check();

            if (check.State == CheckManager.CheckState.Open)
                Container.Resolve<ICloseCheckStrategy>().Modify(ref check);

            check.SessionId = sessionManager.Value.GetCurrentSessionId;
            check.OpenTimeStamp = dateTimeService.Value.Now;
            check.OpenTimeStampUtc = dateTimeService.Value.UtcNow;
            check.TotalSum = 0;
            check.TotalDiscount = 0;

            CheckOpened?.Invoke(this, check);

            Logger.Info("Check opened");

            return true;
        }
    }
}