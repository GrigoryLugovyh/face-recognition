﻿using System;
using FR.Mobile.Listener;
using FR.Mobile.Services;

namespace FR.Mobile.Cash.Strategy
{
    public abstract class CheckStrategy : ICheckStrategy
    {
        protected Lazy<IDateTimeService> dateTimeService =
            new Lazy<IDateTimeService>(Container.Resolve<IDateTimeService>);

        protected abstract CheckManager.CheckState ExpectedState { get; }

        public virtual void Modify(ref Check check, ExternalEvent externalEvent = null)
        {
            if (TryModify(ref check, externalEvent))
                check.State = ExpectedState;
        }

        protected abstract bool TryModify(ref Check check, ExternalEvent externalEvent);
    }
}