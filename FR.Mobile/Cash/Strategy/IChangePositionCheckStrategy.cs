﻿using System;

namespace FR.Mobile.Cash.Strategy
{
    public interface IChangePositionCheckStrategy : ICheckStrategy
    {
        event EventHandler<Check> CheckChanged;
    }
}