﻿using System;
using FR.Mobile.Extensions;
using FR.Mobile.Listener;

namespace FR.Mobile.Cash.Strategy
{
    public class SubtotalCheckStrategy : CheckStrategy, ISubtotalCheckStrategy
    {
        protected override CheckManager.CheckState ExpectedState => CheckManager.CheckState.Open;
        public event EventHandler<Check> CheckChanged;

        protected override bool TryModify(ref Check check, ExternalEvent externalEvent)
        {
            if (check == null || check.State != CheckManager.CheckState.Open)
                return false;

            check.TotalDiscount = Math.Round(check.ActivePositionsPrice - externalEvent.Total.ToDecimal(), 2);

            check.TotalSum = externalEvent.Total.ToDecimal();

            if (!check.Payments.Contains(externalEvent.Payment))
                check.Payments.Add(externalEvent.Payment);

            CheckChanged?.Invoke(this, check);

            return true;
        }
    }
}