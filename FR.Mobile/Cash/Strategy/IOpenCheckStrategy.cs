﻿using System;

namespace FR.Mobile.Cash.Strategy
{
    public interface IOpenCheckStrategy : ICheckStrategy
    {
        event EventHandler<Check> CheckOpened;
    }
}