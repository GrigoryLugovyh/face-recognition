﻿using System;

namespace FR.Mobile.Cash.Strategy
{
    public interface ISubtotalCheckStrategy : ICheckStrategy
    {
        event EventHandler<Check> CheckChanged;
    }
}