﻿using FR.Mobile.Listener;

namespace FR.Mobile.Cash.Strategy
{
    public interface ICheckStrategy
    {
        void Modify(ref Check check, ExternalEvent externalEvent = null);
    }
}