﻿using System;

namespace FR.Mobile.Cash.Strategy
{
    public interface ICloseCheckStrategy : ICheckStrategy
    {
        event EventHandler<Check> CheckClosed;
    }
}