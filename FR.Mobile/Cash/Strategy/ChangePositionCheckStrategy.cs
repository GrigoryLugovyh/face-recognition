﻿using System;
using System.Linq;
using FR.Mobile.Extensions;
using FR.Mobile.Listener;

namespace FR.Mobile.Cash.Strategy
{
    public class ChangePositionCheckStrategy : CheckStrategy, IChangePositionCheckStrategy
    {
        protected override CheckManager.CheckState ExpectedState => CheckManager.CheckState.Open;

        public event EventHandler<Check> CheckChanged;

        protected override bool TryModify(ref Check check, ExternalEvent externalEvent)
        {
            Logger.Info("Check changing ...");

            if (check == null ||
                new[] {CheckManager.CheckState.Idle, CheckManager.CheckState.Close}.Contains(check
                    .State))
                Container.Resolve<IOpenCheckStrategy>().Modify(ref check);

            // нужно добавить/изменить товар в чеке и заполнить все поля

            var changingPosition =
                check.Positions.FirstOrDefault(position =>
                    position.Barcode == externalEvent.Barcode && position.Code == externalEvent.Code);

            if (changingPosition == null)
            {
                changingPosition = new CheckPosition
                {
                    Number = check.ActivePositionsCount + 1,
                    Name = externalEvent.Name,
                    Code = externalEvent.Code,
                    Barcode = externalEvent.Barcode,
                    Price = externalEvent.Price.ToDecimal(),
                    Quantity = externalEvent.Count.ToDouble(),
                    TotalPrice = externalEvent.Total.ToDecimal(),

                    AddTimeStamp = dateTimeService.Value.Now,
                    AddTimeStampUtc = dateTimeService.Value.UtcNow
                };

                check.Positions.Insert(0, changingPosition);

                Logger.Info("Position was added to the check");
            }
            else
            {
                changingPosition.Price = externalEvent.Price.ToDecimal();
                changingPosition.Quantity = externalEvent.Count.ToDouble();
                changingPosition.TotalPrice = externalEvent.Total.ToDecimal();

                changingPosition.Deleted = false;
                changingPosition.DeleteTimeStamp = DateTime.MinValue;
                changingPosition.DeleteTimeStampUtc = DateTime.MinValue;

                changingPosition.Changed = true;
                changingPosition.ChangeTimeStamp = dateTimeService.Value.Now;
                changingPosition.ChangeTimeStampUtc = dateTimeService.Value.UtcNow;

                Logger.Info("Check position was modified");
            }

            if (externalEvent.Delete)
            {
                changingPosition.Deleted = true;
                changingPosition.DeleteTimeStamp = dateTimeService.Value.Now;
                changingPosition.DeleteTimeStampUtc = dateTimeService.Value.UtcNow;

                Logger.Info("Check position was deleted");
            }

            check.TotalSum = externalEvent.Total.ToDecimal();

            CheckChanged?.Invoke(this, check);

            Logger.Info("Check position was changed");

            return true;
        }
    }
}