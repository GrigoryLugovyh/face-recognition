﻿using System;
using FR.Mobile.Cash.Strategy;
using FR.Mobile.Listener;

namespace FR.Mobile.Cash
{
    public class CheckManager : ICheckManager
    {
        public enum CheckState
        {
            Idle,
            Open,
            Close
        }

        private readonly object checkManagerLockContext = new object();


        private Check check;
        private IOpenCheckStrategy openCheckStrategy;
        private ICloseCheckStrategy closeCheckStrategy;
        private ISubtotalCheckStrategy subtotalCheckStrategy;
        private IChangePositionCheckStrategy changePositionCheckStrategy;

        public Check Check
        {
            get
            {
                lock (checkManagerLockContext)
                {
                    return check;
                }
            }
        }

        public void Open()
        {
            InContext(openCheckStrategy);
        }

        public void Close()
        {
            InContext(closeCheckStrategy);
        }

        public void SubTotal(ExternalEvent externalEvent)
        {
            InContext(subtotalCheckStrategy, externalEvent);
        }

        public void ChangePosition(ExternalEvent externalEvent)
        {
            InContext(changePositionCheckStrategy, externalEvent);
        }

        public event EventHandler<Check> CheckOpened;
        public event EventHandler<Check> CheckClosed;
        public event EventHandler<Check> CheckChanged;

        public bool CanInit => true;

        public void Init()
        {
            openCheckStrategy = Container.Resolve<IOpenCheckStrategy>();
            closeCheckStrategy = Container.Resolve<ICloseCheckStrategy>();
            subtotalCheckStrategy = Container.Resolve<ISubtotalCheckStrategy>();
            changePositionCheckStrategy = Container.Resolve<IChangePositionCheckStrategy>();
        }

        public void Start()
        {
            openCheckStrategy.CheckOpened += OpenCheckStrategyOnCheckOpened;
            closeCheckStrategy.CheckClosed += CloseCheckStrategyOnCheckClosed;
            subtotalCheckStrategy.CheckChanged += ChangeCheckStrategyOnCheckChanged;
            changePositionCheckStrategy.CheckChanged += ChangeCheckStrategyOnCheckChanged;
        }

        public void Stop()
        {
            openCheckStrategy.CheckOpened -= OpenCheckStrategyOnCheckOpened;
            closeCheckStrategy.CheckClosed -= CloseCheckStrategyOnCheckClosed;
            subtotalCheckStrategy.CheckChanged -= ChangeCheckStrategyOnCheckChanged;
            changePositionCheckStrategy.CheckChanged -= ChangeCheckStrategyOnCheckChanged;
        }

        public void Release()
        {
            Stop();
        }

        private void InContext(ICheckStrategy checkStrategy, ExternalEvent externalEvent = null)
        {
            lock (checkManagerLockContext)
            {
                checkStrategy.Modify(ref check, externalEvent);
            }
        }

        private void OpenCheckStrategyOnCheckOpened(object sender, Check c)
        {
            CheckOpened?.Invoke(this, c);
        }

        private void CloseCheckStrategyOnCheckClosed(object sender, Check c)
        {
            CheckClosed?.Invoke(this, c);
        }

        private void ChangeCheckStrategyOnCheckChanged(object sender, Check c)
        {
            CheckChanged?.Invoke(this, c);
        }
    }
}