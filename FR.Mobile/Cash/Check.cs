﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FR.Mobile.Cash
{
    public class Check
    {
        public string SessionId { get; set; }

        public CheckManager.CheckState State { get; set; } = CheckManager.CheckState.Idle;

        public string PosName { get; set; }

        public string CashierName { get; set; }

        public string[] DiscountCards { get; set; }

        public string[] BonusCards { get; set; }

        public decimal TotalSum { get; set; }

        public decimal TotalDiscount { get; set; }

        public List<string> Payments { get; set; } = new List<string>();

        public DateTime OpenTimeStamp { get; set; }

        public DateTime OpenTimeStampUtc { get; set; }

        public DateTime CloseTimeStamp { get; set; }

        public DateTime CloseTimeStampUtc { get; set; }

        public TimeSpan DurationTimeSpan => CloseTimeStamp - OpenTimeStamp;

        public List<CheckPosition> Positions { get; set; } = new List<CheckPosition>();

        public IEnumerable<CheckPosition> ActivePositions => Positions.Where(position => !position.Deleted);

        public int ActivePositionsCount => ActivePositions.Count();

        public IEnumerable<CheckPosition> DeletedPositions => Positions.Where(position => position.Deleted);

        public decimal ActivePositionsPrice => ActivePositions.Sum(position => position.Price);

        public decimal DeletedPositionsPrice => DeletedPositions.Sum(position => position.Price);
    }
}