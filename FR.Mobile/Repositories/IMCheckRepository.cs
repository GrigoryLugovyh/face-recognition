﻿using System.Collections.Generic;
using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMCheckRepository : IMRepository<MCheck>
    {
        IEnumerable<MCheckPosition> Positions(int checkId);

        IEnumerable<MCheckPayment> Payments(int checkId);

        IEnumerable<MCheckBonusCard> BonusCards(int checkId);

        IEnumerable<MCheckDiscountCard> DiscountCards(int checkId);
    }
}