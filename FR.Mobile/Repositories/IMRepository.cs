﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FR.Mobile.Models;
using SQLite;

namespace FR.Mobile.Repositories
{
    public interface IMRepository<TEntity> where TEntity : MEntity
    {
        TableQuery<TEntity> AsQuery();

        IEnumerable<TEntity> Select(Expression<Func<TEntity, bool>> condition);

        IEnumerable<TEntity> SelectAll();

        TEntity Read(int id);

        void Save(TEntity entity);

        int Insert(TEntity entity);

        int InsertAll(IEnumerable<TEntity> entities, bool inTransaction = true);

        int Update(TEntity entity);

        void Delete(TEntity entity);

        void Delete(int entityId);

        void Drop();
    }
}