﻿using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMImageRepository : IMRepository<MImage>
    {
    }
}