﻿using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public abstract class MCheckEntityRepository<TEntity> : MRepository<TEntity>, IMCheckEntityRepository<TEntity>
        where TEntity : MCheckEntity, new()
    {
        public IEnumerable<TEntity> Foreigners(int checkId)
        {
            return AsQuery().Where(entity => entity.CheckId == checkId).AsEnumerable();
        }
    }
}