﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public class MCheckRepository : MRepository<MCheck>, IMCheckRepository
    {
        private readonly Lazy<IMCheckBonusCardRepository> checkBonuscardRepository =
            new Lazy<IMCheckBonusCardRepository>(Container.Resolve<IMCheckBonusCardRepository>);

        private readonly Lazy<IMCheckDiscountCardRepository> checkDiscountcardRepository =
            new Lazy<IMCheckDiscountCardRepository>(Container.Resolve<IMCheckDiscountCardRepository>);

        private readonly Lazy<IMCheckPaymentRepository> checkPaymentRepository =
            new Lazy<IMCheckPaymentRepository>(Container.Resolve<IMCheckPaymentRepository>);

        private readonly Lazy<IMCheckPositionRepository> checkPositionRepository =
            new Lazy<IMCheckPositionRepository>(Container.Resolve<IMCheckPositionRepository>);

        public IEnumerable<MCheckPosition> Positions(int checkId)
        {
            return checkPositionRepository.Value.AsQuery().Where(position => position.CheckId == checkId)
                .AsEnumerable();
        }

        public IEnumerable<MCheckPayment> Payments(int checkId)
        {
            return checkPaymentRepository.Value.AsQuery().Where(payment => payment.CheckId == checkId).AsEnumerable();
        }

        public IEnumerable<MCheckBonusCard> BonusCards(int checkId)
        {
            return checkBonuscardRepository.Value.AsQuery().Where(card => card.CheckId == checkId);
        }

        public IEnumerable<MCheckDiscountCard> DiscountCards(int checkId)
        {
            return checkDiscountcardRepository.Value.AsQuery().Where(card => card.CheckId == checkId);
        }

        public override void Delete(MCheck check)
        {
            Run(connection =>
            {
                connection.RunInTransaction(() =>
                {
                    foreach (var position in Positions(check.Id))
                    {
                        checkPositionRepository.Value.Delete(position);
                    }

                    foreach (var payment in Payments(check.Id))
                    {
                        checkPaymentRepository.Value.Delete(payment);
                    }

                    foreach (var bonusCard in BonusCards(check.Id))
                    {
                        checkBonuscardRepository.Value.Delete(bonusCard);
                    }

                    foreach (var discountCard in DiscountCards(check.Id))
                    {
                        checkDiscountcardRepository.Value.Delete(discountCard);
                    }

                    connection.Delete(check);
                });
            });
        }
    }
}