﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FR.Mobile.Extensions;
using FR.Mobile.Infrastructure.Database;
using FR.Mobile.Models;
using FR.Mobile.Services;
using FR.Mobile.Session;
using FR.Mobile.Storage;
using SQLite;

namespace FR.Mobile.Repositories
{
    public class MRepository<TEntity> : IMRepository<TEntity> where TEntity : MEntity, new()
    {
        private readonly Lazy<IDatabaseConnector> connector =
            new Lazy<IDatabaseConnector>(Container.Resolve<IDatabaseConnector>);

        private readonly Lazy<IDateTimeService> datetimeService =
            new Lazy<IDateTimeService>(Container.Resolve<IDateTimeService>);

        private readonly Lazy<IPreferences> preferences = new Lazy<IPreferences>(Container.Resolve<IPreferences>);

        private readonly Lazy<ISessionManager> sessionManager =
            new Lazy<ISessionManager>(Container.Resolve<ISessionManager>);

        public MRepository()
        {
            Run(connection => connection.CreateTable<TEntity>());
        }

        public TableQuery<TEntity> AsQuery()
        {
            return Run(connection => connection.Table<TEntity>());
        }

        public IEnumerable<TEntity> Select(Expression<Func<TEntity, bool>> condition)
        {
            return Run(connection => connection.Table<TEntity>().Where(condition));
        }

        public IEnumerable<TEntity> SelectAll()
        {
            return Select(entity => true);
        }

        public TEntity Read(int id)
        {
            return Run(connection => connection.Table<TEntity>().FirstOrDefault(entity => entity.Id == id));
        }

        public void Save(TEntity entity)
        {
            Run(connection =>
            {
                switch (entity.Id)
                {
                    case 0:
                        Insert(entity);
                        break;
                    default:
                        Update(entity);
                        break;
                }
            });
        }

        public int Insert(TEntity entity)
        {
            return Run(connection =>
            {
                Prepare(ref entity);
                return connection.Insert(entity);
            });
        }

        public int InsertAll(IEnumerable<TEntity> entities, bool inTransaction = true)
        {
            return Run(connection =>
            {
                return connection.InsertAll(entities.Select(entity =>
                {
                    Prepare(ref entity);
                    return entity;
                }), inTransaction);
            });
        }

        public int Update(TEntity entity)
        {
            return Run(connection =>
            {
                entity.UpdatedTimeStamp = datetimeService.Value.UtcNow;
                return connection.Update(entity);
            });
        }

        public virtual void Delete(TEntity entity)
        {
            Run(connection => connection.Delete(entity));
        }

        public void Delete(int entityId)
        {
            Run(connection => connection.Delete(entityId));
        }

        public void Drop()
        {
            Run(connection =>
            {
                connection.DropTable<TEntity>();
                connection.CreateTable<TEntity>();
            });
        }

        protected void Run(Action<SQLiteConnection> distributor)
        {
            try
            {
                lock (DatabaseConnector.ConnectorLockContext)
                {
                    distributor(connector.Value.Connection);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        protected TOut Run<TOut>(Func<SQLiteConnection, TOut> distributor)
        {
            try
            {
                lock (DatabaseConnector.ConnectorLockContext)
                {
                    return distributor(connector.Value.Connection);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);

                return default(TOut);
            }
        }

        protected void Prepare(ref TEntity entity)
        {
            entity.DeviceId = preferences.Value.DeviveId;
            entity.DeviceTimeStamp = datetimeService.Value.Now;
            entity.CreatedTimeStamp = datetimeService.Value.UtcNow;

            if (!entity.SessionId.HasValue())
                entity.SessionId = sessionManager.Value.GetCurrentSessionId;
        }
    }
}