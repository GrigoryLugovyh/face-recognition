﻿using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMEventRepository : IMRepository<MEvent>
    {
    }
}