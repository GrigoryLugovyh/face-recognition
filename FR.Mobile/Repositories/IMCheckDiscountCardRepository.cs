﻿using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMCheckDiscountCardRepository : IMCheckEntityRepository<MCheckDiscountCard>
    {
    }
}