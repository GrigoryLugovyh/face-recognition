﻿using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMCheckBonusCardRepository : IMCheckEntityRepository<MCheckBonusCard>
    {
    }

    public interface IMCheckPaymentRepository : IMCheckEntityRepository<MCheckPayment>
    {
    }
}