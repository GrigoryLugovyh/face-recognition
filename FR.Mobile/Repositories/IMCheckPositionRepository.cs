﻿using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMCheckPositionRepository : IMCheckEntityRepository<MCheckPosition>
    {
    }
}