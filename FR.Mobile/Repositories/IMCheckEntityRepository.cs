﻿using System.Collections.Generic;
using FR.Mobile.Models;

namespace FR.Mobile.Repositories
{
    public interface IMCheckEntityRepository<TEntity> : IMRepository<TEntity> where TEntity : MCheckEntity
    {
        IEnumerable<TEntity> Foreigners(int checkId);
    }
}