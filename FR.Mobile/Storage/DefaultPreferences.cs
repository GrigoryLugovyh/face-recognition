﻿using System;
using System.Collections.Concurrent;
using FR.Mobile.App;
using FR.Mobile.Extensions;

namespace FR.Mobile.Storage
{
    public class DefaultPreferences : IPreferences
    {
        public const string DeviveIdKey = nameof(IAppSettings.DeviveId);
        public const string ImageQualityKey = nameof(IAppSettings.ImageQuality);
        public const string ServerAddressKey = nameof(IAppSettings.ServerAddress);
        public const string LoggingEnableKey = nameof(IAppSettings.LoggingEnable);
        public const string VoiceOverEnableKey = nameof(IAppSettings.VoiceOverEnable);
        public const string AdvertisingEnableKey = nameof(IAppSettings.AdvertisingEnable);
        public const string CameraPreviewEnableKey = nameof(IAppSettings.CameraPreviewEnable);

        private readonly Lazy<IAppSettings> appSettings = new Lazy<IAppSettings>(Container.Resolve<IAppSettings>);

        private readonly ConcurrentDictionary<string, string> preferences = new ConcurrentDictionary<string, string>();

        [Obsolete]
        public int ImageQuality
        {
            get
            {
                int.TryParse(GetValue(ImageQualityKey), out var imageQualityKey);
                if (imageQualityKey <= 0 || imageQualityKey > 100)
                    imageQualityKey = 70;

                return imageQualityKey;
            }
        }

        public bool CameraPreviewEnable => GetBoolValue(CameraPreviewEnableKey);

        public bool LoggingEnable => GetBoolValue(LoggingEnableKey);

        public bool AdvertisingEnable => GetBoolValue(AdvertisingEnableKey);

        public bool VoiceOverEnable => GetBoolValue(VoiceOverEnableKey);

        public string DeviveId => GetValue(DeviveIdKey);

        public string ServerAddress => GetValue(ServerAddressKey);

        public virtual void SetValue(string key, string value)
        {
            preferences.AddOrUpdate(key, value, (k, v) => value);
        }

        public virtual string GetValue(string key)
        {
            return preferences.GetOrAdd(key, string.Empty);
        }

        public bool CanInit => true;

        public virtual void Init()
        {
            foreach (var value in appSettings.Value.AllValues)
                if (!GetValue(value.Key).HasValue())
                    SetValue(value.Key, value.Value);
        }

        protected bool GetBoolValue(string key)
        {
            bool.TryParse(GetValue(key), out var value);
            return value;
        }
    }
}