﻿namespace FR.Mobile.Storage
{
    public interface IPreferences : IInitiable
    {
        bool CameraPreviewEnable { get; }

        bool AdvertisingEnable { get; }

        bool VoiceOverEnable { get; }

        int ImageQuality { get; }

        bool LoggingEnable { get; }

        string DeviveId { get; }

        string ServerAddress { get; }

        void SetValue(string key, string value);

        string GetValue(string key);
    }
}