﻿namespace FR.Mobile
{
    public interface IStopable
    {
        void Stop();
    }
}