﻿using System.Collections.Generic;
using FR.Mobile.Advertising;
using FR.Mobile.Cash;
using FR.Mobile.Cash.Strategy;
using FR.Mobile.Events;
using FR.Mobile.Executors;
using FR.Mobile.Executors.Commands.Checks;
using FR.Mobile.Executors.Commands.Events;
using FR.Mobile.Executors.Commands.Faces;
using FR.Mobile.Infrastructure.Connectivity;
using FR.Mobile.Infrastructure.Database;
using FR.Mobile.Infrastructure.FileSystem;
using FR.Mobile.Infrastructure.Speech;
using FR.Mobile.Listener;
using FR.Mobile.Listener.Prisma;
using FR.Mobile.Mapper;
using FR.Mobile.Models;
using FR.Mobile.Repositories;
using FR.Mobile.Services;
using FR.Mobile.Session;
using FR.Mobile.Storage;
using FR.Mobile.Vision;
using FR.Model.Dto;

namespace FR.Mobile.App
{
    public class AppModel : IAppModel
    {
        private static readonly object modelRunLockContext = new object();

        private volatile bool initialized;

        public bool CanInit => true;

        public void Init()
        {
            lock (modelRunLockContext)
            {
                if (initialized) return;

                Register();
                Prepare();

                initialized = true;
            }
        }

        public static IAppModel Instance<TModel>() where TModel : IAppModel, new()
        {
            lock (modelRunLockContext)
            {
                TModel model;

                if (Container.IsRegistred(typeof(IAppModel)))
                {
                    model = (TModel) Container.Resolve<IAppModel>();
                }
                else
                {
                    model = new TModel();
                    model.Init();
                    Container.AddRegistration<IAppModel>(model);
                }

                return model;
            }
        }

        protected virtual void Register()
        {
            RegisterMappers();
            RegisterAdvertising();
            RegisterInfrastructure();
        }

        protected virtual void Prepare()
        {
            Container.Resolve<IAppDispatcher>().Init();
        }

        protected virtual void RegisterMappers()
        {
            Container.AddRegistration<IMapper<IEnumerable<MEvent>, IEnumerable<EventDto>>, EventsDtoMapper>()
                .AsMultiInstance();
            Container.AddRegistration<IMapper<MCheckPosition, CheckPositionDto>, CheckPositionDtoMapper>()
                .AsMultiInstance();
            Container.AddRegistration<IMapper<string, MCheckBonusCard>, CheckBonusCardMapper>().AsMultiInstance();
            Container.AddRegistration<IMapper<string, MCheckDiscountCard>, CheckDiscountCardMapper>().AsMultiInstance();
            Container.AddRegistration<IMapper<string, MCheckPayment>, CheckPaymentMapper>().AsMultiInstance();
            Container.AddRegistration<IMapper<CheckPosition, MCheckPosition>, CheckPositionMapper>().AsMultiInstance();
            Container
                .AddRegistration<
                    IMapper<(MCheck, IEnumerable<MCheckPosition>, IEnumerable<MCheckPayment>,
                        IEnumerable<MCheckBonusCard>, IEnumerable<MCheckDiscountCard>), CheckDto>, CheckDtoMapper>()
                .AsMultiInstance();
            Container
                .AddRegistration<
                    IMapper<Check, (MCheck, IEnumerable<MCheckPosition>, IEnumerable<MCheckPayment>,
                        IEnumerable<MCheckBonusCard>, IEnumerable<MCheckDiscountCard>)>, CheckMapper>()
                .AsMultiInstance();
        }

        protected virtual void RegisterAdvertising()
        {
            var advertisingProvider = CreateAdvertisingProvider();
            Container.AddRegistration(advertisingProvider);

            var advertisingRegistry = CreateAdvertisingRegistry();
            Container.AddRegistration(advertisingRegistry);

            var textToSpeech = CreateTextToSpeech();
            Container.AddRegistration(textToSpeech);

            var textToSpeechProvider = CreateTextToSpeechProvider();
            Container.AddRegistration(textToSpeechProvider);
        }

        protected virtual void RegisterInfrastructure()
        {
            InitDateTimeService();
            InitAppSettings();

            Container.AddRegistration(CreateSyncService());
            Container.AddRegistration(CreateLoggerService());
            Container.AddRegistration(CreateSessionManager());
            Container.AddRegistration(CreateGlobalEventBus());

            InitPrismaExternalEventDispatcher();
            InitVisionDispatcher();
            InitInfrastructure();
            InitAppDispatcher();
            InitCheckManager();
            InitPreferences();
            InitExecutors();
        }

        protected virtual void InitDateTimeService()
        {
            Container.AddRegistration<IDateTimeService, DateTimeService>().AsMultiInstance();
        }

        protected virtual void InitAppSettings()
        {
            Container.AddRegistration<IAppSettings, AppSettings>().AsMultiInstance();
        }

        protected virtual ICommandQueueHandler InitExecutors()
        {
            Container.AddRegistration(typeof(ICommandSender<>), typeof(CommandSender<>)).AsMultiInstance();

            Container.AddRegistration<ICommandSaver<EventsCommand>, EventsCommandSaver>().AsMultiInstance();
            Container.AddRegistration<ICommandExecutor<EventsCommand>, EventsCommandExecutor>().AsMultiInstance();

            Container.AddRegistration<ICommandSaver<CheckCommand>, CheckCommandSaver>().AsMultiInstance();
            Container.AddRegistration<ICommandExecutor<CheckCommand>, CheckCommandExecutor>().AsMultiInstance();

            Container.AddRegistration<ICommandSaver<FaceUploadCommand>, FaceUploadCommandSaver>().AsMultiInstance();
            Container.AddRegistration<ICommandExecutor<FaceUploadCommand>, FaceUploadCommandExecutor>()
                .AsMultiInstance();

            var commandGenerator = CreateCommandGenerator();
            Container.AddRegistration(commandGenerator);

            var commandQueueHandler = CreateClientCommandQueueHandler();
            Container.AddRegistration<IStartable>(commandQueueHandler);
            Container.AddRegistration(commandQueueHandler);
            commandQueueHandler.Start();

            return commandQueueHandler;
        }

        protected virtual IPreferences InitPreferences()
        {
            var preferences = CreatePreferences();
            Container.AddRegistration<IInitiable>(preferences);
            Container.AddRegistration(preferences);
            preferences.Init();
            return preferences;
        }

        protected virtual IVisionDispatcher InitVisionDispatcher()
        {
            var visionDispatcher = CreateVisionDispatcher();
            Container.AddRegistration<IManage>(visionDispatcher);
            Container.AddRegistration(visionDispatcher);
            return visionDispatcher;
        }

        protected virtual IAppDispatcher InitAppDispatcher()
        {
            var appDispatcher = CreateAppDispatcher();
            Container.AddRegistration<IManage>(appDispatcher);
            Container.AddRegistration(appDispatcher);
            return appDispatcher;
        }

        protected virtual IExternalEventDispatcher InitPrismaExternalEventDispatcher()
        {
            Container.AddRegistration<IExternalEventInterpretator, PrismaEventInterpretator>().AsMultiInstance();
            Container.AddRegistration<IExternalEventDistributor, PrismaEventDistributor>().AsMultiInstance();

            var prismaExternalEventDispatcher = CreatePrismaExternalEventDispatcher();
            Container.AddRegistration(prismaExternalEventDispatcher);
            return prismaExternalEventDispatcher;
        }

        protected virtual ICheckManager InitCheckManager()
        {
            Container.AddRegistration<IOpenCheckStrategy, OpenCheckStrategy>().AsMultiInstance();
            Container.AddRegistration<ICloseCheckStrategy, CloseCheckStrategy>().AsMultiInstance();
            Container.AddRegistration<ISubtotalCheckStrategy, SubtotalCheckStrategy>().AsMultiInstance();
            Container.AddRegistration<IChangePositionCheckStrategy, ChangePositionCheckStrategy>().AsMultiInstance();

            var checkManager = CreateCheckManager();
            Container.AddRegistration<IManage>(checkManager);
            Container.AddRegistration(checkManager);
            return checkManager;
        }

        protected virtual void InitInfrastructure()
        {
            var wifiObserver = CreateWifiStateObserver();
            Container.AddRegistration(wifiObserver);

            var fileSystem = CreateFileSystem();
            Container.AddRegistration(fileSystem);

            var fileLoader = CreateFileLoader();
            Container.AddRegistration(fileLoader);

            var databaseConnector = CreateDatabaseConnector();
            Container.AddRegistration(databaseConnector);

            InitRepositories();
        }

        protected virtual void InitRepositories()
        {
            Container.AddRegistration<IMEventRepository, IMRepository<MEvent>, MEventRepository>();
            Container.AddRegistration<IMCheckRepository, IMRepository<MCheck>, MCheckRepository>();
            Container.AddRegistration<IMImageRepository, IMRepository<MImage>, MImageRepository>();
            Container
                .AddRegistration<IMCheckPaymentRepository, IMCheckEntityRepository<MCheckPayment>,
                    MCheckPaymentRepository>();
            Container
                .AddRegistration<IMCheckPositionRepository, IMCheckEntityRepository<MCheckPosition>,
                    MCheckPositionRepository>();
            Container
                .AddRegistration<IMCheckBonusCardRepository, IMCheckEntityRepository<MCheckBonusCard>,
                    MCheckBonusCardRepository>();
            Container
                .AddRegistration<IMCheckDiscountCardRepository, IMCheckEntityRepository<MCheckDiscountCard>,
                    MCheckDiscountCardRepository>();
        }

        protected virtual ISyncService CreateSyncService()
        {
            return new SyncService();
        }

        protected virtual IEventService CreateLoggerService()
        {
            return new EventService();
        }

        protected virtual IGlobalEvents CreateGlobalEventBus()
        {
            return new GlobalEvents();
        }

        protected virtual ICommandGenerator CreateCommandGenerator()
        {
            return new CommandGenerator();
        }

        protected virtual ICommandQueueHandler CreateClientCommandQueueHandler()
        {
            return new CommandQueueHandler();
        }

        protected virtual ISessionManager CreateSessionManager()
        {
            return new SessionManager();
        }

        protected virtual IPreferences CreatePreferences()
        {
            return new DefaultPreferences();
        }

        protected virtual IVisionDispatcher CreateVisionDispatcher()
        {
            return new DefaultVisionDispatcher();
        }

        protected virtual IAppDispatcher CreateAppDispatcher()
        {
            return new AppDispatcher();
        }

        protected virtual IExternalEventDispatcher<PrismaEventDistributor, PrismaEventInterpretator>
            CreatePrismaExternalEventDispatcher()
        {
            return new ExternalEventDispatcher<PrismaEventDistributor, PrismaEventInterpretator>();
        }

        protected virtual IWifiStateObserver CreateWifiStateObserver()
        {
            return new WifiStateObserver();
        }

        protected virtual ICheckManager CreateCheckManager()
        {
            return new CheckManager();
        }

        protected virtual IFileSystem CreateFileSystem()
        {
            return new FileSystem();
        }

        protected virtual IFileLoader CreateFileLoader()
        {
            return new FileLoader();
        }

        protected virtual IDatabaseConnector CreateDatabaseConnector()
        {
            return new DatabaseConnector();
        }

        protected virtual IAdvertisingProvider CreateAdvertisingProvider()
        {
            return new AdvertisingProvider();
        }

        protected virtual IAdvertisingRegistry CreateAdvertisingRegistry()
        {
            return new AdvertisingRegistry();
        }

        protected virtual ITextToSpeech CreateTextToSpeech()
        {
            return new DefaultTextToSpeech();
        }

        protected virtual ITextToSpeechProvider CreateTextToSpeechProvider()
        {
            return new TextToSpeechProvider();
        }
    }
}