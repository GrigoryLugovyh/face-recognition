﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FR.Mobile.App
{
    public class AppSettings : IAppSettings
    {
        public virtual string DeviveId => Guid.NewGuid().ToString();

        public virtual int ImageQuality => 70;

#if DEBUG
        public virtual string ServerAddress => "http://192.168.0.206:8596";
#else
        public virtual string ServerAddress => "http://geonosis.northeurope.cloudapp.azure.com:8596";
#endif

        public virtual bool CameraPreviewEnable => false;

        public virtual bool LoggingEnable => true;

        public virtual bool AdvertisingEnable => true;

        public virtual bool VoiceOverEnable => true;

        public IEnumerable<(string Key, string Value)> AllValues => GetType()
            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Select(info => (info.Name, info.GetValue(this).ToString()));
    }
}