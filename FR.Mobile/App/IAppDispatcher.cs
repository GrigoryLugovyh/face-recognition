﻿using System;
using FR.Mobile.Cash;
using FR.Mobile.Vision;

namespace FR.Mobile.App
{
    public interface IAppDispatcher : IManage
    {
        IVisionDispatcher VisionDispatcher { get; }

        IExternalEventDispatcher ExternalEventDispatcher { get; }

        ICheckManager CheckManager { get; }

        void AddConponent(IManage component);

        event EventHandler<Check> StartSales;

        event EventHandler<Check> ChangeSale;

        event EventHandler<Check> FinishSales;
    }
}