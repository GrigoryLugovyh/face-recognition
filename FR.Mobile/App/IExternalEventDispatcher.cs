﻿using System;
using FR.Mobile.Listener;

namespace FR.Mobile.App
{
    public interface IExternalEventDispatcher : IManage
    {
        event EventHandler<ExternalEvent> StartSales;

        event EventHandler<ExternalEvent> ChangeSale;

        event EventHandler<ExternalEvent> SubtotSales;

        event EventHandler<ExternalEvent> FinishSales;
    }
}