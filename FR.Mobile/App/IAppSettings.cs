﻿using System.Collections.Generic;

namespace FR.Mobile.App
{
    public interface IAppSettings
    {
        string DeviveId { get; }

        int ImageQuality { get; }

        string ServerAddress { get; }

        bool CameraPreviewEnable { get; }

        bool LoggingEnable { get; }

        bool AdvertisingEnable { get; }

        bool VoiceOverEnable { get; }

        IEnumerable<(string Key, string Value)> AllValues { get; }
    }
}