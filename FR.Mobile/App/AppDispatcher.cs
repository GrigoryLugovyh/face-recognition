﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Mobile.Advertising;
using FR.Mobile.Cash;
using FR.Mobile.Executors;
using FR.Mobile.Infrastructure.Connectivity;
using FR.Mobile.Infrastructure.Speech;
using FR.Mobile.Listener;
using FR.Mobile.Listener.Prisma;
using FR.Mobile.Session;
using FR.Mobile.Vision;

namespace FR.Mobile.App
{
    public class AppDispatcher : IAppDispatcher
    {
        private readonly List<IManage> appComponents = new List<IManage>();
        private readonly object appLockContext = new object();

        private readonly Lazy<ICommandGenerator> commandGenerator =
            new Lazy<ICommandGenerator>(Container.Resolve<ICommandGenerator>);

        private readonly Lazy<ISessionManager> sessionManager =
            new Lazy<ISessionManager>(Container.Resolve<ISessionManager>);

        private readonly Lazy<IWifiStateObserver> networkStateObserver =
            new Lazy<IWifiStateObserver>(Container.Resolve<IWifiStateObserver>);

        private readonly Lazy<IAdvertisingProvider> advertisingProvider =
            new Lazy<IAdvertisingProvider>(Container.Resolve<IAdvertisingProvider>);

        private readonly Lazy<ITextToSpeechProvider> textToSpeechProvider =
            new Lazy<ITextToSpeechProvider>(Container.Resolve<ITextToSpeechProvider>);

        public IVisionDispatcher VisionDispatcher => !appComponents.Any()
            ? null
            : (IVisionDispatcher) appComponents.First(manage => manage is IVisionDispatcher);

        public IExternalEventDispatcher ExternalEventDispatcher => !appComponents.Any()
            ? null
            : (IExternalEventDispatcher) appComponents.First(manage => manage is IExternalEventDispatcher);

        public ICheckManager CheckManager => !appComponents.Any()
            ? null
            : (ICheckManager) appComponents.First(manage => manage is ICheckManager);

        public void AddConponent(IManage component)
        {
            appComponents.Add(component);
        }

        public bool CanInit => true;

        public virtual void Init()
        {
            appComponents.Add(GetCheckManager());
            appComponents.Add(GetVisionDispatcher());
            appComponents.Add(GetTextToSpeechProvider());
            appComponents.Add(GetExternalEventDispatcher());

            appComponents.ForEach(manage =>
            {
                if (manage.CanInit)
                    manage.Init();
            });
        }

        public void Start()
        {
            appComponents.ForEach(manage => manage.Start());

            ExternalEventDispatcher.ChangeSale += EventDispatcherOnAddSale;
            ExternalEventDispatcher.StartSales += EventDispatcherOnStartSales;
            ExternalEventDispatcher.FinishSales += EventDispatcherOnFinishSales;
            ExternalEventDispatcher.SubtotSales += ExternalEventDispatcherOnSubtotSales;

            CheckManager.CheckOpened += CheckManagerOnCheckOpened;
            CheckManager.CheckClosed += CheckManagerOnCheckClosed;
            CheckManager.CheckChanged += CheckManagerOnCheckChanged;

            networkStateObserver.Value.Disconnected += NetworkOnDisconnected;

            Logger.Info("Started");
        }

        public void Stop()
        {
            ExternalEventDispatcher.ChangeSale -= EventDispatcherOnAddSale;
            ExternalEventDispatcher.StartSales -= EventDispatcherOnStartSales;
            ExternalEventDispatcher.FinishSales -= EventDispatcherOnFinishSales;
            ExternalEventDispatcher.SubtotSales -= ExternalEventDispatcherOnSubtotSales;

            CheckManager.CheckOpened -= CheckManagerOnCheckOpened;
            CheckManager.CheckClosed -= CheckManagerOnCheckClosed;
            CheckManager.CheckChanged -= CheckManagerOnCheckChanged;

            networkStateObserver.Value.Disconnected -= NetworkOnDisconnected;

            appComponents.ForEach(manage => manage.Stop());

            Logger.Info("Stopped");
        }

        public void Release()
        {
            ExternalEventDispatcher.ChangeSale -= EventDispatcherOnAddSale;
            ExternalEventDispatcher.StartSales -= EventDispatcherOnStartSales;
            ExternalEventDispatcher.FinishSales -= EventDispatcherOnFinishSales;
            ExternalEventDispatcher.SubtotSales -= ExternalEventDispatcherOnSubtotSales;

            CheckManager.CheckOpened -= CheckManagerOnCheckOpened;
            CheckManager.CheckClosed -= CheckManagerOnCheckClosed;
            CheckManager.CheckChanged -= CheckManagerOnCheckChanged;

            appComponents.ForEach(manage => manage.Release());

            Logger.Info("Released");
        }

        public event EventHandler<Check> StartSales;
        public event EventHandler<Check> ChangeSale;
        public event EventHandler<Check> FinishSales;

        /// <summary>
        ///     Старт продаж
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="externalEvent"></param>
        private void EventDispatcherOnStartSales(object sender, ExternalEvent externalEvent)
        {
            lock (appLockContext)
            {
                Logger.Info("Start of sales raising");

                sessionManager.Value.StartNewIfNeed();
                advertisingProvider.Value.Start();
                CheckManager.Open();

                Logger.Info("Start of sales raised");
            }
        }

        /// <summary>
        ///     Добавление новой продажи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="externalEvent"></param>
        private void EventDispatcherOnAddSale(object sender, ExternalEvent externalEvent)
        {
            lock (appLockContext)
            {
                Logger.Info("Add sales raising");

                sessionManager.Value.StartNewIfNeed();
                CheckManager.ChangePosition(externalEvent);

                Logger.Info("Add sales raised");
            }
        }

        /// <summary>
        ///     Подитог
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="externalEvent"></param>
        private void ExternalEventDispatcherOnSubtotSales(object sender, ExternalEvent externalEvent)
        {
            lock (appLockContext)
            {
                Logger.Info("Subtotal raising");

                CheckManager.SubTotal(externalEvent);
                textToSpeechProvider.Value.SpeakAsSubtotal(externalEvent.Total);

                Logger.Info("Subtotal raised");
            }
        }

        /// <summary>
        ///     Завершение продаж
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="externalEvent"></param>
        private void EventDispatcherOnFinishSales(object sender, ExternalEvent externalEvent)
        {
            lock (appLockContext)
            {
                Logger.Info("Finish of sales raising");

                CheckManager.Close();
                advertisingProvider.Value.Stop();
                sessionManager.Value.FinishCurrent();

                Logger.Info("Finish of sales raised");
            }
        }

        private void NetworkOnDisconnected(object sender, EventArgs eventArgs)
        {
            EventDispatcherOnFinishSales(sender, new ExternalEvent());
        }

        private void CheckManagerOnCheckOpened(object sender, Check check)
        {
            StartSales?.Invoke(this, check);
        }

        private void CheckManagerOnCheckChanged(object sender, Check check)
        {
            ChangeSale?.Invoke(this, check);
        }

        private void CheckManagerOnCheckClosed(object sender, Check check)
        {
            lock (appLockContext)
            {
                commandGenerator.Value.Enqueue(check);
            }

            FinishSales?.Invoke(this, check);
        }

        protected virtual IManage GetExternalEventDispatcher()
        {
            return Container.Resolve<IExternalEventDispatcher<PrismaEventDistributor, PrismaEventInterpretator>>();
        }

        protected virtual IManage GetVisionDispatcher()
        {
            return Container.Resolve<IVisionDispatcher>();
        }

        protected virtual IManage GetCheckManager()
        {
            return Container.Resolve<ICheckManager>();
        }

        protected virtual IManage GetTextToSpeechProvider()
        {
            return Container.Resolve<ITextToSpeech>();
        }
    }
}