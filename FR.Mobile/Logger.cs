﻿using System;
using System.Runtime.CompilerServices;
using FR.Mobile.Extensions;
using FR.Mobile.Services;

namespace FR.Mobile
{
    public class Logger
    {
        private static readonly Lazy<IEventService> instance =
            new Lazy<IEventService>(Container.Resolve<IEventService>);

        public static void Info(string message, [CallerFilePath] string file = "",
            [CallerMemberName] string method = "")
        {
            instance.Value.Info(message, $"{file}:{method}");
        }

        public static void Warn(string message, [CallerFilePath] string file = "",
            [CallerMemberName] string method = "")
        {
            instance.Value.Warn(message, $"{file}:{method}");
        }

        public static void Error(string message, [CallerFilePath] string file = "",
            [CallerMemberName] string method = "")
        {
            instance.Value.Error(message, $"{file}:{method}");
        }

        public static void Error(Exception e, [CallerFilePath] string file = "", [CallerMemberName] string method = "")
        {
            instance.Value.Error(e.Expand(), $"{file}:{method}");
        }
    }
}