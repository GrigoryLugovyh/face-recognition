﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using FR.Mobile.Services.SyncActions;

namespace FR.Mobile.Services
{
    public class SyncService : ISyncService
    {
        private static readonly IReadOnlyCollection<ISyncAction> syncActions = new ReadOnlyCollection<ISyncAction>(
            new List<ISyncAction>
            {
                new PushImagesSyncAction(),
                new PushChecksSyncAction(),
                new PushEventsSyncAction()
            });

        private readonly object syncLockContext = new object();

        public void Run()
        {
            if (!Monitor.TryEnter(syncLockContext))
            {
                Logger.Warn("Synchronization already started");
                return;
            }

            Logger.Info("Synchronization was started");

            var startTimeStamp = Environment.TickCount;

            try
            {
                foreach (var syncAction in syncActions)
                    syncAction.Invoke();
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                Monitor.Exit(syncLockContext);

                Logger.Info($"Synchronization was finished from {Environment.TickCount - startTimeStamp} ms");
            }
        }
    }
}