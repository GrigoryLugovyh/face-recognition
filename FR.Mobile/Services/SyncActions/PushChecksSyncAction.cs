﻿using FR.Mobile.Executors.Commands.Checks;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Services.SyncActions
{
    public class PushChecksSyncAction : SyncAction<MCheckRepository, MCheck, CheckCommand>
    {
        public override string Name => "Выгрузка чеков";

        public override void Execute()
        {
            foreach (var check in Repository.AsQuery())
            {
                var command = new CheckCommand();

                var positions = Repository.Positions(check.Id);
                var payments = Repository.Payments(check.Id);
                var bonuscards = Repository.BonusCards(check.Id);
                var discountrcards = Repository.DiscountCards(check.Id);

                command.Value = (check, positions, payments, bonuscards, discountrcards);

                if (Sender.Send(command))
                    Repository.Delete(check);
            }
        }
    }
}