﻿using System;
using System.IO;
using FR.Mobile.Executors.Commands.Faces;
using FR.Mobile.Infrastructure.FileSystem;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Services.SyncActions
{
    public class PushImagesSyncAction : SyncAction<MImageRepository, MImage, FaceUploadCommand>
    {
        private readonly Lazy<IFileSystem> fileSystem = new Lazy<IFileSystem>(Container.Resolve<IFileSystem>);

        public override string Name => "Выгрузка файлов изображений";

        public override void Execute()
        {
            foreach (var image in Repository.AsQuery())
            {
                Logger.Info($"Sending image with name \"{image.Name}\" ...");

                var imageFilePath = Path.Combine(fileSystem.Value.ApplicationPhotoImagesDirectory, image.Name);

                if (File.Exists(imageFilePath))
                {
                    var command = new FaceUploadCommand
                    {
                        SessionId = image.SessionId,
                        ImageData = File.ReadAllBytes(imageFilePath)
                    };

                    if (!Sender.Send(command))
                    {
                        Logger.Warn($"Cannot send image \"{image.Name}\"");
                        return;
                    }

                    File.Delete(imageFilePath);

                    Logger.Info($"Image sended \"{image.Name}\"");
                }

                Repository.Delete(image);
            }
        }
    }
}