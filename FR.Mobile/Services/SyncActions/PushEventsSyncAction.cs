﻿using FR.Mobile.Executors.Commands.Events;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Services.SyncActions
{
    public class PushEventsSyncAction : PushPotrionSyncAction<MEventRepository, MEvent, EventsCommand>
    {
        public override string Name => "Выгрузка событий устройства";

        protected override bool ProcessPortion(MEvent[] portion)
        {
            return Sender.Send(new EventsCommand
            {
                Value = portion
            });
        }
    }
}