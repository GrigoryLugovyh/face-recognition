﻿using System;
using FR.Mobile.Executors;
using FR.Mobile.Executors.Commands;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Services.SyncActions
{
    public abstract class SyncAction<TRepository, TEntity, TCommand> : ISyncAction<TRepository, TEntity, TCommand>
        where TEntity : MEntity where TRepository : IMRepository<TEntity> where TCommand : IFrCommand
    {
        public TRepository Repository => (TRepository) Container.Resolve<IMRepository<TEntity>>();

        public ICommandSender<TCommand> Sender => Container.Resolve<ICommandSender<TCommand>>();

        public abstract string Name { get; }

        public void Invoke()
        {
            try
            {
                Logger.Info($"Sync action \"{Name}\" starting ...");

                Execute();
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                Logger.Info($"Sync action was \"{Name}\" finished");
            }
        }

        public abstract void Execute();
    }
}