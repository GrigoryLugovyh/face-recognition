﻿using System.Linq;
using FR.Mobile.Executors.Commands;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Services.SyncActions
{
    public abstract class PushPotrionSyncAction<TRepository, TEntity, TCommand> : SyncAction<TRepository, TEntity, TCommand>
        where TEntity : MEntity where TRepository : IMRepository<TEntity> where TCommand : IFrCommand
    {
        protected virtual int PortionSize => 100;

        protected virtual TEntity[] ExtractPortion(int takeCount, int skipCount)
        {
            return Repository.AsQuery().Take(takeCount).Skip(skipCount).ToArray();
        }

        protected abstract bool ProcessPortion(TEntity[] portion);

        public override void Execute()
        {
            var skipCount = 0;

            var portion = ExtractPortion(PortionSize, skipCount);

            while (portion.Any())
            {
                if (!ProcessPortion(portion))
                {
                    Logger.Warn($"\"{Name}\" portion was unloaded unsuccessfully");
                    return;
                }

                foreach (var entity in portion)
                {
                    Repository.Delete(entity);
                }

                skipCount += PortionSize;

                portion = ExtractPortion(PortionSize, skipCount);
            }
        }
    }
}