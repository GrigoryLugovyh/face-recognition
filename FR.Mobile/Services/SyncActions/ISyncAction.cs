﻿using System;
using FR.Mobile.Executors;
using FR.Mobile.Executors.Commands;
using FR.Mobile.Models;
using FR.Mobile.Repositories;

namespace FR.Mobile.Services.SyncActions
{
    public interface ISyncAction
    {
        string Name { get; }

        void Invoke();
    }

    public interface ISyncAction<out TRepository, TEntity, in TCommand> : ISyncAction
        where TRepository : IMRepository<TEntity> where TEntity : MEntity where TCommand : IFrCommand
    {
        TRepository Repository { get; }

        ICommandSender<TCommand> Sender { get; }
    }
}