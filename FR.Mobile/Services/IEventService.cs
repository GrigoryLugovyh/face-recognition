﻿using System.Runtime.CompilerServices;

namespace FR.Mobile.Services
{
    public interface IEventService
    {
        void Info(string message, [CallerMemberName] string scope = "");

        void Warn(string message, [CallerMemberName] string scope = "");

        void Error(string message, [CallerMemberName] string scope = "");
    }
}