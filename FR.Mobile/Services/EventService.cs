﻿using System;
using System.Runtime.CompilerServices;
using FR.Mobile.Executors;
using FR.Mobile.Models;
using FR.Mobile.Storage;
using FR.Model.Dto;

namespace FR.Mobile.Services
{
    public class EventService : IEventService
    {
        private readonly Lazy<ICommandGenerator> commandGenerator =
            new Lazy<ICommandGenerator>(Container.Resolve<ICommandGenerator>);

        private readonly Lazy<IPreferences> preferences = new Lazy<IPreferences>(Container.Resolve<IPreferences>);

        public void Info(string message, [CallerMemberName] string scope = "")
        {
            SendMessage(EventDto.Info, message, scope);
        }

        public void Warn(string message, [CallerMemberName] string scope = "")
        {
            SendMessage(EventDto.Warn, message, scope);
        }

        public void Error(string message, [CallerMemberName] string scope = "")
        {
            SendMessage(EventDto.Error, message, scope);
        }

        protected virtual void SendMessage(string level, string message, string scope)
        {
            if (!preferences.Value.LoggingEnable)
                return;

            commandGenerator.Value.Enqueue(new MEvent
            {
                Level = level,
                Scope = scope,
                Message = message
            });
        }
    }
}