﻿using System;

namespace FR.Mobile.Services
{
    public interface IDateTimeService
    {
        DateTime Now { get; }

        DateTime UtcNow { get; }
    }
}