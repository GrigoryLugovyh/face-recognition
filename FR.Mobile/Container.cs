﻿using System;
using System.Collections.Generic;
using TinyIoC;

namespace FR.Mobile
{
    public static class Container
    {
        public static void Init()
        {
        }

        public static T Resolve<T>() where T : class => TinyIoCContainer.Current.Resolve<T>();

        public static object Resolve(Type type) => TinyIoCContainer.Current.Resolve(type);

        public static IEnumerable<T> ResolveAll<T>() where T : class => TinyIoCContainer.Current.ResolveAll<T>(true);

        public static T Resolve<T>(Type type) where T : class => (T) TinyIoCContainer.Current.Resolve(type);

        public static TinyIoCContainer.RegisterOptions AddRegistration<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class => TinyIoCContainer.Current.Register<TInterface, TImplementation>();

        public static void AddRegistration<TInterfaceFirst, TInterfaceSecond, TImplementation>()
            where TImplementation : class, TInterfaceFirst, TInterfaceSecond
            where TInterfaceFirst : class
            where TInterfaceSecond : class
        {
            TinyIoCContainer.Current.Register<TInterfaceFirst, TImplementation>();
            TinyIoCContainer.Current.Register<TInterfaceSecond, TImplementation>();
        }

        public static void AddRegistration<TInterfaceFirst, TInterfaceSecond, TInterfaceThird, TImplementation>()
            where TImplementation : class, TInterfaceFirst, TInterfaceSecond, TInterfaceThird
            where TInterfaceFirst : class
            where TInterfaceSecond : class
            where TInterfaceThird : class
        {
            TinyIoCContainer.Current.Register<TInterfaceFirst, TImplementation>();
            TinyIoCContainer.Current.Register<TInterfaceSecond, TImplementation>();
            TinyIoCContainer.Current.Register<TInterfaceThird, TImplementation>();
        }

        public static bool IsRegistred<T>() where T : class => TinyIoCContainer.Current.CanResolve<T>();

        public static bool IsRegistred(Type resolveType)  => TinyIoCContainer.Current.CanResolve(resolveType);

        public static TinyIoCContainer.RegisterOptions AddRegistration<TInterface>(TInterface instance)
            where TInterface : class => TinyIoCContainer.Current.Register(instance);

        public static TinyIoCContainer.RegisterOptions AddRegistration(Type registerType,
            Type registerImplementationType) => TinyIoCContainer.Current.Register(registerType,
            registerImplementationType);

        public static TinyIoCContainer.MultiRegisterOptions
            RegisterMultiple<TInterface>(IEnumerable<Type> implementationTypes) => TinyIoCContainer.Current
            .RegisterMultiple<TInterface>(implementationTypes);
    }
}