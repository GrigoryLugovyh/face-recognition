﻿namespace FR.Mobile
{
    public interface IStartable
    {
        void Start();
    }
}