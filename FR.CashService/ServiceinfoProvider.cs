﻿using System;
using System.Collections.Generic;
using CommonModel.Services;
using FR.Model;

namespace FR.CashService
{
    public class ServiceInfoProvider : IServiceInfoProvider
    {
        public string GetCurrentService()
        {
            return Service.Cash;
        }

        public Dictionary<Type, string> TaskDataToServiceMap => new Dictionary<Type, string>();
    }
}