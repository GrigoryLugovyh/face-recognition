﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface ICheckPositionRepository : IFrRepository<CheckPosition>
    {
    }
}