﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IPersonRepository : IFrRepository<Person>
    {
        void Upsert(string persistedFaceId, string faceId, double confidence, string faceListId = "");
    }
}