﻿using FR.Model.Models;

namespace FR.Repository
{
    public class PersonGroupRepository : FrRepository<PersonGroup>, IPersonGroupRepository
    {
    }
}