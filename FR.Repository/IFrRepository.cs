﻿using CommonModel.Model;
using FR.Model.Models;
using Repositories.Base;

namespace FR.Repository
{
    public interface IFrRepository<TEntity> : IBaseRepository<TEntity> where TEntity : FrEntity, IEntity
    {
    }
}