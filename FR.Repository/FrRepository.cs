﻿using CommonModel.Model;
using FR.Model.Models;
using Repositories.Base;

namespace FR.Repository
{
    public class FrRepository<TEntity> : BaseRepository<TEntity>, IFrRepository<TEntity>
        where TEntity : FrEntity, IEntity
    {
    }
}