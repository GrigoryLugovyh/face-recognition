﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Model.Models;
using Repositories.Base;

namespace FR.Repository
{
    public class PersonRepository : FrRepository<Person>, IPersonRepository
    {
        private readonly ILockingContext lockingContext;

        public PersonRepository(ILockingContext lockingContext)
        {
            this.lockingContext = lockingContext;
        }

        public void Upsert(string persistedFaceId, string faceId, double confidence, string faceListId = "")
        {
            lockingContext.InLockingContext(persistedFaceId, () =>
            {
                var personEntity = AsQueryable(false)
                                       .FirstOrDefault(person => person.PersistedFaceId == persistedFaceId) ??
                                   new Person
                                   {
                                       FaceListId = faceListId,
                                       PersistedFaceId = persistedFaceId,
                                       Faces = new List<Person.FaceConfidence>()
                                   };

                if (string.IsNullOrEmpty(personEntity.FaceListId))
                    throw new ArgumentNullException($"{nameof(faceListId)} can not be undefined");

                personEntity.Faces.Add(Person.FaceConfidence.Create(faceId, confidence));

                Write(personEntity);
            });
        }
    }
}