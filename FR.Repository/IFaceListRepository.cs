﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IFaceListRepository : IFrRepository<FaceList>
    {
    }
}