﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IItemRepository : IFrRepository<Item>
    {
    }
}