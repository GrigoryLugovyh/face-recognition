﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IPersonGroupRepository : IFrRepository<PersonGroup>
    {
    }
}