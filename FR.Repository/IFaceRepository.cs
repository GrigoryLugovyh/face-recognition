﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IFaceRepository : IFrRepository<Face>
    {
    }
}