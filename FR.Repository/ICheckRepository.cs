﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface ICheckRepository : IFrRepository<Check>
    {
    }
}