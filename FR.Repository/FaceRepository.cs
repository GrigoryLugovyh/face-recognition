﻿using FR.Model.Models;
using Repositories.Base;

namespace FR.Repository
{
    public class FaceRepository : BaseRepository<Face>, IFaceRepository
    {
    }
}