﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IEventRepository : IFrRepository<Event>
    {
    }
}