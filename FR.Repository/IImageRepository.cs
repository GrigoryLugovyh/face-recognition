﻿using FR.Model.Models;

namespace FR.Repository
{
    public interface IImageRepository : IFrRepository<Image>
    {
    }
}