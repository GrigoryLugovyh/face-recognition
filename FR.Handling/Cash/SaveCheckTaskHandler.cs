﻿using System;
using System.Linq;
using Common.Services;
using FR.Infrastructure.Extensions;
using FR.Model.Models;
using FR.Model.TaskData.Cash;
using FR.Repository;
using MoreLinq;
using Tasks.Handling;

namespace FR.Handling.Cash
{
    public class SaveCheckTaskHandler : TaskHandler<SaveCheckTaskData>
    {
        private readonly ICheckPositionRepository checkPositionRepository;
        private readonly ICheckRepository checkRepository;
        private readonly IGuidService guidService;
        private readonly IItemRepository itemRepository;

        public SaveCheckTaskHandler(IGuidService guidService, IItemRepository itemRepository,
            ICheckRepository checkRepository,
            ICheckPositionRepository checkPositionRepository)
        {
            this.guidService = guidService;
            this.itemRepository = itemRepository;
            this.checkRepository = checkRepository;
            this.checkPositionRepository = checkPositionRepository;
        }

        protected override HandleResult HandleTask(SaveCheckTaskData taskData)
        {
            Logger.Info($"Handle check in session \"{taskData.SessionId}\" ...");

            var check = new Check
            {
                Id = guidService.Generate(),
                SessionId = taskData.CheckDto.SessionId,
                CashierName = taskData.CheckDto.CashierName,
                DiscountCards = taskData.CheckDto.DiscountCards,
                BonusCards = taskData.CheckDto.BonusCards,
                TotalSum = taskData.CheckDto.TotalSum,
                TotalDiscount = taskData.CheckDto.TotalDiscount,
                ActivePositionsSum = taskData.CheckDto.ActivePositionsSum,
                DeletedPositionsSum = taskData.CheckDto.DeletedPositionsSum,
                DurationSeconds = taskData.CheckDto.DurationSeconds,
                Payments = taskData.CheckDto.Payments,
                OpenTimeStamp = taskData.CheckDto.OpenTimeStamp.ToDateTime(),
                OpenTimeStampUtc = taskData.CheckDto.OpenTimeStampUtc.ToDateTime(),
                CloseTimeStamp = taskData.CheckDto.CloseTimeStamp.ToDateTime(),
                CloseTimeStampUtc = taskData.CheckDto.CloseTimeStampUtc.ToDateTime(),
                SendTimeStamp = taskData.CheckDto.SendTimeStamp.ToDateTime(),
                SendTimeStampUtc = taskData.CheckDto.SendTimeStampUtc.ToDateTime(),
                ReciveTimeStamp = DateTime.Now,
                ReciveTimeStampUtc = DateTime.UtcNow
            };

            checkRepository.Write(check);

            Logger.Info($"Was saved new check \"{check.Id}\"");

            taskData.CheckDto.Positions.ForEach((position, i) =>
            {
                Logger.Info($"Handling {i} position from {taskData.CheckDto.Positions.Length} ...");

                var item = string.IsNullOrEmpty(position.Barcode)
                    ? itemRepository.AsQueryable(false).FirstOrDefault(item1 => item1.Code == position.Code)
                    : itemRepository.AsQueryable(false).FirstOrDefault(item1 => item1.Barcode == position.Barcode);

                if (item == null)
                {
                    item = new Item
                    {
                        Id = guidService.Generate(),
                        Name = position.Name,
                        Code = position.Code,
                        Barcode = position.Barcode
                    };

                    if (string.IsNullOrEmpty(item.Name))
                    {
                        item.Name = $"Товар {item.Barcode}";
                    }
                    itemRepository.Write(item);

                    Logger.Info($"Was created new item with id \"{item.Id}\", [{item.Barcode}, {item.Name}]");
                }

                var checkPosition = new CheckPosition
                {
                    Id = guidService.Generate(),
                    CheckId = check.Id,
                    ItemId = item.Id,
                    Quantity = position.Quantity,
                    Price = position.Price,
                    TotalPrice = position.TotalPrice,
                    Change = position.Changed,
                    Delete = position.Deleted,
                    AddTimeStamp = position.AddTimeStamp.ToDateTime(),
                    AddTimeStampUtc = position.AddTimeStampUtc.ToDateTime(),
                    ChangeTimeStamp = position.ChangeTimeStamp.ToDateTime(),
                    ChangeTimeStampUtc = position.ChangeTimeStampUtc.ToDateTime(),
                    DeleteTimeStamp = position.DeleteTimeStamp.ToDateTime(),
                    DeleteTimeStampUtc = position.DeleteTimeStampUtc.ToDateTime()
                };

                checkPositionRepository.Write(checkPosition);

                Logger.Info($"Was created position \"{checkPosition.Id}\"");
            });

            return Finish();
        }
    }
}