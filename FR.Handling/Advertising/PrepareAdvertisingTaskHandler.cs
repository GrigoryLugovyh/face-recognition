﻿using System;
using FR.Model.TaskData.Advertising;
using FR.Repository;
using Tasks.Handling;

namespace FR.Handling.Advertising
{
    public class PrepareAdvertisingTaskHandler : TaskHandler<PrepareAdvertisingTaskData>
    {
        private readonly IFaceRepository faceRepository;

        public PrepareAdvertisingTaskHandler(IFaceRepository faceRepository)
        {
            this.faceRepository = faceRepository;
        }

        protected override HandleResult HandleTask(PrepareAdvertisingTaskData taskData)
        {
            Logger.Info($"Prepare advertising for face {taskData.FaceId} in session context {taskData.SessionId} ...");

            var face = faceRepository.Read(taskData.FaceId);

            if (face == null)
                return Fatal(new InvalidOperationException($"Face is not exist, face id \"{taskData.FaceId}\""));

            Logger.Info($"Finding advertising rules for parameters: age \"{face.Age}\", gender \"{face.Gender}\" ...");

            return Finish();
        }
    }
}