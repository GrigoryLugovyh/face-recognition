﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using FR.Infrastructure.Components;
using FR.Infrastructure.Environment;
using FR.Infrastructure.Services;
using FR.Infrastructure.Services.Cognitive;
using FR.Model.Models;
using FR.Model.TaskData.Advertising;
using FR.Model.TaskData.Cognitive;
using FR.Repository;
using Repositories.Base;
using Repositories.FileStorage;
using Tasks.Handling;
using Tasks.Managing;

namespace FR.Handling.Cognitive
{
    public class DetectFaceTaskHandler : TaskHandler<DetectFaceTaskData>
    {
        private readonly IFacesAdvertisingDetector advertisingDetector;
        private readonly IFaceDetectCognitiveService faceDetectCognitiveService;
        private readonly IFaceListRepository faceListRepository;
        private readonly IFaceListService faceListService;
        private readonly IFaceRepository faceRepository;
        private readonly IFileStorageRepository fileStorageRepository;
        private readonly IFindSimilarCognitiveService findSimilarCognitiveService;
        private readonly IImageRepository imageRepository;
        private readonly ILockingContext lockingContext;
        private readonly ITaskManager taskManager;

        public DetectFaceTaskHandler(IImageRepository imageRepository, IFileStorageRepository fileStorageRepository,
            IFaceDetectCognitiveService faceDetectCognitiveService,
            IFindSimilarCognitiveService findSimilarCognitiveService, IFaceRepository faceRepository,
            ILockingContext lockingContext, IFaceListRepository faceListRepository, IFaceListService faceListService,
            IFacesAdvertisingDetector advertisingDetector, ITaskManager taskManager)
        {
            this.imageRepository = imageRepository;
            this.fileStorageRepository = fileStorageRepository;
            this.faceDetectCognitiveService = faceDetectCognitiveService;
            this.findSimilarCognitiveService = findSimilarCognitiveService;
            this.faceRepository = faceRepository;
            this.lockingContext = lockingContext;
            this.faceListRepository = faceListRepository;
            this.faceListService = faceListService;
            this.advertisingDetector = advertisingDetector;
            this.taskManager = taskManager;
        }

        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        protected override HandleResult HandleTask(DetectFaceTaskData taskData)
        {
            Logger.Info($"Detecting face(s) on image with id \"{taskData.ImageId}\"");

            var imageHalfEntity = imageRepository.AsQueryable(false).Where(image => image.Id == taskData.ImageId)
                .Select(image => new {image.Id, image.StorageFilePath}).FirstOrDefault();

            HandleResult FatalHandleResult(string message)
            {
                Logger.Warn(message);
                return Fatal(new InvalidDataException(message));
            }

            if (imageHalfEntity == null)
                return FatalHandleResult($"There is no image with id \"{taskData.ImageId}\"");

            var imageStorageEntry =
                fileStorageRepository.Read(FrFileStorageScopes.Faces, imageHalfEntity.StorageFilePath);

            if (imageStorageEntry == null)
                return FatalHandleResult(
                    $"There is no file storage entry for image with id \"{imageHalfEntity.Id}\", file storge path \"{imageHalfEntity.StorageFilePath}\"");

            Logger.Info($"Image \"{imageHalfEntity.Id}\" recognizing ...");

            var faces = faceDetectCognitiveService.Detect(imageStorageEntry.Item2).Result.ToList();

            if (!faces.Any())
                return Fatal(
                    new InvalidDataException($"Failed to recognize faces on image with id \"{taskData.ImageId}\""));

            Image imageEntity = null;

            lockingContext.InLockingContext(imageHalfEntity.Id, () =>
            {
                imageEntity = imageRepository.Read(imageHalfEntity.Id);

                imageEntity.FaceIds = faces.Select(face => face.FaceId.ToString()).ToArray();
                imageEntity.Recognized = true;

                imageRepository.Write(imageEntity);
            });

            Logger.Info($"Image \"{imageHalfEntity.Id}\" was updated as 'recognized'!");

            faceRepository.Write(faces.Select(face => new Face
            {
                Id = face.FaceId.ToString(),
                ImageId = imageEntity.Id,
                Age = face.FaceAttributes.Age,
                Gender = face.FaceAttributes.Gender,
                Rectangle = new Face.FaceRectangle
                {
                    Height = face.FaceRectangle.Height,
                    Width = face.FaceRectangle.Width,
                    Left = face.FaceRectangle.Left,
                    Top = face.FaceRectangle.Top
                }
            }));

            Logger.Info("Getting actual face list(s) ...");

            var faceLists = faceListRepository.AsQueryable(false).Select(list => new {list.Id}).AsEnumerable()
                .Select(arg => arg.Id).ToArray();

            if (!faceLists.Any())
                faceLists = new[] {faceListService.GetOrAddActualFaceList().Id};

            Logger.Info(
                $"Got/created face list(s) \"{string.Join(", ", faceLists)}\". Starting find similar face(s) in face lists(s) ...");

            foreach (var face in faces)
            {
                foreach (var faceListId in faceLists)
                {
                    Logger.Info($"Try to find face \"{face.FaceId}\" in face list \"{faceListId}\" ...");

                    var similarPersistedFaces = findSimilarCognitiveService.Find(face.FaceId, faceListId);

                    Logger.Info(
                        $"Similar persisted face(s): \"{string.Join(", ", similarPersistedFaces.Select(persistedFace => persistedFace.PersistedFaceId))}\"");

                    if ((similarPersistedFaces.Any() || faceLists.Last() == faceListId) &&
                        faceListService.AddFaceToFaceList(face.FaceId.ToString(), similarPersistedFaces))
                    {
                        Logger.Info("Face was added to actual face list");
                        break;
                    }
                }
            }

            Logger.Info("Detecting face for advertising ...");

            var faceForAdvertising = advertisingDetector.Detect(faces);

            Logger.Info($"Face for advertising detected, face id \"{faceForAdvertising.FaceId}\"");

            taskManager.NewTask(new PrepareAdvertisingTaskData
            {
                FaceId = faceForAdvertising.FaceId.ToString(),
                SessionId = imageEntity.SessionId
            });

            return Finish();
        }
    }
}