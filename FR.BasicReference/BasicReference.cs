﻿using System.Collections.Generic;
using FR.Model.Models;

namespace FR.BasicReference
{
    public class BasicReference : IBasicReference
    {
        public IEnumerable<PersonGroup> PersonGroups
        {
            get
            {
                yield return new PersonGroup
                {
                    Id = "3a1c68cd-ddbe-41e6-812a-d114cb26e7f5",
                    Name = "Ekaterinburg"
                };
            }
        }
    }
}