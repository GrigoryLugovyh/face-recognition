﻿using System.Collections.Generic;
using FR.Model.Models;

namespace FR.BasicReference
{
    public interface IBasicReference
    {
        IEnumerable<PersonGroup> PersonGroups { get; }
    }
}