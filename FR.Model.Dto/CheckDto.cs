﻿namespace FR.Model.Dto
{
    public class CheckDto
    {
        public string SessionId { get; set; }

        public string CashierName { get; set; }

        public string[] DiscountCards { get; set; }

        public string[] BonusCards { get; set; }

        public decimal TotalSum { get; set; }

        public decimal TotalDiscount { get; set; }

        public decimal ActivePositionsSum { get; set; }

        public decimal DeletedPositionsSum { get; set; }

        public string SendTimeStamp { get; set; }

        public string SendTimeStampUtc { get; set; }

        public string OpenTimeStamp { get; set; }

        public string OpenTimeStampUtc { get; set; }

        public string CloseTimeStamp { get; set; }

        public string CloseTimeStampUtc { get; set; }

        public double DurationSeconds { get; set; }

        public string[] Payments { get; set; }

        public CheckPositionDto[] Positions { get; set; }
    }
}