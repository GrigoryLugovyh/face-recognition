﻿namespace FR.Model.Dto
{
    public class EventDto
    {
        public const string Info = nameof(Info);
        public const string Warn = nameof(Warn);
        public const string Error = nameof(Error);

        public string DeviceTimeStamp { get; set; }

        public string SessionId { get; set; }

        public string DeviceId { get; set; }

        public string Level { get; set; }

        public string Scope { get; set; }

        public string Message { get; set; }
    }
}