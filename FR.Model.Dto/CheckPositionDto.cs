﻿namespace FR.Model.Dto
{
    public class CheckPositionDto
    {
        public string Name { get; set; }

        public string Barcode { get; set; }

        public string Code { get; set; }

        public double Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal TotalPrice { get; set; }

        public bool Changed { get; set; }

        public bool Deleted { get; set; }

        public string AddTimeStamp { get; set; }

        public string AddTimeStampUtc { get; set; }

        public string ChangeTimeStamp { get; set; }

        public string ChangeTimeStampUtc { get; set; }

        public string DeleteTimeStamp { get; set; }

        public string DeleteTimeStampUtc { get; set; }
    }
}