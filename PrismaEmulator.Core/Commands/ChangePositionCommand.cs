﻿namespace PrismaEmulator.Core.Commands
{
    public class ChangePositionCommand : Command
    {
        protected override int Code => 12;
    }
}