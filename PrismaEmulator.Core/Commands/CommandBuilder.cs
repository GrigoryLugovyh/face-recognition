﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using PrismaEmulator.Core.Generators;

namespace PrismaEmulator.Core.Commands
{
    public class CommandBuilder
    {
        private readonly List<(int Length, Func<int, string> Value)> valueOwnerList =
            new List<(int Length, Func<int, string> Value)>
            {
                (9, code => "ККМ"),
                (4, code => code.ToString()),
                (20, code => "1"),
                (30, code => "Системный администратор"),
                (10, code => "1"),
                (3, code => "1"),
                (13, code => NumberGenerator.Num(10000000, 999999999).ToString(CultureInfo.InvariantCulture)),
                (4, code => NumberGenerator.Num(1000, 9999).ToString()),
                (26, code => "?"),
                (30, code => ItemNameGenerator.Name),
                (15, code => NumberGenerator.Price.ToString(CultureInfo.InvariantCulture)),
                (15, code => NumberGenerator.Count.ToString()),
                (15, code => NumberGenerator.Price.ToString(CultureInfo.InvariantCulture)),
                (15, code =>
                    (NumberGenerator.Price * NumberGenerator.Num(1, 10)).ToString(CultureInfo.InvariantCulture))
            };

        public string Build(int commandCode)
        {
            var command = new StringBuilder();

            valueOwnerList.ForEach(behavior =>
            {
                var value = behavior.Value(commandCode);

                if (value.Length > behavior.Length)
                    value = value.Substring(0, behavior.Length);
                else if (value.Length < behavior.Length)
                    value = value.PadRight(behavior.Length, ' ');

                command.Append(value);
            });

            return command.ToString();
        }
    }
}