﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace PrismaEmulator.Core.Commands
{
    public abstract class Command
    {
        protected abstract int Code { get; }

        protected virtual string Content => new CommandBuilder().Build(Code);

        public virtual byte[] Create()
        {
            return Encoding.GetEncoding(866).GetBytes(Content);
        }

        public virtual void Invoke()
        {
            using (var sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                sock.SendTo(Create(), new IPEndPoint(IPAddress.Parse(Configuration.Address), Configuration.Port));
            }
        }
    }
}