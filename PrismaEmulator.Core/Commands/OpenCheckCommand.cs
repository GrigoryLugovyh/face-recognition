﻿namespace PrismaEmulator.Core.Commands
{
    public class OpenCheckCommand : Command
    {
        protected override int Code => 4;
    }
}