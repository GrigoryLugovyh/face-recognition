﻿namespace PrismaEmulator.Core.Commands
{
    public class SubtotalCommand : Command
    {
        protected override int Code => 37;
    }
}