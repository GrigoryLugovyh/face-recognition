﻿namespace PrismaEmulator.Core.Commands
{
    public class AddPositionCommand : Command
    {
        protected override int Code => 6;
    }
}