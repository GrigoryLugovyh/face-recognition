﻿namespace PrismaEmulator.Core.Commands
{
    public class CloseCheckCommand : Command
    {
        protected override int Code => 5;
    }
}