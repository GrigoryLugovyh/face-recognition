﻿namespace PrismaEmulator.Core
{
    public class AddressParcer
    {
        public bool TryParce(string input, out (string Address, int Port) addressport)
        {
            if (input.Contains(":"))
            {
                var candidates = input.Split(':');

                if (candidates.Length == 2)
                {
                    var addressCandidate = candidates[0].Trim();

                    if (int.TryParse(candidates[1].Trim(), out var portCandidate))
                    {
                        addressport = (addressCandidate, portCandidate);
                        return true;
                    }
                }
            }

            addressport = default((string, int));
            return false;
        }
    }
}