﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrismaEmulator.Core.Scripts
{
    public class TestScript
    {
        public void Run()
        {
            var commandManager = new CommandManager();

            var commands = new List<(string, int)>
            {
                ("1", 1000),
                ("2", 1500),
                ("2", 1500),
                ("2", 1500),
                ("2", 1500),
                ("2", 1500),
                ("4", 500),
                ("5", 2000)
            };

            while (true)
            {
                foreach (var command in commands)
                {
                    commandManager.Execute(command.Item1);

                    Task.Delay(command.Item2).Wait();
                }
            }
        }
    }
}