﻿using System;
using System.Collections.Generic;
using PrismaEmulator.Core.Commands;

namespace PrismaEmulator.Core
{
    public class CommandManager
    {
        private const int SuccessExitCode = 0;
        private const int NotSupportedExitCode = 1;
        private const int UnknownExceptionExitCode = -1;

        private readonly Dictionary<int, Command> commandBehaviorOwner = new Dictionary<int, Command>
        {
            [1] = new OpenCheckCommand(),
            [2] = new AddPositionCommand(),
            [3] = new ChangePositionCommand(),
            [4] = new SubtotalCommand(),
            [5] = new CloseCheckCommand()
        };

        public int Execute(string value)
        {
            if (!int.TryParse(value.Trim(), out var command) || !commandBehaviorOwner.ContainsKey(command))
                return NotSupportedExitCode;

            try
            {
                commandBehaviorOwner[command].Invoke();
            }
            catch (Exception)
            {
                return UnknownExceptionExitCode;
            }

            return SuccessExitCode;
        }
    }
}