﻿namespace PrismaEmulator.Core
{
    public static class Configuration
    {
        public static string Address { get; set; }

        public static int Port { get; set; }
    }
}