﻿using System;
using System.Threading;

namespace PrismaEmulator.Core.Generators
{
    public class DataGenerator
    {
        private static int seed = Environment.TickCount;

        private static readonly ThreadLocal<Random> randomWrapper = new ThreadLocal<Random>(
            () =>
                new Random(Interlocked.Increment(ref seed))
        );

        public static Random Randomizer => randomWrapper.Value;
    }
}