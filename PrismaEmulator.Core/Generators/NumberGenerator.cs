﻿using System;

namespace PrismaEmulator.Core.Generators
{
    public class NumberGenerator : DataGenerator
    {
        public static decimal Price => (decimal) Math.Round(Randomizer.NextDouble(), 2) * Randomizer.Next(1, 5000);

        public static double Quantity => Math.Round(Randomizer.NextDouble(), 2) * Randomizer.Next(1, 10);

        public static int Count => Randomizer.Next(1, 10) + Randomizer.Next(5, 1000);

        public static int Num(int min = 1, int max = int.MaxValue)
        {
            return Randomizer.Next(min, max);
        }
    }
}