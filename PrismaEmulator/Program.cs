﻿using System;
using PrismaEmulator.Core;
using PrismaEmulator.Core.Scripts;

namespace PrismaEmulator
{
    internal class Program
    {
        private static readonly AddressParcer addressParcer = new AddressParcer();
        private static readonly CommandManager commandManager = new CommandManager();

        private static void Main()
        {
            Console.WriteLine("Wait address and port in format \"address:port\" ...");

            var addressPortCandidate = Console.ReadLine();

            if (!addressParcer.TryParce(addressPortCandidate, out var addressPort))
            {
                Console.WriteLine("Incorrect address/port string");
                return;
            }

            Configuration.Address = addressPort.Address;
            Configuration.Port = addressPort.Port;

            Console.WriteLine("Ready for work!");
            Console.WriteLine(
                "Commands:   1 - open check, 2 - add random position, 3 - change position, 4 - subtotal, 5 - close check, t - load test");
            Console.WriteLine("Exit codes: 0 - success, 1 - command is not supported, -1 - unknown exception");
            Console.WriteLine("-------------");

            while (true)
            {
                Console.WriteLine(">>> waiting command ...");

                var command = Console.ReadLine();

                if (command != "t")
                {
                    var startCommandTicks = Environment.TickCount;

                    var code = commandManager.Execute(command);

                    Console.WriteLine(
                        $"command executed with code \"{code}\" for {Environment.TickCount - startCommandTicks} ms");
                    Console.WriteLine("<<<");
                }
                else
                {
                    new TestScript().Run();
                }
            }
        }
    }
}