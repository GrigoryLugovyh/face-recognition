set logdir=%cd%\Assets\MongoDB\log
set dbdir=%cd%\Assets\MongoDB\db

if not exist %logdir% (@md %logdir%)
if not exist %dbdir% (@md %dbdir%) 

start Assets\MongoDB\server\mongod.exe --storageEngine inMemory --logpath %logdir%\mongo.log --dbpath %dbdir% -nojournal