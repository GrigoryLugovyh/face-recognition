﻿using Android.App;
using Android.Content;
using FR.Mobile.Storage;

namespace FR.Droid.Infrastructure
{
    public class DroidPreferences : DefaultPreferences
    {
        private ISharedPreferences preferences;

        public override void Init()
        {
            if (preferences == null)
                preferences =
                    Application.Context.GetSharedPreferences(
                        Application.Context.Resources.GetString(Resource.String.app_name), FileCreationMode.Private);

            base.Init();
        }

        public override void SetValue(string key, string value)
        {
            if (string.IsNullOrEmpty(key))
                return;

            var editor = preferences.Edit();
            editor.PutString(key, value);
            editor.Apply();
        }

        public override string GetValue(string key)
        {
            return preferences.GetString(key, string.Empty);
        }
    }
}