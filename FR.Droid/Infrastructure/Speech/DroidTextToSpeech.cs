﻿using Android.App;
using Android.Speech.Tts;
using FR.Droid.Activities.Env;
using FR.Mobile;
using FR.Mobile.Infrastructure.Speech;
using Java.Lang;
using Java.Util;

namespace FR.Droid.Infrastructure.Speech
{
    public class DroidTextToSpeech : Object, ITextToSpeech, TextToSpeech.IOnInitListener
    {
        private TextToSpeech textToSpeechEngine;

        public void OnInit(OperationResult status)
        {
            if (status == OperationResult.Success)
            {
                var locale = new Locale("ru");
                textToSpeechEngine.SetLanguage(locale);
            }
        }

        public bool CanInit => Container.IsRegistred<IActivity>();

        public void Init()
        {
            textToSpeechEngine = new TextToSpeech(Application.Context, this);
        }

        public void Release()
        {
            if (textToSpeechEngine != null)
            {
                textToSpeechEngine.Stop();
                textToSpeechEngine.Shutdown();
            }
        }

        public void Speak(string text)
        {
            // todo использовать другую реализацию Speak
            textToSpeechEngine?.Speak(text, QueueMode.Flush, null);
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }
    }
}