﻿using System;
using Android.App;
using Android.Gms.Vision;
using Android.Gms.Vision.Faces;
using FR.Droid.Activities.Env;
using FR.Droid.Activities.Env.Main;
using FR.Mobile;
using FR.Mobile.Executors;
using FR.Mobile.Extensions;
using FR.Mobile.Session;

namespace FR.Droid.Infrastructure.Vision
{
    public class DroidVisionDispatcher : IDroidVisionDispatcher
    {
        private readonly Lazy<ISessionManager> sessionManager =
            new Lazy<ISessionManager>(Container.Resolve<ISessionManager>);

        private readonly Lazy<ICommandGenerator> commandGenerator =
            new Lazy<ICommandGenerator>(Container.Resolve<ICommandGenerator>);

        private IVisionOptions visionOptions;
        private IVisionProcessorFactory visionProcessorFactory;

        private IVisionSource visionSource;

        public CameraSource CameraSource { get; private set; }

        public bool CanInit => Container.IsRegistred<IActivity>();

        public void Init()
        {
            Logger.Info($"Initializing {GetType().Name} ...");

            visionSource = Container.Resolve<IVisionSource>();
            visionOptions = Container.Resolve<IVisionOptions>();

            visionProcessorFactory = Container.Resolve<IVisionProcessorFactory>();

            var context = Application.Context;
            var detector = new FaceDetector.Builder(context)
                .SetProminentFaceOnly(true)
                .SetClassificationType(ClassificationType.All)
                .Build();

            var builder = new MultiProcessor.Builder(visionProcessorFactory);

            detector.SetProcessor(builder.Build());

            CameraSource = new CameraSource.Builder(context, detector)
                .SetRequestedPreviewSize(visionOptions.RequestedPreviewSize.Width,
                    visionOptions.RequestedPreviewSize.Height)
                .SetAutoFocusEnabled(false)
                .SetFacing(visionOptions.Facing)
                .SetRequestedFps(visionOptions.RequestedFps)
                .Build();

            visionProcessorFactory.PictureTaken += OnFaceDetected;

            Logger.Info($"{GetType().Name} initialized!");
        }

        public void Start()
        {
            if (CameraSource == null) return;

            visionSource?.Preview?.Start(CameraSource, visionSource.Overlay);

            Logger.Info($"{GetType().Name} started");
        }

        public void Stop()
        {
            visionSource?.Preview?.Stop();

            Logger.Info($"{GetType().Name} stopped");
        }

        public void Release()
        {
            if (visionProcessorFactory != null)
                visionProcessorFactory.PictureTaken -= OnFaceDetected;

            Stop();
            CameraSource?.Release();

            Logger.Info($"{GetType().Name} released");
        }

        private void OnFaceDetected(object sender, byte[] bytes)
        {
            if (!sessionManager.Value.GetCurrentSessionId.HasValue())
                return;

            Logger.Info($"Picture was taken, content length {bytes.Length}");

            commandGenerator.Value.Enqueue(bytes);

            Logger.Info("It was created command to send picture to the server");
        }
    }
}