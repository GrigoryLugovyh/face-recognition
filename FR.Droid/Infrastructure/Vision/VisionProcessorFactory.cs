﻿using System;
using Android.Gms.Vision;
using FR.Mobile;
using Object = Java.Lang.Object;

namespace FR.Droid.Infrastructure.Vision
{
    public class VisionProcessorFactory : Object, IVisionProcessorFactory
    {
        private readonly object createTrackerLockContext = new object();

        private readonly Lazy<IDroidVisionDispatcher> droidVisionDispatcher =
            new Lazy<IDroidVisionDispatcher>(Container.Resolve<IDroidVisionDispatcher>);

        private IFaceDetector faceTracker;

        private Tracker Tracker
        {
            get
            {
                lock (createTrackerLockContext)
                {
                    if (faceTracker != null)
                        faceTracker.Detected -= OnFaceDetected;

                    faceTracker = new FaceTracker(droidVisionDispatcher.Value);
                    faceTracker.Detected += OnFaceDetected;

                    return (Tracker) faceTracker;
                }
            }
        }

        public event EventHandler<byte[]> PictureTaken;

        public Tracker Create(Object item)
        {
            return Tracker;
        }

        private void OnFaceDetected(object sender, byte[] e)
        {
            PictureTaken?.Invoke(sender, e);
        }
    }
}