﻿using Android.Gms.Vision;

namespace FR.Droid.Infrastructure.Vision
{
    public interface IVisionOptions
    {
        float RequestedFps { get; set; }

        CameraFacing Facing { get; set; }

        (int Width, int Height) RequestedPreviewSize { get; set; }
    }
}