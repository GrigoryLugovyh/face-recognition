﻿using System;
using Android.Gms.Vision;
using FR.Mobile;
using Object = Java.Lang.Object;

namespace FR.Droid.Infrastructure.Vision
{
    public class FaceTracker : Tracker, CameraSource.IPictureCallback, IFaceDetector
    {
        public static bool FaceTrackerInProcess;

        private readonly CameraSource cameraSource;

        public FaceTracker(IDroidVisionDispatcher visionDispatcher)
        {
            cameraSource = visionDispatcher.CameraSource;
        }

        public event EventHandler<byte[]> Detected;

        public void OnPictureTaken(byte[] data)
        {
            FaceTrackerInProcess = true;

            try
            {
                Detected?.Invoke(null, data);
            }
            finally
            {
                FaceTrackerInProcess = false;
            }
        }

        public override void OnNewItem(int id, Object item)
        {
            if (FaceTrackerInProcess)
                return;

            try
            {
                cameraSource?.Start().TakePicture(null, this);
            }
            catch (Exception e)
            {
                Logger.Warn(e.Message);
            }
        }
    }
}