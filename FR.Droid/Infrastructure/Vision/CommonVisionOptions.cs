﻿using Android.Gms.Vision;

namespace FR.Droid.Infrastructure.Vision
{
    public class CommonVisionOptions : IVisionOptions
    {
        public float RequestedFps { get; set; } = 15.0f;

        public CameraFacing Facing { get; set; } = CameraFacing.Front;

        public (int Width, int Height) RequestedPreviewSize { get; set; } = (480, 640);
    }
}