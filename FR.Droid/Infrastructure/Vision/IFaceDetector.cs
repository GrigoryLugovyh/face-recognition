﻿using System;

namespace FR.Droid.Infrastructure.Vision
{
    public interface IFaceDetector : IDisposable
    {
        event EventHandler<byte[]> Detected;
    }
}