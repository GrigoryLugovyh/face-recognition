﻿using System;
using Android.Gms.Vision;

namespace FR.Droid.Infrastructure.Vision
{
    public interface IVisionProcessorFactory : MultiProcessor.IFactory
    {
        event EventHandler<byte[]> PictureTaken;
    }
}