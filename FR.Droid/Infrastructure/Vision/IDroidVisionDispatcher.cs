﻿using Android.Gms.Vision;
using FR.Mobile.Vision;

namespace FR.Droid.Infrastructure.Vision
{
    public interface IDroidVisionDispatcher : IVisionDispatcher
    {
        CameraSource CameraSource { get; }
    }
}