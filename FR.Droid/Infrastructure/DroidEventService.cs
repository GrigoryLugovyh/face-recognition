﻿using Android.Util;
using FR.Mobile.Services;
using FR.Model.Dto;

namespace FR.Droid.Infrastructure
{
    public class DroidEventService : EventService
    {
        protected override void SendMessage(string level, string message, string scope)
        {
            const string tag = "FR.Droid >>>";

            switch (level)
            {
                case EventDto.Info:
                    Log.Info(tag, message);
                    break;
                case EventDto.Warn:
                    Log.Info(tag, message);
                    break;
                case EventDto.Error:
                    Log.Info(tag, message);
                    break;
            }

            base.SendMessage(level, message, scope);
        }
    }
}