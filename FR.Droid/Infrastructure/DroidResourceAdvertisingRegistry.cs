﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Android.App;
using Android.Graphics;
using FR.Mobile;
using FR.Mobile.Advertising;
using FR.Mobile.Infrastructure.FileSystem;
using Path = System.IO.Path;

namespace FR.Droid.Infrastructure
{
    public class DroidResourceAdvertisingRegistry : IAdvertisingRegistry
    {
        public IEnumerable<AdvertisingScenario> GetScenarios()
        {
            var fileSystem = Container.Resolve<IFileSystem>();

            var advertisingImageFiles = Directory.GetFiles(fileSystem.ApplicationAdvertisingImagesDirectory);

            if (advertisingImageFiles.Any())
            {
                foreach (var advertisingImageFile in advertisingImageFiles)
                {
                    File.Delete(advertisingImageFile);
                }
            }

            int[] imageIds =
            {
                Resource.Drawable.r_1,
                Resource.Drawable.r_2,
                Resource.Drawable.r_3,
                Resource.Drawable.r_4,
                Resource.Drawable.r_5,
                Resource.Drawable.r_6
            };

            foreach (var imageId in imageIds)
            {
                var imageBitmap = BitmapFactory.DecodeResource(Application.Context.Resources, imageId);

                using (var stream = new MemoryStream())
                {
                    imageBitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);

                    File.WriteAllBytes(
                        Path.Combine(fileSystem.ApplicationAdvertisingImagesDirectory, Guid.NewGuid().ToString()),
                        stream.ToArray());
                }

                imageBitmap.Recycle();
            }

            advertisingImageFiles = Directory.GetFiles(fileSystem.ApplicationAdvertisingImagesDirectory);

            foreach (var advertisingImageFile in advertisingImageFiles)
                yield return new AdvertisingScenario
                {
                    FilePath = advertisingImageFile,
                    Duration = TimeSpan.FromSeconds(8)
                };
        }
    }
}