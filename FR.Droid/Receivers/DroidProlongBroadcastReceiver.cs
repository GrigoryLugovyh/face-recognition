﻿using System;
using Android.Content;

namespace FR.Droid.Receivers
{
    [BroadcastReceiver]
    public abstract class DroidProlongBroadcastReceiver : BroadcastReceiver
    {
        protected abstract Type ServiceType { get; }

        protected virtual void StartService(Context context)
        {
            var serviceIntent = new Intent(context, ServiceType);
            context.StartService(serviceIntent);
        }

        public override void OnReceive(Context context, Intent intent)
        {
            StartService(context);
        }
    }
}