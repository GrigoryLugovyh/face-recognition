﻿using System;
using Android.Content;
using FR.Droid.Services.Pulse;

namespace FR.Droid.Receivers
{
    [BroadcastReceiver]
    public class PulseBroadcastReciver : DroidProlongBroadcastReceiver
    {
        protected override Type ServiceType => typeof(PulseService);
    }
}