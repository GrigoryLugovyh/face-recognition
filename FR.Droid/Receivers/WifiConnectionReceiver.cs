﻿using System;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using FR.Mobile;
using FR.Mobile.Infrastructure.Connectivity;

namespace FR.Droid.Receivers
{
    public class WifiConnectionReceiver : BroadcastReceiver
    {
        private readonly Lazy<IWifiStateObserver> stateObserver =
            new Lazy<IWifiStateObserver>(Container.Resolve<IWifiStateObserver>);

        public override void OnReceive(Context context, Intent intent)
        {
            if (intent.Action != WifiManager.NetworkStateChangedAction) return;

            var info = intent.GetParcelableExtra(WifiManager.ExtraNetworkInfo) as NetworkInfo;

            if (info?.IsConnected is true)
                stateObserver.Value.RaiseConnected();
            else
                stateObserver.Value.RaiseDisconnected();
        }
    }
}