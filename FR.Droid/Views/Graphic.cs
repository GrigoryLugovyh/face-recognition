﻿using Android.Gms.Vision;
using Android.Graphics;

namespace FR.Droid.Views
{
    public abstract class Graphic
    {
        private readonly GraphicOverlayView overlay;

        protected Graphic(GraphicOverlayView overlay)
        {
            this.overlay = overlay;
        }

        public abstract void Draw(Canvas canvas);

        public float ScaleX(float horizontal)
        {
            return horizontal * overlay.WidthScaleFactor;
        }

        public float ScaleY(float vertical)
        {
            return vertical * overlay.HeightScaleFactor;
        }

        public float TranslateX(float x)
        {
            return overlay.CameraFacing == CameraFacing.Front ? overlay.Width - ScaleX(x) : ScaleX(x);
        }

        public float TranslateY(float y)
        {
            return ScaleY(y);
        }

        public void PostInvalidate()
        {
            overlay.PostInvalidate();
        }
    }
}