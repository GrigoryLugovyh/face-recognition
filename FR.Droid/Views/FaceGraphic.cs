﻿using System;
using Android.Gms.Vision.Faces;
using Android.Graphics;

namespace FR.Droid.Views
{
    [Obsolete]
    public class FaceGraphic : Graphic
    {
        private static readonly Color[] colorChoices =
        {
            Color.Blue,
            Color.Cyan,
            Color.Green,
            Color.Magenta,
            Color.Red,
            Color.White,
            Color.Yellow
        };

        private static int currentColorIndex;
        private readonly Paint boxPaint;
        private volatile Face face;

        public FaceGraphic(GraphicOverlayView overlay) : base(overlay)
        {
            currentColorIndex = (currentColorIndex + 1) % colorChoices.Length;
            var selectedColor = colorChoices[currentColorIndex];

            boxPaint = new Paint
            {
                Color = selectedColor
            };

            boxPaint.SetStyle(Paint.Style.Stroke);
            boxPaint.StrokeWidth = 5.0f;
        }

        public void UpdateFace(Face faceCandidate)
        {
            face = faceCandidate;
            PostInvalidate();
        }

        public override void Draw(Canvas canvas)
        {
            if (face == null)
                return;

            var x = TranslateX(face.Position.X + face.Width / 2);
            var y = TranslateY(face.Position.Y + face.Height / 2);

            var xOffset = ScaleX(face.Width / 2.0f);
            var yOffset = ScaleY(face.Height / 2.0f);
            var left = x - xOffset;
            var top = y - yOffset;
            var right = x + xOffset;
            var bottom = y + yOffset;

            canvas.DrawRect(left, top, right, bottom, boxPaint);
        }
    }
}