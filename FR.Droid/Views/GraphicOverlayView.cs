﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Gms.Vision;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;

namespace FR.Droid.Views
{
    public class GraphicOverlayView : View
    {
        private readonly HashSet<Graphic> graphics = new HashSet<Graphic>();
        private readonly object overlayLockContext = new object();

        protected GraphicOverlayView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public GraphicOverlayView(Context context) : base(context)
        {
        }

        public GraphicOverlayView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public GraphicOverlayView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs,
            defStyleAttr)
        {
        }

        public GraphicOverlayView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(
            context, attrs, defStyleAttr, defStyleRes)
        {
        }

        public int PreviewWidth { get; set; }
        public int PreviewHeight { get; set; }

        public float WidthScaleFactor { get; set; } = 1.0f;
        public float HeightScaleFactor { get; set; } = 1.0f;

        public CameraFacing CameraFacing { get; set; } = CameraFacing.Front;

        public void Clear()
        {
            lock (overlayLockContext)
            {
                graphics.Clear();
            }

            PostInvalidate();
        }

        public void Add(Graphic graphic)
        {
            lock (overlayLockContext)
            {
                graphics.Add(graphic);
            }

            PostInvalidate();
        }

        public void Remove(Graphic graphic)
        {
            lock (overlayLockContext)
            {
                graphics.Remove(graphic);
            }

            PostInvalidate();
        }

        public void SetCameraInfo(int previewWidth, int previewHeight, CameraFacing facing)
        {
            lock (overlayLockContext)
            {
                PreviewWidth = previewWidth;
                PreviewHeight = previewHeight;
                CameraFacing = facing;
            }

            PostInvalidate();
        }

        public override void Draw(Canvas canvas)
        {
            base.Draw(canvas);

            lock (overlayLockContext)
            {
                if (PreviewWidth != 0 && PreviewHeight != 0)
                {
                    WidthScaleFactor = canvas.Width / (float) PreviewWidth;
                    HeightScaleFactor = canvas.Height / (float) PreviewHeight;
                }

                foreach (var graphic in graphics)
                    graphic.Draw(canvas);
            }
        }
    }
}