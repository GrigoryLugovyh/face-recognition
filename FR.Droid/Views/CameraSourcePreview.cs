﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.Gms.Vision;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using FR.Mobile;

namespace FR.Droid.Views
{
    public sealed class CameraSourcePreview : ViewGroup, ISurfaceHolderCallback
    {
        private readonly Context context;
        private CameraSource cameraSource;
        private GraphicOverlayView graphicOverlay;

        private bool started;

        private bool startRequested;
        private bool surfaceAvailable;
        private SurfaceView surfaceView;

        public CameraSourcePreview(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            this.context = context;
        }

        private static Handler CameraHandler => new Handler(Looper.MainLooper);

        public void SurfaceChanged(ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height)
        {
        }

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            surfaceAvailable = true;

            try
            {
                StartIfReady();
            }
            catch (Exception e)
            {
                Log.Error(GetType().Name, "Could not start camera source.", e);
            }
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            surfaceAvailable = false;
        }

        public void Start(CameraSource camera, GraphicOverlayView overlay)
        {
            if (started) return;

            CameraHandler.Post(() =>
            {
                Logger.Info("Camera starting ...");

                graphicOverlay = overlay;

                try
                {
                    startRequested = false;
                    surfaceAvailable = false;

                    surfaceView = new SurfaceView(context);
                    surfaceView.Holder.AddCallback(this);

                    AddView(surfaceView);

                    Start(camera);

                    started = true;
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }

                Logger.Info("Camera started");
            });
        }

        public void Stop()
        {
            if (!started) return;

            CameraHandler.Post(() =>
            {
                Logger.Info("Camera stopping ...");

                try
                {
                    cameraSource?.Stop();
                    cameraSource = null;

                    surfaceView.Holder.RemoveCallback(this);
                    RemoveView(surfaceView);
                    surfaceView = null;

                    started = false;
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }

                Logger.Info("Camera stoped");
            });
        }

        public void Start(CameraSource camera)
        {
            if (camera == null)
                Stop();

            cameraSource = camera;

            if (cameraSource != null)
            {
                startRequested = true;
                StartIfReady();
            }
        }

        private void StartIfReady()
        {
            if (startRequested && surfaceAvailable)
            {
                cameraSource.Start(surfaceView.Holder);

                if (graphicOverlay != null)
                {
                    var size = cameraSource.PreviewSize;

                    var min = Math.Min(size.Width, size.Height);
                    var max = Math.Max(size.Width, size.Height);

                    if (IsPortraitMode())
                        graphicOverlay.SetCameraInfo(min, max, cameraSource.CameraFacing);
                    else
                        graphicOverlay.SetCameraInfo(max, min, cameraSource.CameraFacing);

                    graphicOverlay.Clear();
                }

                startRequested = false;
            }
        }

        private bool IsPortraitMode()
        {
            var orientation = context.Resources.Configuration.Orientation;

            if (orientation == Orientation.Landscape)
                return false;

            if (orientation == Orientation.Portrait)
                return true;

            Log.Debug(GetType().Name, "isPortraitMode returning false by default");

            return false;
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            var width = 320;
            var height = 240;

            var size = cameraSource?.PreviewSize;

            if (size != null)
            {
                width = size.Width;
                height = size.Height;
            }

            if (IsPortraitMode())
            {
                var tmp = width;
                width = height;
                height = tmp;
            }

            var layoutWidth = r - l;
            var layoutHeight = b - t;

            var childWidth = layoutWidth;
            var childHeight = (int) (layoutWidth / (float) width * height);

            if (childHeight > layoutHeight)
            {
                childHeight = layoutHeight;
                childWidth = (int) (layoutHeight / (float) height * width);
            }

            for (var i = 0; i < ChildCount; ++i)
                GetChildAt(i).Layout(0, 0, childWidth, childHeight);

            try
            {
                StartIfReady();
            }
            catch (Exception e)
            {
                Log.Error(GetType().Name, "Could not start camera source.", e);
            }
        }
    }
}