﻿using Java.Lang;

namespace FR.Droid.Extensions
{
    internal class JavaWrapper : Object
    {
        public JavaWrapper(object instance)
        {
            Instance = instance;
        }

        public object Instance { get; set; }
    }

    public static class JavaExtensions
    {
        public static Object ToJava<T>(this T value)
        {
            return new JavaWrapper(value);
        }

        public static T FromJava<T>(this Object obj) where T : class
        {
            var propertyInfo = obj.GetType().GetProperty("Instance");
            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
        }
    }
}