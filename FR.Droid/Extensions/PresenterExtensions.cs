﻿using System.Collections.Generic;
using FR.Droid.Activities.Env;

namespace FR.Droid.Extensions
{
    public static class PresenterExtensions
    {
        public static IEnumerable<IActivityPresenter> Subscribe(this IEnumerable<IActivityPresenter> presenters)
        {
            foreach (var activityPresenter in presenters)
            {
                activityPresenter.Subscribe();
                yield return activityPresenter;
            }
        }

        public static void Unsubscribe(this IEnumerable<IActivityPresenter> presenters)
        {
            foreach (var activityPresenter in presenters)
            {
                activityPresenter.Unsubscribe();
            }
        }
    }
}