﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using FR.Droid.Activities.Env;
using FR.Droid.Activities.Env.Main;
using FR.Droid.Views;
using FR.Mobile;
using FR.Mobile.App;

namespace FR.Droid.Activities
{
    [Activity(Label = "@string/app_title", MainLauncher = true, Theme = "@style/Theme.Main",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : SourceActivity, IActivity, IHelloSource, ISalesSource, IVisionSource,
        IAdvertisingSource
    {
        public ImageSwitcher AdvertisingImageSwitcher { get; private set; }
        public MainActivity Activity => this;

        public TextView HelloText { get; private set; }

        public ListView SalesList { get; private set; }

        public TextView SalesTotal { get; private set; }

        public LinearLayout SalesLayout { get; private set; }

        public GraphicOverlayView Overlay { get; private set; }

        public CameraSourcePreview Preview { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            Window.AddFlags(WindowManagerFlags.Fullscreen);

            SetContentView(Resource.Layout.Main);

            base.OnCreate(savedInstanceState);
        }

        protected override void Creating()
        {
            HelloText = FindViewById<TextView>(Resource.Id.HelloText);

            SalesList = FindViewById<ListView>(Resource.Id.SalesList);
            SalesTotal = FindViewById<TextView>(Resource.Id.SalesTotal);
            SalesLayout = FindViewById<LinearLayout>(Resource.Id.SalesLayout);

            Preview = FindViewById<CameraSourcePreview>(Resource.Id.CameraSource);
            Overlay = FindViewById<GraphicOverlayView>(Resource.Id.GraphicOverlay);

            AdvertisingImageSwitcher = FindViewById<ImageSwitcher>(Resource.Id.AdvertisingImageSwitcher);

            Container.AddRegistration<IActivity>(this);
            Container.AddRegistration<IHelloSource>(this);
            Container.AddRegistration<ISalesSource>(this);
            Container.AddRegistration<IVisionSource>(this);
            Container.AddRegistration<IAdvertisingSource>(this);

            AppModel.Instance<DroidModel>();
        }

        protected override void Resuming()
        {
            Container.Resolve<IAppDispatcher>().Start();
        }

        protected override void Pausing()
        {
            Container.Resolve<IAppDispatcher>().Stop();
        }

        protected override void Destroing()
        {
            Container.Resolve<IAppDispatcher>().Release();
        }
    }
}