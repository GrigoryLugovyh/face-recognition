﻿using System;
using Android.Runtime;
using Android.Views;
using Android.Views.Animations;
using FR.Mobile;
using FR.Mobile.App;
using FR.Mobile.Cash;

namespace FR.Droid.Activities.Env.Main
{
    public class SalesPresenter : ActivityPresenter<MainActivity, ISalesSource>
    {
        private IAppDispatcher appDispatcher;

        protected override void SourceOnCreated(object sender, EventArgs e)
        {
            base.SourceOnCreated(sender, e);

            appDispatcher = Container.Resolve<IAppDispatcher>();

            ToggleComponentsVisibility(ComponentsVisibilityMode.Hello);

            Source.SalesList.Adapter = new SalesAdapter(Source.Activity, Resource.Layout._Sale);
        }

        protected override void SourceOnResumed(object sender, EventArgs eventArgs)
        {
            base.SourceOnResumed(sender, eventArgs);

            appDispatcher.StartSales += AppDispatcherOnStartSales;
            appDispatcher.ChangeSale += AppDispatcherOnChangeSale;
            appDispatcher.FinishSales += AppDispatcherOnFinishSales;
        }

        protected override void SourceOnPaused(object sender, EventArgs eventArgs)
        {
            base.SourceOnPaused(sender, eventArgs);

            appDispatcher.StartSales -= AppDispatcherOnStartSales;
            appDispatcher.ChangeSale -= AppDispatcherOnChangeSale;
            appDispatcher.FinishSales -= AppDispatcherOnFinishSales;
        }

        private static void AppDispatcherOnStartSales(object sender, Check check)
        {
        }

        private void AppDispatcherOnChangeSale(object sender, Check check)
        {
            Source.Activity.RunOnUiThread(() =>
            {
                ToggleComponentsVisibility(ComponentsVisibilityMode.Sales);
                ((SalesAdapter) Source.SalesList.Adapter).ChangeCheck(check);
                Source.SalesTotal.Text = $"{check.TotalSum} P";
            });
        }

        private void AppDispatcherOnFinishSales(object sender, Check check)
        {
            Source.Activity.RunOnUiThread(() =>
            {
                ToggleComponentsVisibility(ComponentsVisibilityMode.Hello);
                ((SalesAdapter) Source.SalesList.Adapter).CreateCheck();
            });
        }

        private void ToggleComponentsVisibility(ComponentsVisibilityMode mode)
        {
            switch (mode)
            {
                case ComponentsVisibilityMode.Hello:
                    Source.SalesLayout.Visibility = ViewStates.Gone;
                    GlobalEvents.Value.RaiseHelloScreenVisible(true);
                    break;
                case ComponentsVisibilityMode.Sales:
                    Source.SalesLayout.Visibility = ViewStates.Visible;
                    GlobalEvents.Value.RaiseHelloScreenVisible(false);
                    break;
            }
        }

        private enum ComponentsVisibilityMode
        {
            Hello,
            Sales
        }
    }
}