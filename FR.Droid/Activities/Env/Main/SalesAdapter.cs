﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Android.Content;
using Android.Views;
using Android.Widget;
using FR.Mobile;
using FR.Mobile.Cash;

namespace FR.Droid.Activities.Env.Main
{
    public class SalesAdapter : ArrayAdapter<CheckPosition>
    {
        private static IList<CheckPosition> checkPositions = new List<CheckPosition>();

        public SalesAdapter(Context context, int textViewResourceId) : base(context,
            textViewResourceId, checkPositions)
        {
        }

        public override int Count => checkPositions.Count;

        public void CreateCheck()
        {
            try
            {
                checkPositions = new List<CheckPosition>();
                NotifyDataSetChanged();
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        public void ChangeCheck(Check check)
        {
            try
            {
                checkPositions = check.ActivePositions.ToList();
                NotifyDataSetChanged();
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        public override long GetItemId(int position)
        {
            try
            {
                return checkPositions[position].GetHashCode();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return -1;
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = LayoutInflater.From(Context).Inflate(Resource.Layout._Sale, parent, false);

            try
            {
                var checkPosition = checkPositions[position];

                var num = view.FindViewById<TextView>(Resource.Id.SaleItemNumber);
                var name = view.FindViewById<TextView>(Resource.Id.SaleItemName);
                //var cout = view.FindViewById<TextView>(Resource.Id.SaleItemCount);
                var pric = view.FindViewById<TextView>(Resource.Id.SaleItemPrice);

                num.Text = checkPosition.Number.ToString();
                name.Text = checkPosition.Name;
                //cout.Text = checkPosition.Quantity.ToString(CultureInfo.InvariantCulture);
                pric.Text = $"{checkPosition.Price}";
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

            return view;
        }
    }
}