﻿using Android.Widget;

namespace FR.Droid.Activities.Env.Main
{
    public interface IHelloSource : ISource<MainActivity>
    {
        TextView HelloText { get; }
    }
}