﻿using Android.Widget;

namespace FR.Droid.Activities.Env.Main
{
    public interface IAdvertisingSource : ISource<MainActivity>
    {
        ImageSwitcher AdvertisingImageSwitcher { get; }
    }
}