﻿using System;

namespace FR.Droid.Activities.Env.Main
{
    public class VisionPresenter : ActivityPresenter<MainActivity, IVisionSource>
    {
        protected override void SourceOnCreated(object sender, EventArgs e)
        {
            base.SourceOnCreated(sender, e);

            GlobalEvents.Value.CameraPreviewEnable += GlobalEventsOnCameraPreviewEnable;
        }

        protected override void SourceOnDestroied(object sender, EventArgs eventArgs)
        {
            base.SourceOnDestroied(sender, eventArgs);

            GlobalEvents.Value.CameraPreviewEnable -= GlobalEventsOnCameraPreviewEnable;
        }

        private void GlobalEventsOnCameraPreviewEnable(object sender, bool visible)
        {
            switch (visible)
            {
                case true:
                    Source.Preview.LayoutParameters.Height = 320;
                    Source.Preview.LayoutParameters.Width = 240;
                    break;
                default:
                    Source.Preview.LayoutParameters.Height = 1;
                    Source.Preview.LayoutParameters.Width = 1;
                    break;
            }

            Source.Preview.RequestLayout();
        }
    }
}