﻿using System;
using Android.Views;

namespace FR.Droid.Activities.Env.Main
{
    public class HelloPresenter : ActivityPresenter<MainActivity, IHelloSource>
    {
        protected override void SourceOnResumed(object sender, EventArgs eventArgs)
        {
            base.SourceOnResumed(sender, eventArgs);

            Source.HelloText.LongClick += HelloTextOnLongClick;
            GlobalEvents.Value.HelloScreenVisible += GlobalEventsOnHelloScreenVisible;
        }

        protected override void SourceOnPaused(object sender, EventArgs eventArgs)
        {
            base.SourceOnPaused(sender, eventArgs);

            Source.HelloText.LongClick -= HelloTextOnLongClick;
            GlobalEvents.Value.HelloScreenVisible -= GlobalEventsOnHelloScreenVisible;
        }

        private void HelloTextOnLongClick(object sender, View.LongClickEventArgs longClickEventArgs)
        {
            Source.Activity.StartActivity(typeof(SettingsActivity));
        }

        private void GlobalEventsOnHelloScreenVisible(object sender, bool visible)
        {
            switch (visible)
            {
                case true:
                    Source.HelloText.Visibility = ViewStates.Visible;
                    break;
                default:
                    Source.HelloText.Visibility = ViewStates.Gone;
                    break;
            }
        }
    }
}