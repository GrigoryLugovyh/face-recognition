﻿using FR.Droid.Views;

namespace FR.Droid.Activities.Env.Main
{
    public interface IVisionSource : ISource<MainActivity>
    {
        CameraSourcePreview Preview { get; }

        GraphicOverlayView Overlay { get; }
    }
}