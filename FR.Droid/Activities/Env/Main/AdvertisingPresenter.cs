﻿using System;
using System.IO;
using Android.Graphics.Drawables;
using FR.Mobile;
using FR.Mobile.Advertising;
using FR.Mobile.App;
using FR.Mobile.Cash;
using FR.Mobile.Infrastructure.FileSystem;

namespace FR.Droid.Activities.Env.Main
{
    public class AdvertisingPresenter : ActivityPresenter<MainActivity, IAdvertisingSource>
    {
        private IAdvertisingProvider advertisingProvider;
        private IAppDispatcher appDispatcher;
        private IFileLoader fileLoader;

        protected override void SourceOnCreated(object sender, EventArgs e)
        {
            base.SourceOnCreated(sender, e);

            fileLoader = Container.Resolve<IFileLoader>();
            appDispatcher = Container.Resolve<IAppDispatcher>();
            advertisingProvider = Container.Resolve<IAdvertisingProvider>();

            Source.AdvertisingImageSwitcher.SetFactory(new AdvertisingViewFactory(Source.Activity));
            Source.AdvertisingImageSwitcher.SetInAnimation(Source.Activity, Resource.Animation.advertising_in);
            Source.AdvertisingImageSwitcher.SetOutAnimation(Source.Activity, Resource.Animation.advertising_out);
        }

        protected override void SourceOnResumed(object sender, EventArgs eventArgs)
        {
            base.SourceOnResumed(sender, eventArgs);

            if (!Preferences.Value.AdvertisingEnable) return;

            appDispatcher.StartSales += AppDispatcherOnStartSales;
            appDispatcher.FinishSales += AppDispatcherOnFinishSales;
        }

        protected override void SourceOnPaused(object sender, EventArgs eventArgs)
        {
            base.SourceOnPaused(sender, eventArgs);

            if (!Preferences.Value.AdvertisingEnable) return;

            appDispatcher.StartSales -= AppDispatcherOnStartSales;
            appDispatcher.FinishSales -= AppDispatcherOnFinishSales;
        }

        private void AppDispatcherOnStartSales(object sender, Check check)
        {
            advertisingProvider.MoveNext += AdvertisingProviderOnMoveNext;
        }

        private void AppDispatcherOnFinishSales(object sender, Check check)
        {
            advertisingProvider.MoveNext -= AdvertisingProviderOnMoveNext;

            Source.Activity.RunOnUiThread(() =>
            {
                Source.AdvertisingImageSwitcher.SetImageResource(Resource.Color.transperent);
            });
        }

        private Drawable advertisingCurrentDrawable;

        private void AdvertisingProviderOnMoveNext(object sender, AdvertisingScenario advertisingScenario)
        {
            if (!File.Exists(advertisingScenario.FilePath))
                return;

            var image = fileLoader.Download(advertisingScenario.FilePath);

            if (image.Length == 0)
                return;

            advertisingCurrentDrawable = null;
            advertisingCurrentDrawable = Drawable.CreateFromPath(advertisingScenario.FilePath);

            Source.Activity.RunOnUiThread(() =>
            {
                try
                {
                    Source.AdvertisingImageSwitcher.SetImageDrawable(advertisingCurrentDrawable);
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }
            });
        }
    }
}