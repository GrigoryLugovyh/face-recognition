﻿using Android.Content;
using Android.Views;
using Android.Widget;

namespace FR.Droid.Activities.Env.Main
{
    public class AdvertisingViewFactory : Java.Lang.Object, ViewSwitcher.IViewFactory
    {
        private readonly Context context;

        public AdvertisingViewFactory(Context context)
        {
            this.context = context;
        }

        public View MakeView()
        {
            var imageView = new ImageView(context);
            imageView.SetScaleType(ImageView.ScaleType.CenterInside);
            imageView.LayoutParameters =
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            return imageView;
        }
    }
}