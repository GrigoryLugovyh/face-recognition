﻿using Android.Widget;

namespace FR.Droid.Activities.Env.Main
{
    public interface ISalesSource : ISource<MainActivity>
    {
        LinearLayout SalesLayout { get; }

        ListView SalesList { get; }

        TextView SalesTotal { get; }
    }
}