﻿using System;
using Android.App;

namespace FR.Droid.Activities.Env
{
    public interface ISource
    {
        event EventHandler Created;

        event EventHandler Resumed;

        event EventHandler Paused;

        event EventHandler Destroied;
    }

    public interface ISource<out TActivity> : ISource where TActivity : Activity
    {
        TActivity Activity { get; }
    }
}