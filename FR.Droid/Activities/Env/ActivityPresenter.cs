﻿using System;
using Android.App;
using FR.Mobile;
using FR.Mobile.Events;
using FR.Mobile.Storage;

namespace FR.Droid.Activities.Env
{
    public abstract class ActivityPresenter<TActivity, TSource> : IActivityPresenter
        where TActivity : Activity where TSource : class, ISource<TActivity>
    {
        protected Lazy<IGlobalEvents> GlobalEvents = new Lazy<IGlobalEvents>(Container.Resolve<IGlobalEvents>);

        protected Lazy<IPreferences> Preferences = new Lazy<IPreferences>(Container.Resolve<IPreferences>);

        protected TSource Source => Container.Resolve<TSource>();

        public virtual void Subscribe()
        {
            Source.Created += SourceOnCreated;
            Source.Resumed += SourceOnResumed;
            Source.Paused += SourceOnPaused;
            Source.Destroied += SourceOnDestroied;
        }

        public void Unsubscribe()
        {
            Source.Created -= SourceOnCreated;
            Source.Resumed -= SourceOnResumed;
            Source.Paused -= SourceOnPaused;
            Source.Destroied -= SourceOnDestroied;
        }

        protected virtual void SourceOnCreated(object sender, EventArgs e)
        {
        }

        protected virtual void SourceOnResumed(object sender, EventArgs eventArgs)
        {
        }

        protected virtual void SourceOnPaused(object sender, EventArgs eventArgs)
        {
        }

        protected virtual void SourceOnDestroied(object sender, EventArgs eventArgs)
        {
        }
    }
}