﻿namespace FR.Droid.Activities.Env
{
    public interface IActivityPresenter
    {
        void Subscribe();

        void Unsubscribe();
    }
}