﻿using System;
using Android.Content;
using Android.Net.Wifi;
using Android.Text;
using Android.Text.Format;
using Android.Widget;
using FR.Mobile.Storage;

namespace FR.Droid.Activities.Env.Settings
{
    public class SettingsPresenter : ActivityPresenter<SettingsActivity, ISettingsSource>
    {
        protected override void SourceOnResumed(object sender, EventArgs eventArgs)
        {
            base.SourceOnResumed(sender, eventArgs);

            Source.Version.Text = GetCurrentVersion();
            Source.DeviceAddress.Text = GetDeviceIpAddress();
            Source.DeviceId.Text = Preferences.Value.DeviveId;
            Source.ServerAddress.Text = Preferences.Value.ServerAddress;
            Source.SendLogsEnable.Checked = Preferences.Value.LoggingEnable;
            Source.VoiceOverEnable.Checked = Preferences.Value.VoiceOverEnable;
            Source.AdvertisingEnable.Checked = Preferences.Value.AdvertisingEnable;
            Source.CameraPreviewEnable.Checked = Preferences.Value.CameraPreviewEnable;

            Source.ServerAddress.TextChanged += ServerAddressOnTextChanged;
            Source.SendLogsEnable.CheckedChange += SendLogsEnableOnCheckedChange;
            Source.VoiceOverEnable.CheckedChange += VoiceOverEnableOnCheckedChange;
            Source.AdvertisingEnable.CheckedChange += AdvertisingEnableOnCheckedChange;
            Source.CameraPreviewEnable.CheckedChange += CameraPreviewEnableOnCheckedChange;
        }

        protected override void SourceOnPaused(object sender, EventArgs eventArgs)
        {
            base.SourceOnPaused(sender, eventArgs);

            Source.ServerAddress.TextChanged -= ServerAddressOnTextChanged;
            Source.SendLogsEnable.CheckedChange -= SendLogsEnableOnCheckedChange;
            Source.VoiceOverEnable.CheckedChange -= VoiceOverEnableOnCheckedChange;
            Source.AdvertisingEnable.CheckedChange -= AdvertisingEnableOnCheckedChange;
            Source.CameraPreviewEnable.CheckedChange -= CameraPreviewEnableOnCheckedChange;
        }

        protected override void SourceOnDestroied(object sender, EventArgs eventArgs)
        {
            base.SourceOnDestroied(sender, eventArgs);

            Toast.MakeText(Source.Activity, Resource.String.settings_saved, ToastLength.Short).Show();
        }

        private void SendLogsEnableOnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            Preferences.Value.SetValue(DefaultPreferences.LoggingEnableKey, e.IsChecked.ToString());
        }

        private void CameraPreviewEnableOnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            Preferences.Value.SetValue(DefaultPreferences.CameraPreviewEnableKey, e.IsChecked.ToString());
            GlobalEvents.Value.RaiseCameraPreviewEnable(e.IsChecked);
        }

        private void ServerAddressOnTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            Preferences.Value.SetValue(DefaultPreferences.ServerAddressKey, Source.ServerAddress.Text);
        }

        private void VoiceOverEnableOnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            Preferences.Value.SetValue(DefaultPreferences.VoiceOverEnableKey, e.IsChecked.ToString());
        }

        private void AdvertisingEnableOnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            Preferences.Value.SetValue(DefaultPreferences.AdvertisingEnableKey, e.IsChecked.ToString());
        }

        private string GetDeviceIpAddress() // todo вынести в отдельный сервис
        {
#pragma warning disable CS0618 // Type or member is obsolete
            return Formatter.FormatIpAddress(
                ((WifiManager) Source.Activity.GetSystemService(Context.WifiService)).ConnectionInfo
                .IpAddress);
#pragma warning restore CS0618 // Type or member is obsolete
        }

        private string GetCurrentVersion() // todo вынести в отдельный сервис
        {
            var packageManager = Source.Activity.PackageManager.GetPackageInfo(Source.Activity.PackageName, 0);
            return $"v. {packageManager.VersionName} ({packageManager.VersionCode})";
        }
    }
}