﻿using Android.Widget;

namespace FR.Droid.Activities.Env.Settings
{
    public interface ISettingsSource : ISource<SettingsActivity>
    {
        EditText ServerAddress { get; }

        CheckBox CameraPreviewEnable { get; }

        CheckBox AdvertisingEnable { get; }

        CheckBox SendLogsEnable { get; }

        CheckBox VoiceOverEnable { get; }

        TextView DeviceAddress { get; }

        TextView DeviceId { get; }

        TextView Version { get; }
    }
}