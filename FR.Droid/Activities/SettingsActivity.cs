﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using FR.Droid.Activities.Env.Settings;
using FR.Mobile;

#pragma warning disable 618

namespace FR.Droid.Activities
{
    [Activity(Label = "@string/app_settings", Theme = "@style/Theme.AppCompat.Light.Dialog",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SettingsActivity : SourceActivity, ISettingsSource
    {
        public SettingsActivity Activity => this;

        public EditText ServerAddress { get; private set; }

        public CheckBox CameraPreviewEnable { get; private set; }

        public CheckBox AdvertisingEnable { get; private set; }

        public CheckBox SendLogsEnable { get; private set; }

        public CheckBox VoiceOverEnable { get; private set; }

        public TextView DeviceAddress { get; private set; }

        public TextView DeviceId { get; private set; }

        public TextView Version { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            SetContentView(Resource.Layout.Settings);
            base.OnCreate(savedInstanceState);
        }

        protected override void Creating()
        {
            base.Creating();

            Version = FindViewById<TextView>(Resource.Id.Version);
            DeviceId = FindViewById<TextView>(Resource.Id.DeviceId);
            DeviceAddress = FindViewById<TextView>(Resource.Id.DeviceAddress);
            ServerAddress = FindViewById<EditText>(Resource.Id.ServerAddress);
            SendLogsEnable = FindViewById<CheckBox>(Resource.Id.SendLogsEnable);
            VoiceOverEnable = FindViewById<CheckBox>(Resource.Id.VoiceOverEnable);
            AdvertisingEnable = FindViewById<CheckBox>(Resource.Id.AdvertisingEnable);
            CameraPreviewEnable = FindViewById<CheckBox>(Resource.Id.CameraPreviewEnable);

            Container.AddRegistration<ISettingsSource>(this);
        }
    }
}