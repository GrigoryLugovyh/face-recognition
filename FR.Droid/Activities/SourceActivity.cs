﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.OS;
using FR.Droid.Activities.Env;
using FR.Droid.Extensions;
using FR.Mobile;

namespace FR.Droid.Activities
{
    public abstract class SourceActivity : Activity, ISource
    {
        public event EventHandler Created;

        public event EventHandler Resumed;

        public event EventHandler Paused;

        public event EventHandler Destroied;

        private IActivityPresenter[] presenters;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Creating();

            presenters = GetCurrentPresenters.Subscribe().ToArray();

            Created?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnResume()
        {
            base.OnResume();

            Resuming();

            Resumed?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnPause()
        {
            base.OnPause();

            Pausing();

            Paused?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            Destroing();

            Destroied?.Invoke(this, EventArgs.Empty);

            presenters.Unsubscribe();
        }

        protected virtual void Creating()
        {
        }

        protected virtual void Resuming()
        {
        }

        protected virtual void Pausing()
        {
        }

        protected virtual void Destroing()
        {
        }

        private IEnumerable<IActivityPresenter> GetCurrentPresenters => Container.ResolveAll<IActivityPresenter>()
            .ToDictionary(presenter => presenter,
                presenter => presenter.GetType().BaseType?.GetGenericArguments())
            .Where(pair => pair.Value.FirstOrDefault(type => type == GetType()) != null).Select(pair => pair.Key);
    }
}