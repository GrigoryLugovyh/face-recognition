﻿using System;
using Android.App;
using Android.Content;
using FR.Droid.Receivers;
using Java.Lang;

namespace FR.Droid.Services.Base
{
    public class DroidServiceSheduler : IDroidServiceSheduler
    {
        public void Schedule<TBroadcastReceiver>(long delayPeriodMillis = 30000)
            where TBroadcastReceiver : DroidProlongBroadcastReceiver
        {
            ProlongService<TBroadcastReceiver>(delayPeriodMillis);
        }

        public void ProlongService<TBroadcastReceiver>(long delayPeriodMillis = 1, int pendingRequestCode = -1)
            where TBroadcastReceiver : DroidProlongBroadcastReceiver
        {
            ProlongService(typeof(TBroadcastReceiver), delayPeriodMillis, pendingRequestCode);
        }

        public void ProlongService(Type broadcastReceiverType, long delayPeriodMillis = 1, int pendingRequestCode = -1)
        {
            var context = Application.Context;

            var alarm =
                (AlarmManager) context.GetSystemService(Context.AlarmService);

            var receiverIntent = new Intent(context, broadcastReceiverType);

            var pendingServiceIntent = PendingIntent.GetBroadcast(context, pendingRequestCode, receiverIntent,
                PendingIntentFlags.UpdateCurrent);

            alarm.Set(AlarmType.Rtc, JavaSystem.CurrentTimeMillis() + delayPeriodMillis, pendingServiceIntent);
        }

        public void RepeatService(Intent service, int timeout)
        {
            if (AlarmSet(service)) return;

            var alarm = (AlarmManager) Application.Context.GetSystemService(Context.AlarmService);

            var pendingServiceIntent = PendingIntent.GetService(Application.Context, 0, service,
                PendingIntentFlags.CancelCurrent);

            alarm.SetRepeating(AlarmType.Rtc, 0, timeout, pendingServiceIntent);
        }

        private static bool AlarmSet(Intent service)
        {
            return PendingIntent.GetBroadcast(Application.Context, 0, service, PendingIntentFlags.NoCreate) != null;
        }
    }
}