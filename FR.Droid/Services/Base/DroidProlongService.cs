﻿using System;
using Android.App;
using Android.Content;
using FR.Droid.Receivers;
using FR.Mobile;
using FR.Mobile.App;

namespace FR.Droid.Services.Base
{
    [Service]
    public abstract class DroidProlongService<TBroadcastReceiver> : IntentService
        where TBroadcastReceiver : DroidProlongBroadcastReceiver
    {
        private readonly Lazy<IDroidServiceSheduler> sheduler =
            new Lazy<IDroidServiceSheduler>(Container.Resolve<IDroidServiceSheduler>);

        protected abstract string ServiceName { get; }

        protected virtual int PendingRequestCode => -1;

        protected virtual long DelayPeriodMillis => 60 * 1000;

        public override void OnCreate()
        {
            base.OnCreate();

            AppModel.Instance<DroidModel>();
        }

        protected abstract void Execute(Context context, Intent intent);

        protected override void OnHandleIntent(Intent intent)
        {
            Logger.Info($"Service \"{ServiceName}\" starting ...");

            try
            {
                Execute(this, intent);
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            finally
            {
                sheduler.Value.ProlongService<TBroadcastReceiver>(DelayPeriodMillis, PendingRequestCode);

                Logger.Info($"Service \"{ServiceName}\" was planned through {DelayPeriodMillis} ms");
            }

            Logger.Info($"Service \"{ServiceName}\" executed");
        }
    }
}