﻿using System;
using Android.Content;
using FR.Droid.Receivers;

namespace FR.Droid.Services.Base
{
    public interface IDroidServiceSheduler
    {
        void Schedule<TBroadcastReceiver>(long delayPeriodMillis = 30000)
            where TBroadcastReceiver : DroidProlongBroadcastReceiver;

        void ProlongService<TBroadcastReceiver>(long delayPeriodMillis = 1, int pendingRequestCode = -1)
            where TBroadcastReceiver : DroidProlongBroadcastReceiver;

        void ProlongService(Type broadcastReceiverType, long delayPeriodMillis = 1, int pendingRequestCode = -1);

        void RepeatService(Intent service, int timeout);
    }
}