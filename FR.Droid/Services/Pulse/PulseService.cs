﻿using Android.App;
using Android.Content;
using FR.Droid.Receivers;
using FR.Droid.Services.Base;
using FR.Mobile;
using FR.Mobile.Services;

namespace FR.Droid.Services.Pulse
{
    [Service]
    public class PulseService : DroidProlongService<PulseBroadcastReciver>
    {
        protected override string ServiceName => "Pulse service";

        protected override void Execute(Context context, Intent intent) => Container.Resolve<ISyncService>().Run();
    }
}