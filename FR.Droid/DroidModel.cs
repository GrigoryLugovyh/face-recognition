﻿using System.Linq;
using System.Reflection;
using Android.App;
using Android.Content;
using Android.Gms.Vision;
using Android.Graphics;
using Android.Net.Wifi;
using FR.Droid.Activities.Env;
using FR.Droid.Infrastructure;
using FR.Droid.Infrastructure.Speech;
using FR.Droid.Infrastructure.Vision;
using FR.Droid.Receivers;
using FR.Droid.Services.Base;
using FR.Mobile;
using FR.Mobile.Advertising;
using FR.Mobile.App;
using FR.Mobile.Infrastructure.Speech;
using FR.Mobile.Services;
using FR.Mobile.Storage;
using FR.Mobile.Vision;

namespace FR.Droid
{
    public class DroidModel : AppModel
    {
        protected override void Register()
        {
            base.Register();

            RegisterVision();
            RegisterServices();
            RegisterReceivers();
            RegisterRepositories();

            RegisterEnvironment();
        }

        private static void RegisterVision()
        {
            InitVisionProcessorFactory();

            Container.AddRegistration<IVisionOptions, CommonVisionOptions>().AsMultiInstance();
        }

        private static void RegisterServices()
        {
            Container.AddRegistration<IDroidServiceSheduler, DroidServiceSheduler>();
        }

        private static void RegisterReceivers()
        {
            InitPulseReciver();
            InitWifiReciver();
        }

        private static void RegisterRepositories()
        {
            Container.RegisterMultiple<IActivityPresenter>(Assembly.GetAssembly(typeof(DroidModel)).GetTypes()
                .Where(type => type.IsClass && !type.IsAbstract && typeof(IActivityPresenter).IsAssignableFrom(type)));
        }

        private static void RegisterEnvironment()
        {
            FontsOverride.SetDefaultFont("MONOSPACE", "fonts/fr-mobile.ttf");
        }

        protected override IEventService CreateLoggerService()
        {
            return new DroidEventService();
        }

        protected override IPreferences CreatePreferences()
        {
            return new DroidPreferences();
        }

        protected override IVisionDispatcher CreateVisionDispatcher()
        {
            return new DroidVisionDispatcher();
        }

        protected override IVisionDispatcher InitVisionDispatcher()
        {
            var dispatcher = base.InitVisionDispatcher() as IDroidVisionDispatcher;
            Container.AddRegistration(dispatcher);
            return dispatcher;
        }

        private static void InitVisionProcessorFactory()
        {
            var visionProcessorFactory = CreateVisionProcessorFactory();
            Container.AddRegistration<MultiProcessor.IFactory>(visionProcessorFactory);
            Container.AddRegistration(visionProcessorFactory);
        }

        private static IVisionProcessorFactory CreateVisionProcessorFactory()
        {
            return new VisionProcessorFactory();
        }

        private static void InitPulseReciver()
        {
            Container.Resolve<IDroidServiceSheduler>().Schedule<PulseBroadcastReciver>();
        }

        private static void InitWifiReciver()
        {
            var intentFilter = new IntentFilter();
            intentFilter.AddAction(WifiManager.NetworkStateChangedAction);
            Application.Context.RegisterReceiver(new WifiConnectionReceiver(), intentFilter);
        }

        protected override IAdvertisingRegistry CreateAdvertisingRegistry()
        {
            return new DroidResourceAdvertisingRegistry();
        }

        protected override ITextToSpeech CreateTextToSpeech()
        {
            return new DroidTextToSpeech();
        }
    }

    public static class FontsOverride
    {
        public static void SetDefaultFont(string staticTypefaceFieldName, string fontAssetName)
        {
            var regular = Typeface.CreateFromAsset(Application.Context.Assets, fontAssetName);
            ReplaceFont(staticTypefaceFieldName, regular);
        }

        private static void ReplaceFont(string staticTypefaceFieldName, Typeface newTypeface)
        {
            var staticField = newTypeface.Class.GetDeclaredField(staticTypefaceFieldName);
            staticField.Accessible = true;
            staticField.Set(null, newTypeface);
        }
    }
}