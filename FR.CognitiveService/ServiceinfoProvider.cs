﻿using System;
using System.Collections.Generic;
using CommonModel.Services;
using FR.Model;

namespace FR.CognitiveService
{
    public class ServiceInfoProvider : IServiceInfoProvider
    {
        public string GetCurrentService()
        {
            return Service.Cognitive;
        }

        public Dictionary<Type, string> TaskDataToServiceMap => new Dictionary<Type, string>();
    }
}