﻿using FR.Handling.Cognitive;

namespace FR.CognitiveService
{
    internal class EntryPoint : ServiceBase.ServiceBase
    {
        private EntryPoint()
        {
        }

        private static void Main(string[] args)
        {
            new EntryPoint().Run(args: args);
        }

        protected override void Configure()
        {
            base.Configure();

            RegisterHandler<DetectFaceTaskHandler>();
        }
    }
}