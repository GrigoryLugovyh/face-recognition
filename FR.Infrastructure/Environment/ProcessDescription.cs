﻿using System;
using System.Collections.Generic;

namespace FR.Infrastructure.Environment
{
    public class ProcessDescription : IContractProcessDescription, IHandleProcessDescription,
        IRetryProcessDescription, IExecuteProcessDescription
    {
        public enum ExecuteProcessStatus
        {
            Success,
            Fail
        }

        private object functionExecution, functionResult;

        // >>> IExecuteProcessDescription

        public Action ActionExecutionFinally { get; set; }

        public ExecuteProcessStatus ProcessStatus { get; set; }

        public bool ExecutedWithoutExceptions { get; set; }

        public Exception LastException { get; set; }

        public long ExecutionTimeTicks { get; set; } = 0;

        public TimeSpan ExecutionTimeSpan => TimeSpan.FromTicks(ExecutionTimeTicks);

        // >>> IActionProcessDescription

        public Action ActionExecution { get; set; }

        // >>> IFunctionProcessDescription

        public void SetFunction<TReturn>(Func<TReturn> func)
        {
            functionExecution = func;
        }

        public Func<TReturn> GetFunction<TReturn>()
        {
            return functionExecution as Func<TReturn>;
        }


        public void SetResult<TReturn>(TReturn result)
        {
            functionResult = result;
        }

        public TReturn GetResult<TReturn>()
        {
            return (TReturn) functionResult;
        }

        public TReturn TryGetResult<TReturn>()
        {
            try
            {
                return GetResult<TReturn>();
            }
            catch (Exception)
            {
                return default(TReturn);
            }
        }

        // >>> IHandleProcessDescription

        public List<Type> HandableExceptionTypes { get; set; } = new List<Type>();

        public IDictionary<Type, (Delegate exceptionDelegate, Action<Exception> exceptionAction, Func<Exception>
            customExceptionAfterAction)> HandableExceptionResults { get; set; } =
            new Dictionary<Type, (Delegate, Action<Exception>, Func<Exception>)>();

        // >>> IRetryProcessDescription 

        public int Retries { get; set; } = 1;
    }
}