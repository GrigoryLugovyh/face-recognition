﻿using System;

namespace FR.Infrastructure.Environment
{
    public interface IFunctionExecuteProcessDescription
    {
        void SetFunction<TReturn>(Func<TReturn> func);

        Func<TReturn> GetFunction<TReturn>();

        void SetResult<TReturn>(TReturn result);

        TReturn GetResult<TReturn>();

        TReturn TryGetResult<TReturn>();
    }
}