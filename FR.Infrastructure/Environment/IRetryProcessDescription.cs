﻿namespace FR.Infrastructure.Environment
{
    public interface IRetryProcessDescription
    {
        int Retries { get; set; }
    }
}