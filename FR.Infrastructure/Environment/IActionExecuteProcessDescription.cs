﻿using System;

namespace FR.Infrastructure.Environment
{
    public interface IActionExecuteProcessDescription
    {
        Action ActionExecution { get; set; }

        Action ActionExecutionFinally { get; set; }
    }
}