﻿using System;
using System.Linq;
using CommonModel.IoCContainer;

namespace FR.Infrastructure.Environment
{
    public static class Contractor
    {
        private static IContractService Contract => Container.Resolve<IContractService>();

        public static IContractProcessDescription Assert(Func<bool> condition, string message = "")
        {
            return Contract.Assert(condition, message);
        }

        public static IContractProcessDescription AndAlso(this IContractProcessDescription processDescription,
            Func<bool> condition, string message = "")
        {
            return Assert(condition, message);
        }

        public static IContractProcessDescription Assert<TException>(Func<bool> condition, TException exception)
            where TException : Exception
        {
            return Contract.Assert(condition, exception);
        }

        public static IContractProcessDescription Assert<TException>(Func<bool> condition, Func<TException> exception)
            where TException : Exception
        {
            return Contract.Assert(condition, exception.Invoke());
        }

        public static IContractProcessDescription AssertAllNotNullOrEmpty(params string[] values)
        {
            return Contract.Assert(() => values.All(s => !string.IsNullOrEmpty(s)));
        }

        public static IContractProcessDescription AssertNotNullOrEmpty(string value, string message)
        {
            return Contract.Assert(() => !string.IsNullOrEmpty(value), message);
        }

        public static IContractProcessDescription AssertNotNull(object value, string message)
        {
            return Contract.Assert(() => value != null, message);
        }

        public static IContractProcessDescription AssertNotNull(object value, Exception exception)
        {
            return Contract.Assert(() => value != null, exception);
        }

        public static IContractProcessDescription AssertIsTrue(bool condition)
        {
            return Contract.Assert(() => condition);
        }
    }
}