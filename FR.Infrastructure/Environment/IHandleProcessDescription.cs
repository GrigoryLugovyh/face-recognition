﻿using System;
using System.Collections.Generic;

namespace FR.Infrastructure.Environment
{
    public interface IHandleProcessDescription
    {
        List<Type> HandableExceptionTypes { get; set; }

        IDictionary<Type, (Delegate exceptionDelegate, Action<Exception> exceptionAction, Func<Exception>
            customExceptionAfterAction)> HandableExceptionResults { get; set; }
    }
}