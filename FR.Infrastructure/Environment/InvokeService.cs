﻿using System;
using System.Linq;
using FR.Infrastructure.Exceptions;

namespace FR.Infrastructure.Environment
{
    public class InvokeService : IInvokeService
    {
        public IExecuteProcessDescription Execute(IExecuteProcessDescription executeProcessDescription) => ExecuteImpl(executeProcessDescription as ProcessDescription,
            () =>
            {
                executeProcessDescription.ActionExecution.Invoke();
            });

        public IExecuteProcessDescription Execute<TReturn>(IExecuteProcessDescription executeProcessDescription) => ExecuteImpl(executeProcessDescription as ProcessDescription,
            () =>
            {
                executeProcessDescription.SetResult(executeProcessDescription.GetFunction<TReturn>().Invoke());
            });

        protected virtual ProcessDescription ExecuteImpl(ProcessDescription processDescription, Action internalAction)
        {
            Contractor.Assert(() => processDescription != null, $"{nameof(processDescription)} can not be null");

            var count = 0;

            do
            {
                var timeStart = System.Environment.TickCount;

                try
                {
                    internalAction();

                    processDescription.ExecutedWithoutExceptions = true;
                    processDescription.ProcessStatus = ProcessDescription.ExecuteProcessStatus.Success;

                    break;
                }
                catch (Exception exception)
                {
                    processDescription.LastException = exception;
                    processDescription.ExecutedWithoutExceptions = false;
                    processDescription.ProcessStatus = ProcessDescription.ExecuteProcessStatus.Fail;

                    var handableExceptionResult =
                        processDescription.HandableExceptionResults.FirstOrDefault(
                            pair => pair.Key == exception.GetType() || pair.Key.IsInstanceOfType(exception));

                    (Delegate exceptionDelegate, Action<Exception> exceptionAction, Func<Exception> customExceptionAfterAction) value =
                        handableExceptionResult.Value;

                    Contractor.Assert(() => ++count < processDescription.Retries || processDescription.Retries <= 1,
                        new FrInvokeRetriesException($"There were `{processDescription.Retries}` tries to invoke, but",
                            exception,
                            processDescription));

                    Contractor.Assert(
                        () => processDescription.HandableExceptionTypes.Any(
                                  exceptionType => exception.GetType() == exceptionType ||
                                                   exceptionType.IsInstanceOfType(exception)) && value.customExceptionAfterAction == null,
                        value.customExceptionAfterAction ?? (() => new FrInvokeUnhandableException($"Try to process unhandable exception `{exception.GetType()}`",
                            exception, processDescription)));

                    Contractor.Assert(
                        () => value.Equals(default((Delegate, Action<Exception>, Func<Exception>)))
                              || value.exceptionDelegate == null ||
                              (bool)value.exceptionDelegate.DynamicInvoke(exception),
                        new FrInvokeUnknownResultException("Try to process unknown result", exception,
                            processDescription));

                    Contractor.Assert(
                        () => value.Equals(default((Delegate, Action<Exception>, Func<Exception>))) || value.exceptionAction == null ||
                              Invoker.Execute(() => value.exceptionAction(exception)).ExecutedWithoutExceptions,
                        new FrInvokeExceptionActionException("Exception was in action exception",
                            processDescription.LastException,
                            processDescription));

                    if (processDescription.Retries == 1)
                        break;
                }
                finally
                {
                    if (processDescription.ActionExecutionFinally != null)
                        Invoker.Execute(processDescription.ActionExecutionFinally);

                    processDescription.ExecutionTimeTicks = System.Environment.TickCount - timeStart;
                }
            } while (true);

            return processDescription;
        }
    }
}