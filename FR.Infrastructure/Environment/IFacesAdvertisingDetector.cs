﻿using System.Collections.Generic;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Environment
{
    public interface IFacesAdvertisingDetector
    {
        Face Detect(List<Face> faceCandidate);
    }
}