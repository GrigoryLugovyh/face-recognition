﻿using System;
using CommonModel.IoCContainer;
using FR.Infrastructure.Exceptions;

namespace FR.Infrastructure.Environment
{
    public class ContractService : IContractService
    {
        public IContractProcessDescription Assert(Func<bool> condition, string message = "")
        {
            if (!condition.Invoke())
                throw new FrContractException("Condition failed" +
                                              (!string.IsNullOrEmpty(message) ? $": {message}" : string.Empty));

            return Container.Resolve<IContractProcessDescription>();
        }

        public IContractProcessDescription Assert<TException>(Func<bool> condition, TException exception)
            where TException : Exception
        {
            if (!condition.Invoke())
                throw exception;

            return Container.Resolve<IContractProcessDescription>();
        }
    }
}