﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CommonModel.IoCContainer;

namespace FR.Infrastructure.Environment
{
    public static class Invoker
    {
        private static IHandleProcessDescription HandleImpl<TException>(
            IHandleProcessDescription processDescription, Expression<Func<TException, bool>> expressionOnResult = null,
            Action<Exception> actionAfterException = null, Func<Exception> customExceptionAfterAction = null)
            where TException : Exception
        {
            Contractor.Assert(() => processDescription != null, $"{processDescription} can not be null");

            var exceptionType = typeof(TException);

            if (processDescription.HandableExceptionTypes.All(type => type != exceptionType))
                processDescription.HandableExceptionTypes.Add(exceptionType);

            processDescription.HandableExceptionResults.Add(typeof(TException),
                (expressionOnResult?.Compile(), actionAfterException, customExceptionAfterAction));

            return processDescription;
        }

        private static IRetryProcessDescription RetryImpl(this IRetryProcessDescription retryProcessDescription,
            int retries)
        {
            Contractor.Assert(() => retryProcessDescription != null, $"{retryProcessDescription} can not be null");
            Contractor.Assert(() => retries > 0, $"{nameof(retries)} must be more than 0");

            retryProcessDescription.Retries = retries;

            return retryProcessDescription;
        }

        private static IExecuteProcessDescription ExecuteImpl(this IExecuteProcessDescription executeProcessDescription,
            Action action,
            Action actionFinally = null)
        {
            Contractor.Assert(() => action != null, $"{nameof(action)} can not be null");
            Contractor.Assert(() => executeProcessDescription != null, $"{executeProcessDescription} can not be null");

            executeProcessDescription.ActionExecution = action;
            executeProcessDescription.ActionExecutionFinally = actionFinally;

            return Container.Resolve<IInvokeService>().Execute(executeProcessDescription);
        }

        private static IExecuteProcessDescription ExecuteImpl<TReturn>(
            this IExecuteProcessDescription executeProcessDescription,
            Func<TReturn> func,
            Action actionFinally = null)
        {
            Contractor.Assert(() => func != null, $"{nameof(func)} can not be null");
            Contractor.Assert(() => executeProcessDescription != null, $"{executeProcessDescription} can not be null");

            executeProcessDescription.SetFunction(func);
            executeProcessDescription.ActionExecutionFinally = actionFinally;

            return Container.Resolve<IInvokeService>().Execute<TReturn>(executeProcessDescription);
        }

        public static IHandleProcessDescription Handle<TException>(
            Expression<Func<TException, bool>> expressionOnResult = null, Action<Exception> actionAfterException = null,
            Func<Exception> customExceptionAfterAction = null)
            where TException : Exception => Container.Resolve<IHandleProcessDescription>()
            .Handle(expressionOnResult, actionAfterException, customExceptionAfterAction);

        public static IHandleProcessDescription Handle<TException>(
            this IContractProcessDescription contractProcessDescription,
            Expression<Func<TException, bool>> expressionOnResult = null, Action<Exception> actionAfterException = null,
            Func<Exception> customExceptionAfterAction = null) where TException : Exception => HandleImpl(contractProcessDescription as IHandleProcessDescription, expressionOnResult,
            actionAfterException, customExceptionAfterAction);

        public static IRetryProcessDescription Retry(this IContractProcessDescription contractProcessDescription,
            int retries) => RetryImpl(contractProcessDescription as IRetryProcessDescription, retries);

        public static IExecuteProcessDescription Execute(this IContractProcessDescription contractProcessDescription,
            Action action,
            Action actionFinally = null) => ExecuteImpl(contractProcessDescription as IExecuteProcessDescription, action, actionFinally);

        public static IExecuteProcessDescription Execute<TReturn>(
            this IContractProcessDescription contractProcessDescription, Func<TReturn> func,
            Action actionFinally = null) => ExecuteImpl(contractProcessDescription as IExecuteProcessDescription, func, actionFinally);

        public static IHandleProcessDescription Handle<TException>(
            this IHandleProcessDescription handleProcessDescription,
            Expression<Func<TException, bool>> expressionOnResult = null, Action<Exception> actionAfterException = null,
            Func<Exception> customExceptionAfterAction = null) where TException : Exception => HandleImpl(handleProcessDescription, expressionOnResult, actionAfterException,
            customExceptionAfterAction);

        public static IRetryProcessDescription Retry(this IHandleProcessDescription handleProcessDescription,
            int retries) => RetryImpl(handleProcessDescription as IRetryProcessDescription, retries);

        public static IRetryProcessDescription RetryInfinity(this IHandleProcessDescription handleProcessDescription) => RetryImpl(handleProcessDescription as IRetryProcessDescription, int.MaxValue);

        public static IExecuteProcessDescription Execute(this IHandleProcessDescription handleProcessDescription,
            Action action,
            Action actionFinally = null) => ExecuteImpl(handleProcessDescription as IExecuteProcessDescription, action, actionFinally);

        public static IExecuteProcessDescription Execute(Action action, Action actionFinally = null) => Container
            .Resolve<IExecuteProcessDescription>().ExecuteImpl(action, actionFinally);

        public static IExecuteProcessDescription Execute<TReturn>(
            this IHandleProcessDescription handleProcessDescription, Func<TReturn> func,
            Action actionFinally = null) => ExecuteImpl(handleProcessDescription as IExecuteProcessDescription, func, actionFinally);

        public static IExecuteProcessDescription Execute(this IRetryProcessDescription retryProcessDescription,
            Action action,
            Action actionFinally = null) => ExecuteImpl(retryProcessDescription as IExecuteProcessDescription, action, actionFinally);

        public static IExecuteProcessDescription Execute<TReturn>(this IRetryProcessDescription retryProcessDescription,
            Func<TReturn> func,
            Action actionFinally = null) => ExecuteImpl(retryProcessDescription as IExecuteProcessDescription, func, actionFinally);

        public static IExecuteProcessDescription Execute<TReturn>(Func<TReturn> function,
            Action actionFinally = null) => Container.Resolve<IExecuteProcessDescription>().ExecuteImpl(function, actionFinally);
    }
}