﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Environment
{
    public class FacesAdvertisingDetector : IFacesAdvertisingDetector
    {
        public Face Detect(List<Face> faceCandidate)
        {
            if (faceCandidate.Count == 1)
                return faceCandidate.First();

            var candidate = faceCandidate
                .Select(face => new {face.FaceId, s = face.FaceRectangle.Height * face.FaceRectangle.Width})
                .OrderByDescending(arg => arg.s).First();

            return faceCandidate.First(face => face.FaceId == candidate.FaceId);
        }
    }
}