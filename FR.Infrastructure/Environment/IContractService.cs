﻿using System;

namespace FR.Infrastructure.Environment
{
    public interface IContractService
    {
        IContractProcessDescription Assert(Func<bool> condition, string message = "");

        IContractProcessDescription Assert<TException>(Func<bool> condition, TException exception)
            where TException : Exception;
    }
}