﻿using System;

namespace FR.Infrastructure.Environment
{
    public interface IExecuteProcessDescription : IActionExecuteProcessDescription, IFunctionExecuteProcessDescription
    {
        ProcessDescription.ExecuteProcessStatus ProcessStatus { get; set; }

        bool ExecutedWithoutExceptions { get; set; }

        Exception LastException { get; set; }

        long ExecutionTimeTicks { get; set; }

        TimeSpan ExecutionTimeSpan { get; }
    }
}