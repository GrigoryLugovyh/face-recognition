﻿namespace FR.Infrastructure.Environment
{
    public interface IInvokeService
    {
        IExecuteProcessDescription Execute(IExecuteProcessDescription executeProcessDescription);

        IExecuteProcessDescription Execute<TReturn>(IExecuteProcessDescription executeProcessDescription);
    }
}