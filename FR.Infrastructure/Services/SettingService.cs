﻿using CommonModel.IoCContainer;
using CommonModel.Services.ApplicationSettings;
using FR.Model;

namespace FR.Infrastructure.Services
{
    public abstract class SettingService : Service
    {
        private IApplicationSettings settings;

        protected IApplicationSettings Settings => settings ?? (settings = Container.Resolve<IApplicationSettings>());
    }
}