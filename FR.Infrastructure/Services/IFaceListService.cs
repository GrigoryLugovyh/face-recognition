using System.Collections.Generic;
using Microsoft.ProjectOxford.Face.Contract;
using FaceList = FR.Model.Models.FaceList;

namespace FR.Infrastructure.Services
{
    public interface IFaceListService
    {
        FaceList GetOrAddActualFaceList();

        bool AddFaceToFaceList(string faceId, IEnumerable<SimilarPersistedFace> similarPersistedFaces);
    }
}