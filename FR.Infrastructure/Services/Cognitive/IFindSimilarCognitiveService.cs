﻿using System;
using System.Collections.Generic;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public interface IFindSimilarCognitiveService : ICognitiveService
    {
        IEnumerable<SimilarPersistedFace> Find(string faceId, string faceListId);

        IEnumerable<SimilarPersistedFace> Find(Guid faceId, string faceListId);
    }
}