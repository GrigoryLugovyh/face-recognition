﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public interface IFaceDetectCognitiveService : ICognitiveService
    {
        Task<IEnumerable<Face>> Detect(Stream imageStream);
    }
}