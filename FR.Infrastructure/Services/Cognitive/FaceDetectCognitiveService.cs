﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public class FaceDetectCognitiveService : CognitiveService, IFaceDetectCognitiveService
    {
        public async Task<IEnumerable<Face>> Detect(Stream imageStream)
        {
            Logger.Info("Face(s) detecting ...");

            var faces = await FaceServiceClient.DetectAsync(imageStream, true, true, FaceAttributes);

            Logger.Info($"Detected {faces.Length} face(s)");

            return faces;
        }
    }
}