﻿using System;
using System.Collections.Generic;
using System.Linq;
using FR.Infrastructure.Environment;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public class FindSimilarCognitiveService : CognitiveService, IFindSimilarCognitiveService
    {
        public IEnumerable<SimilarPersistedFace> Find(string faceId, string faceListId)
        {
            return Find(Guid.Parse(faceId), faceListId);
        }

        public IEnumerable<SimilarPersistedFace> Find(Guid faceId, string faceListId)
        {
            var findResult = Invoker.Handle<Exception>().Execute(() => FaceServiceClient
                .FindSimilarAsync(faceId, faceListId, FindSimilarMatchMode.matchPerson, 1).Result);

            return findResult.ExecutedWithoutExceptions
                ? findResult.TryGetResult<SimilarPersistedFace[]>()
                : Enumerable.Empty<SimilarPersistedFace>();
        }
    }
}