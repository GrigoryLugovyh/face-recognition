﻿using System.Collections.Generic;
using log4net;
using Microsoft.ProjectOxford.Face;

namespace FR.Infrastructure.Services.Cognitive
{
    public abstract class CognitiveService : SettingService, ICognitiveService
    {
        protected CognitiveService()
        {
            FaceServiceClient = new FaceServiceClient(SubscriptionKey, UriBase);

            Logger = LogManager.GetLogger(GetType().Name);
        }

        protected ILog Logger { get; }

        protected IFaceServiceClient FaceServiceClient { get; }

        protected string UriBase => Settings.GetString("CognitiveUri");

        protected string SubscriptionKey => Settings.GetString("CognitiveSubscriptionKey");

        protected virtual IEnumerable<FaceAttributeType> FaceAttributes
        {
            get
            {
                yield return FaceAttributeType.Gender;
                yield return FaceAttributeType.Age;
            }
        }
    }
}