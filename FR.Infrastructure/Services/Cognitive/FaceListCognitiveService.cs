﻿using System;
using System.IO;
using FR.Infrastructure.Environment;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public class FaceListCognitiveService : CognitiveService, IFaceListCognitiveService
    {
        public bool CreateFaceList(string faceListId, string faceListName, string faceListUserData = "")
        {
            var createFaceListResult = Invoker.Handle<Exception>().Execute(() =>
            {
                FaceServiceClient.CreateFaceListAsync(faceListId, faceListName, faceListUserData).Wait();
            });

            return createFaceListResult.ExecutedWithoutExceptions;
        }

        public AddPersistedFaceResult AddFaceToFaceList(Stream imageStream, string faceListId,
            string faceUserData = "", FaceRectangle faceRectangle = null)
        {
            var addFaceToFaceListResult = Invoker.Handle<Exception>().Execute(() => FaceServiceClient
                .AddFaceToFaceListAsync(faceListId, imageStream, faceUserData,
                    faceRectangle).Result);

            return addFaceToFaceListResult.ExecutedWithoutExceptions
                ? addFaceToFaceListResult.TryGetResult<AddPersistedFaceResult>()
                : null;
        }
    }
}