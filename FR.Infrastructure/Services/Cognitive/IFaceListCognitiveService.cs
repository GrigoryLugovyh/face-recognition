﻿using System.IO;
using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public interface IFaceListCognitiveService : ICognitiveService
    {
        bool CreateFaceList(string faceListId, string faceListName, string faceListUserData = "");

        AddPersistedFaceResult AddFaceToFaceList(Stream imageStream, string faceListId,
            string faceUserData = "", FaceRectangle faceRectangle = null);
    }
}