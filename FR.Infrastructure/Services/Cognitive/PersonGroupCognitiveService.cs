﻿using System;
using CommonModel.Extensions;
using FR.Infrastructure.Environment;
using Microsoft.ProjectOxford.Face.Contract;
using MongoDB.Bson;

namespace FR.Infrastructure.Services.Cognitive
{
    public class PersonGroupCognitiveService : CognitiveService, IPersonGroupCognitiveService
    {
        public PersonGroup GetPersonGroup(string personGroupId)
        {
            Contractor.AssertAllNotNullOrEmpty(personGroupId);

            Logger.Info($"Getting person group by id \"{personGroupId}\" ...");

            var processResult = Invoker.Handle<Exception>()
                .Execute(() => FaceServiceClient.GetPersonGroupAsync(personGroupId).Result);

            Logger.Info(
                $"Person group exists: \"{processResult.ExecutedWithoutExceptions}\", for {processResult.ExecutionTimeTicks} ms");

            if (processResult.LastException != null)
                Logger.Error(processResult.LastException.ToLong());

            return processResult.TryGetResult<PersonGroup>();
        }

        public bool CreatePersonGroup(string personGroupId, string name, string userData = null)
        {
            Contractor.AssertAllNotNullOrEmpty(personGroupId, name);

            Logger.Info($"Creating  person group with id \"{personGroupId}\" ...");

            var processResult = Invoker.Handle<Exception>().Execute(() =>
                FaceServiceClient.CreatePersonGroupAsync(personGroupId, name, userData).Wait());

            Logger.Info(
                $"Person group was created with result: \"{processResult.ExecutedWithoutExceptions}\", for {processResult.ExecutionTimeTicks} ms");

            if (processResult.LastException != null)
                Logger.Error(processResult.LastException.ToLong());

            return processResult.ExecutedWithoutExceptions;
        }

        public bool Train(string personGroupId)
        {
            Contractor.AssertAllNotNullOrEmpty(personGroupId);

            var trainResult = Invoker.Handle<Exception>().Execute(() =>
            {
                FaceServiceClient.TrainPersonGroupAsync(personGroupId).Wait();
            });

            return trainResult.ExecutedWithoutExceptions;
        }

        public string GetTrainingStatus(string personGroupId)
        {
            Contractor.AssertAllNotNullOrEmpty(personGroupId);

            var statusResult = Invoker.Handle<Exception>().Execute(() =>
                FaceServiceClient.GetPersonGroupTrainingStatusAsync(personGroupId).Result);

            if (statusResult.ExecutedWithoutExceptions)
                return statusResult.TryGetResult<TrainingStatus>().ToJson();

            return statusResult.LastException.ToLong();
        }
    }
}