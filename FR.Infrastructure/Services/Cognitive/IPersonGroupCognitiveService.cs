﻿using Microsoft.ProjectOxford.Face.Contract;

namespace FR.Infrastructure.Services.Cognitive
{
    public interface IPersonGroupCognitiveService : ICognitiveService
    {
        PersonGroup GetPersonGroup(string personGroupId);

        bool CreatePersonGroup(string personGroupId, string name, string userData = null);

        bool Train(string personGroupId);

        string GetTrainingStatus(string personGroupId);
    }
}