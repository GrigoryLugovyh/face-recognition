﻿using System.Collections.Generic;
using System.Linq;
using Common.Services;
using FR.Infrastructure.Components;
using FR.Infrastructure.Exceptions;
using FR.Infrastructure.Services.Cognitive;
using FR.Repository;
using log4net;
using Microsoft.ProjectOxford.Face.Contract;
using MoreLinq;
using Repositories.Base;
using Repositories.FileStorage;
using FaceList = FR.Model.Models.FaceList;

namespace FR.Infrastructure.Services
{
    public class FaceListService : SettingService, IFaceListService
    {
        private const string FaceListLockContext = nameof(FaceListLockContext);
        private readonly IFaceListCognitiveService faceListCognitiveService;
        private readonly IFaceListRepository faceListRepository;
        private readonly IFaceRepository faceRepository;
        private readonly IFileStorageRepository fileStorageRepository;
        private readonly IGuidService guidService;
        private readonly IImageRepository imageRepository;
        private readonly ILockingContext lockingContext;

        private readonly ILog logger;
        private readonly IPersonRepository personRepository;

        public FaceListService(IGuidService guidService, ILockingContext lockingContext,
            IFaceListRepository faceListRepository,
            IFaceListCognitiveService faceListCognitiveService,
            IFaceRepository faceRepository, IFileStorageRepository fileStorageRepository,
            IImageRepository imageRepository, IPersonRepository personRepository)
        {
            this.guidService = guidService;
            this.faceListRepository = faceListRepository;
            this.faceListCognitiveService = faceListCognitiveService;
            this.faceRepository = faceRepository;
            this.fileStorageRepository = fileStorageRepository;
            this.imageRepository = imageRepository;
            this.personRepository = personRepository;
            this.lockingContext = lockingContext;

            logger = LogManager.GetLogger(GetType());
        }

        public FaceList GetOrAddActualFaceList()
        {
            FaceList actualFaceList = null;

            lockingContext.InLockingContext(FaceListLockContext, () =>
            {
                var faceList = faceListRepository.AsQueryable(false).Where(list => !list.IsFull)
                    .Select(list => new {list.Id}).FirstOrDefault();

                if (faceList == null)
                {
                    var newFaceList = new FaceList
                    {
                        Id = guidService.Generate(),
                        Name = guidService.FirtsBlock()
                    };

                    if (!faceListCognitiveService.CreateFaceList(newFaceList.Id, newFaceList.Name))
                        throw new FrException("failed to create face list");

                    faceListRepository.Write(newFaceList);

                    actualFaceList = newFaceList;
                }
                else
                {
                    actualFaceList = faceListRepository.Read(faceList.Id);
                }
            });

            return actualFaceList;
        }

        public bool AddFaceToFaceList(string faceId, IEnumerable<SimilarPersistedFace> similarPersistedFaces)
        {
            logger.Info($"Processing face \"{faceId}\" ...");

            var face = faceRepository.Read(faceId);

            var persistedFaces = similarPersistedFaces as SimilarPersistedFace[] ?? similarPersistedFaces.ToArray();

            if (persistedFaces.Any())
            {
                persistedFaces.ForEach(persistedFace =>
                {
                    personRepository.Upsert(persistedFace.PersistedFaceId.ToString(), faceId, persistedFace.Confidence);

                    logger.Info($"Face was tie to person: \"{faceId}\" -> \"{persistedFace.PersistedFaceId}\"!");
                });

                return true;
            }

            var imageStorageFile = imageRepository.AsQueryable(false).Where(image => image.Id == face.ImageId)
                .Select(image => new {image.StorageFilePath}).FirstOrDefault();

            if (imageStorageFile == null)
                throw new FrException(
                    $"There is no image with id \"{face.ImageId}\"");

            var faceImageEntity =
                fileStorageRepository.Read(FrFileStorageScopes.Faces, imageStorageFile.StorageFilePath);

            if (faceImageEntity == null)
                throw new FrException(
                    $"There is no file with path \"{imageStorageFile.StorageFilePath}\"");

            var faceList = GetOrAddActualFaceList();

            logger.Info($"Got actual face list \"{faceList.Id}\", adding face to face list ...");

            var addPersistedFaceResult = faceListCognitiveService.AddFaceToFaceList(faceImageEntity.Item2,
                faceList.Id,
                faceRectangle: new FaceRectangle
                {
                    Width = face.Rectangle.Width,
                    Top = face.Rectangle.Top,
                    Height = face.Rectangle.Height,
                    Left = face.Rectangle.Left
                });

            logger.Info("Face was added to face list");

            if (addPersistedFaceResult != null)
            {
                lockingContext.InLockingContext(faceList.Id, () =>
                {
                    faceList = faceListRepository.Read(faceList.Id);

                    if (faceList.IsFull)
                        faceList = GetOrAddActualFaceList();

                    faceList.FaceCount += 1;

                    faceListRepository.Write(faceList);
                });

                personRepository.Upsert(addPersistedFaceResult.PersistedFaceId.ToString(), faceId, 1.0d, faceList.Id);

                logger.Info(
                    $"Face was tie to person: face \"{faceId}\" -> persisted face \"{addPersistedFaceResult.PersistedFaceId}\" -> face list \"{faceList.Id}\"");
            }

            return true;
        }
    }
}