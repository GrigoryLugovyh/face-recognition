﻿using System.Threading.Tasks;
using FR.BasicReference;
using FR.Infrastructure.Exceptions;
using FR.Infrastructure.Services.Cognitive;
using FR.Repository;

namespace FR.Infrastructure.Services.Controller
{
    public class PersonGroupsControllerService : ControllerService, IPersonGroupsControllerService
    {
        private readonly IBasicReference basicReference;
        private readonly IPersonGroupCognitiveService personGroupCognitiveService;
        private readonly IPersonGroupRepository personGroupRepository;

        public PersonGroupsControllerService(IBasicReference basicReference,
            IPersonGroupRepository personGroupRepository, IPersonGroupCognitiveService personGroupCognitiveService)
        {
            this.basicReference = basicReference;
            this.personGroupRepository = personGroupRepository;
            this.personGroupCognitiveService = personGroupCognitiveService;
        }

        public async Task<bool> Init()
        {
            foreach (var personGroup in basicReference.PersonGroups)
            {
                await personGroupRepository.WriteAsync(personGroup);

                if (personGroupCognitiveService.GetPersonGroup(personGroup.Id) == null &&
                    !personGroupCognitiveService.CreatePersonGroup(personGroup.Id, personGroup.Name,
                        personGroup.UserData))
                    throw new FrException("Cannot create person group");
            }

            return true;
        }

        public async Task<bool> Train(string personGroupId)
        {
            var personGroup = await personGroupRepository.ReadAsync(personGroupId);

            if (personGroup == null)
                throw new FrException("Cannot find group");

            return personGroupCognitiveService.Train(personGroupId);
        }

        public async Task<string> GetTrainingStatus(string personGroupId)
        {
            var personGroup = await personGroupRepository.ReadAsync(personGroupId);

            if (personGroup == null)
                throw new FrException("Cannot find group");

            return personGroupCognitiveService.GetTrainingStatus(personGroupId);
        }
    }
}