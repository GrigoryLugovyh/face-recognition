﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FR.Model.Dto;

namespace FR.Infrastructure.Services.Controller
{
    public class AdvertisingControllerService : ClientContextControllerService, IAdvertisingControllerService
    {
        public Task<IEnumerable<AdvertisingRuleDto>> GetRules()
        {
            return Task.Run(() =>
            {
                return Enumerable.Empty<AdvertisingRuleDto>();
            });
        }

        public Task SetResults(IEnumerable<AdvertisingResultDto> results)
        {
            return Task.Run(() => { });
        }
    }
}