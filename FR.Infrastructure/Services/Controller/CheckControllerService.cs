﻿using FR.Model.Dto;
using FR.Model.TaskData.Cash;
using Tasks.Managing;
using Task = System.Threading.Tasks.Task;

namespace FR.Infrastructure.Services.Controller
{
    public class CheckControllerService : ClientContextControllerService, ICheckControllerService
    {
        private readonly ITaskManager taskManager;

        public CheckControllerService(ITaskManager taskManager)
        {
            this.taskManager = taskManager;
        }

        public Task SaveCheck(CheckDto check)
        {
            return Task.Run(() =>
            {
                taskManager.NewTask(new SaveCheckTaskData
                {
                    CheckDto = check,
                    SessionId = SessionId
                });
            });
        }
    }
}