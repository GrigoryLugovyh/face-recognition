﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FR.Model.Dto;
using FR.Model.Models;
using FR.Repository;

namespace FR.Infrastructure.Services.Controller
{
    public class EventsControllerService : ClientContextControllerService, IEventsControllerService
    {
        private readonly IEventRepository eventRepository;

        public EventsControllerService(IEventRepository eventRepository)
        {
            this.eventRepository = eventRepository;
        }

        public async Task SaveEvents(IEnumerable<EventDto> events)
        {
            foreach (var @event in events)
                await eventRepository.WriteAsync(new Event
                {
                    DeviceTimeStamp = DateTime.Parse(@event.DeviceTimeStamp),
                    DeviceId = ClientDeviceId,
                    SessionId = @event.SessionId,
                    Scope = @event.Scope,
                    Message = @event.Message,
                    Level = @event.Level
                });
        }
    }
}