﻿using System.Threading.Tasks;

namespace FR.Infrastructure.Services.Controller
{
    public interface IFaceControllerService : IControllerService
    {
        Task<bool> ProcessImage(byte[] faceImage);
    }
}