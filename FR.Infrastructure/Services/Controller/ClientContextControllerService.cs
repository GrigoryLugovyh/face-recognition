﻿using System;
using FR.Infrastructure.Components;

namespace FR.Infrastructure.Services.Controller
{
    public class ClientContextControllerService : ControllerService, IClientContext
    {
        public string SessionId { get; set; }

        public string ClientDeviceId { get; set; }

        public DateTime ClientDateTime { get; set; }
    }
}