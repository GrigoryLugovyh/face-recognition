﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FR.Model.Dto;

namespace FR.Infrastructure.Services.Controller
{
    public interface IEventsControllerService : IControllerService
    {
        Task SaveEvents(IEnumerable<EventDto> events);
    }
}