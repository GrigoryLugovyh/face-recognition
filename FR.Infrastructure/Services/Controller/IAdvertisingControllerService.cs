﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FR.Model.Dto;

namespace FR.Infrastructure.Services.Controller
{
    public interface IAdvertisingControllerService : IControllerService
    {
        Task<IEnumerable<AdvertisingRuleDto>> GetRules();

        Task SetResults(IEnumerable<AdvertisingResultDto> results);
    }
}