﻿using System.Threading.Tasks;

namespace FR.Infrastructure.Services.Controller
{
    public interface IPersonGroupsControllerService : IControllerService
    {
        Task<bool> Init();

        Task<bool> Train(string personGroupId);

        Task<string> GetTrainingStatus(string personGroupId);
    }
}