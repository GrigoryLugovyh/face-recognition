﻿using System.Threading.Tasks;
using FR.Model.Dto;

namespace FR.Infrastructure.Services.Controller
{
    public interface ICheckControllerService : IControllerService
    {
        Task SaveCheck(CheckDto check);
    }
}