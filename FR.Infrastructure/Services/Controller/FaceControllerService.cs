﻿using System.IO;
using System.Threading.Tasks;
using Common.Services;
using FR.Infrastructure.Components;
using FR.Model.Models;
using FR.Model.TaskData.Cognitive;
using FR.Repository;
using Repositories.FileStorage;
using Tasks.Managing;
using Task = System.Threading.Tasks.Task;

namespace FR.Infrastructure.Services.Controller
{
    public class FaceControllerService : ClientContextControllerService, IFaceControllerService
    {
        private readonly IFileStorageRepository fileStorageRepository;
        private readonly IGuidService guidService;
        private readonly IImageRepository imageRepository;
        private readonly ITaskManager taskManager;

        public FaceControllerService(IGuidService guidService, IFileStorageRepository fileStorageRepository,
            IImageRepository imageRepository, ITaskManager taskManager)
        {
            this.guidService = guidService;
            this.fileStorageRepository = fileStorageRepository;
            this.imageRepository = imageRepository;
            this.taskManager = taskManager;
        }

        public async Task<bool> ProcessImage(byte[] faceImage)
        {
            return await Task.Run(() =>
            {
                var fileName = guidService.Generate();

                using (var imageStream = new MemoryStream(faceImage))
                {
                    fileStorageRepository.Write(FrFileStorageScopes.Faces, imageStream, fileName);
                }

                var image = new Image
                {
                    Id = guidService.Generate(),
                    StorageFilePath = fileName,
                    DeviceId = ClientDeviceId,
                    SessionId = SessionId,
                    DeviceDateTimeStamp = ClientDateTime
                };

                imageRepository.Write(image);

                taskManager.NewTask(new DetectFaceTaskData
                {
                    ImageId = image.Id
                });

                return true;
            });
        }
    }
}