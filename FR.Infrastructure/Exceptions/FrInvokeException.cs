﻿using System;
using FR.Infrastructure.Environment;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrInvokeException : FrException
    {
        public FrInvokeException(string message, Exception inner, ProcessDescription processDescription)
            : base(message, inner)
        {
            ProcessDescription = processDescription;
        }

        public ProcessDescription ProcessDescription { get; }
    }
}