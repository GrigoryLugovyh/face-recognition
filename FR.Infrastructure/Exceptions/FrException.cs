﻿using System;
using System.Runtime.Serialization;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrException : Exception
    {
        public FrException()
        {
        }

        public FrException(string message) : base(message)
        {
        }

        public FrException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected FrException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}