﻿using System;
using FR.Infrastructure.Environment;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrInvokeUnknownResultException : FrInvokeException
    {
        public FrInvokeUnknownResultException(string message, Exception inner, ProcessDescription processDescription)
            : base(message, inner, processDescription)
        {
        }
    }
}