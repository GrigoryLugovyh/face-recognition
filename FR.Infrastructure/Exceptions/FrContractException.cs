﻿using System;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrContractException : FrException
    {
        public FrContractException()
        {
        }

        public FrContractException(string message) : base(message)
        {
        }
    }
}