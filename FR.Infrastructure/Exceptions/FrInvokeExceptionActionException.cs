﻿using System;
using FR.Infrastructure.Environment;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrInvokeExceptionActionException : FrInvokeException
    {
        public FrInvokeExceptionActionException(string message, Exception inner, ProcessDescription processDescription)
            : base(message, inner, processDescription)
        {
        }
    }
}