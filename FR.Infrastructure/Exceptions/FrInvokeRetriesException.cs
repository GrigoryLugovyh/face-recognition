﻿using System;
using FR.Infrastructure.Environment;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrInvokeRetriesException : FrInvokeException
    {
        public FrInvokeRetriesException(string message, Exception inner, ProcessDescription processDescription)
            : base(message, inner, processDescription)
        {
        }
    }
}