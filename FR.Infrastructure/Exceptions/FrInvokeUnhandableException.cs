﻿using System;
using FR.Infrastructure.Environment;

namespace FR.Infrastructure.Exceptions
{
    [Serializable]
    public class FrInvokeUnhandableException : FrInvokeException
    {
        public FrInvokeUnhandableException(string message, Exception inner, ProcessDescription processDescription)
            : base(message, inner, processDescription)
        {
        }
    }
}