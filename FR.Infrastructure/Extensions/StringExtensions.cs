﻿using System;
using System.Globalization;

namespace FR.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static string TweekNumberSeparator(this string value)
        {
            var numberDecimalSeparator = NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator;
            return value.Replace(",", numberDecimalSeparator).Replace(".", numberDecimalSeparator);
        }

        public static DateTime ToDateTime(this string value)
        {
            return DateTime.TryParse(value, out var result) ? result : DateTime.MinValue;
        }
    }
}