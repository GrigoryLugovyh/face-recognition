﻿using CommonModel.Schemes.Mongo;

namespace FR.Infrastructure.Components.Schemes
{
    public class FrMongoScheme : MongoScheme
    {
        public override int Revision => 0;

        public override int ProductVersion => 1;

        public override string DatabaseName => "fr";
    }
}