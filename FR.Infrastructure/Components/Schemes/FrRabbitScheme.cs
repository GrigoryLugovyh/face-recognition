﻿using System.Linq;
using CommonModel.Schemes.Rabbit;
using FR.Model;

namespace FR.Infrastructure.Components.Schemes
{
    public class FrRabbitScheme : RabbitScheme
    {
        public FrRabbitScheme()
        {
            AddSimpleServiceToScheme(Service.Api);
            AddSimpleServiceToScheme(Service.Cash);
            AddSimpleServiceToScheme(Service.Cognitive);
            AddSimpleServiceToScheme(Service.Advertising);
        }

        private void AddSimpleServiceToScheme(string serviceName)
        {
            if (Queues.Any(queue => queue.Name == serviceName)) return;

            Queues.Add(RegisterSimpleQueue(serviceName));
            ServiceMappings.Add(serviceName, RegisterSimpleMapping(serviceName));
        }
    }
}