﻿using CommonModel.Attributes;
using MongoPrimitives.IndexesCreator;

namespace FR.Infrastructure.Components.Registry
{
    [SingleInstance]
    public class FrCustomIndexRegistry : CustomIndexRegistry
    {
    }
}