﻿using System;

namespace FR.Infrastructure.Components
{
    public interface IClientContext
    {
        string SessionId { set; get; }

        string ClientDeviceId { get; set; }

        DateTime ClientDateTime { get; set; }
    }
}