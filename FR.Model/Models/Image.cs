using System;
using CommonModel.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace FR.Model.Models
{
    [Collection("images")]
    public class Image : FrEntity
    {
        public string[] FaceIds { get; set; }

        public bool Recognized { get; set; }

        [Required]
        public string StorageFilePath { get; set; }

        [Required]
        public string DeviceId { get; set; }

        [Required]
        public string SessionId { get; set; }

        [Required]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime DeviceDateTimeStamp { get; set; }
    }
}