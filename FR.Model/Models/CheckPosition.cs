﻿using System;
using CommonModel.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace FR.Model.Models
{
    [Collection("checkPositions")]
    public class CheckPosition : FrEntity
    {
        [Required]
        public string CheckId { get; set; }

        [Required]
        public string ItemId { get; set; }

        public double Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal TotalPrice { get; set; }

        public bool Change { get; set; }

        public bool Delete { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime AddTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime AddTimeStampUtc { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ChangeTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime ChangeTimeStampUtc { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DeleteTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime DeleteTimeStampUtc { get; set; }
    }
}