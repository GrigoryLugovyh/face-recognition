using CommonModel.Attributes;

namespace FR.Model.Models
{
    [Collection("faces")]
    public class Face : FrEntity
    {
        public string ImageId { get; set; }

        public double Age { get; set; }

        public string Gender { get; set; }

        public FaceRectangle Rectangle { get; set; } = new FaceRectangle();

        public class FaceRectangle
        {
            public int Width { get; set; }
            public int Height { get; set; }
            public int Left { get; set; }
            public int Top { get; set; }
        }
    }
}