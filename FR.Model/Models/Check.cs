﻿using System;
using CommonModel.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace FR.Model.Models
{
    [Collection("checks")]
    public class Check : FrEntity
    {
        [Required]
        public string SessionId { get; set; }

        public string CashierName { get; set; }

        public string[] DiscountCards { get; set; }

        public string[] BonusCards { get; set; }

        public decimal TotalSum { get; set; }

        public decimal TotalDiscount { get; set; }

        public decimal ActivePositionsSum { get; set; }

        public decimal DeletedPositionsSum { get; set; }

        public double DurationSeconds { get; set; }

        public string[] Payments { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime OpenTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime OpenTimeStampUtc { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CloseTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CloseTimeStampUtc { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime SendTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime SendTimeStampUtc { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ReciveTimeStamp { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime ReciveTimeStampUtc { get; set; }
    }
}