﻿using CommonModel.Attributes;

namespace FR.Model.Models
{
    [Collection("items")]
    public class Item : FrEntity
    {
        [Required]
        public string Name { get; set; }

        public string Code { get; set; }

        [Required]
        public string Barcode { get; set; }
    }
}