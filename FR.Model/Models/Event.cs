using System;
using CommonModel.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace FR.Model.Models
{
    [Collection("events")]
    public class Event : FrEntity
    {
        [Required]
        public string DeviceId { get; set; }

        public string SessionId { get; set; }

        public string Scope { get; set; }

        [Required]
        public string Level { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime DeviceTimeStamp { get; set; }
    }
}