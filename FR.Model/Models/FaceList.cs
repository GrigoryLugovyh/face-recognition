using CommonModel.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace FR.Model.Models
{
    [Collection("faceLists")]
    public class FaceList : FrEntity
    {
        public string Name { get; set; }

        public int FaceCount { get; set; }

        [BsonElement]
        public bool IsFull => FaceCount >= 1000;
    }
}