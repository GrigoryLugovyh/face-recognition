using CommonModel.Attributes;

namespace FR.Model.Models
{
    [Collection("personGroups")]
    public class PersonGroup : FrEntity
    {
        public string Name { get; set; }

        public string UserData { get; set; }
    }
}