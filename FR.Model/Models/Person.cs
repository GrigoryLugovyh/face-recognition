using System.Collections.Generic;
using CommonModel.Attributes;

namespace FR.Model.Models
{
    [Collection("persons")]
    public class Person : FrEntity
    {
        public string FaceListId { get; set; }

        public string PersistedFaceId { get; set; }

        public List<FaceConfidence> Faces { get; set; }

        public class FaceConfidence
        {
            public string FaceId { get; set; }

            public double Confidence { get; set; }

            public static FaceConfidence Create(string faceId, double confidence)
            {
                return new FaceConfidence
                {
                    FaceId = faceId,
                    Confidence = confidence
                };
            }
        }
    }
}