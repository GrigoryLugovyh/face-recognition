﻿using CommonModel.Attributes;
using CommonModel.Model.TaskDatas;

namespace FR.Model.TaskData.Advertising
{
    [Intent(Service.Advertising)]
    public class PrepareAdvertisingTaskData : TaskDataBase
    {
        public string FaceId { get; set; }

        public string SessionId { get; set; }
    }
}