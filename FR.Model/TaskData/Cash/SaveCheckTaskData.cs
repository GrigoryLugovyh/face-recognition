﻿using CommonModel.Attributes;
using CommonModel.Model.TaskDatas;
using FR.Model.Dto;

namespace FR.Model.TaskData.Cash
{
    [Intent(Service.Cash)]
    public class SaveCheckTaskData : TaskDataBase
    {
        public string SessionId { get; set; }

        public CheckDto CheckDto { get; set; }
    }
}