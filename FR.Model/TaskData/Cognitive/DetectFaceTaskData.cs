using CommonModel.Attributes;
using CommonModel.Model.TaskDatas;

namespace FR.Model.TaskData.Cognitive
{
    [Intent(Service.Cognitive)]
    public class DetectFaceTaskData : TaskDataBase
    {
        public string ImageId { get; set; }
    }
}