namespace FR.Model
{
    public abstract class Service : CommonModel.Model.Service
    {
        public const string Api = "fr_api";
        public const string Cash = "fr_cash";
        public const string Cognitive = "fr_cognitive";
        public const string Advertising = "fr_advertising";
    }
}