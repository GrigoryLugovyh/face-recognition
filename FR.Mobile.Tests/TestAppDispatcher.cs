﻿using FR.Mobile.App;
using FR.Mobile.Listener;
using FR.Mobile.Listener.Prisma;

namespace FR.Mobile.Tests
{
    public class TestAppDispatcher : AppDispatcher
    {
        protected override IManage GetExternalEventDispatcher()
        {
            Container.AddRegistration(new TestExternalEventDistributor());

            return new ExternalEventDispatcher<TestExternalEventDistributor, PrismaEventInterpretator>();
        }
    }
}