using FR.Mobile.App;

namespace FR.Mobile.Tests
{
    public class MobileTestBase
    {
        protected IAppDispatcher App { get; private set; }

        protected IAppModel Model { get; private set; }

        protected void Run()
        {
            AppModel.Instance<TestModel>();

            Model = Container.Resolve<IAppModel>();
            App = Container.Resolve<IAppDispatcher>();

            App.Start();
        }
    }
}