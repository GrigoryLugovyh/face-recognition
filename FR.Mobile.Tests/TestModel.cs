﻿using FR.Mobile.App;
using FR.Mobile.Services;

namespace FR.Mobile.Tests
{
    public class TestModel : AppModel
    {
        protected override IAppDispatcher CreateAppDispatcher()
        {
            return new TestAppDispatcher();
        }

        protected override IEventService CreateLoggerService()
        {
            return new TestEventService();
        }

        protected override void InitAppSettings()
        {
            Container.AddRegistration<IAppSettings, TestAppSettings>().AsMultiInstance();
        }
    }
}