﻿using FR.Mobile.App;
using NUnit.Framework;

namespace FR.Mobile.Tests.Mobile
{
    public class ModelTests
    {
        [Test]
        public void TestTheOnlyModel()
        {
            var model1 = AppModel.Instance<TestModel>();
            var model2 = AppModel.Instance<TestModel>();
            var model3 = Container.Resolve<IAppModel>();

            Assert.IsTrue(ReferenceEquals(model1, model2));
            Assert.IsTrue(ReferenceEquals(model1, model3));
        }
    }
}