﻿using System;
using MoreLinq;
using NUnit.Framework;
using PrismaEmulator.Core.Commands;

namespace FR.Mobile.Tests.Mobile
{
    public class ExampleTests : MobileTestBase
    {
        [Test]
        [Explicit]
        public void TestCommon()
        {
            Run();

            var commands = new[]
            {
                typeof(OpenCheckCommand),
                typeof(AddPositionCommand),
                typeof(AddPositionCommand),
                typeof(AddPositionCommand),
                typeof(SubtotalCommand),
                typeof(CloseCheckCommand)
            };

            var externalEventDistributor = Container.Resolve<TestExternalEventDistributor>();

            commands.ForEach(type =>
            {
                externalEventDistributor.RaiseRawExternalEvent(((Command) Activator.CreateInstance(type)).Create());
            });
        }
    }
}