﻿using FR.Mobile.App;

namespace FR.Mobile.Tests
{
    public class TestAppSettings : AppSettings
    {
        public override string DeviveId { get; } = "00000000-0000-0000-0000-000000000000";

        public override int ImageQuality { get; } = 100;

        public override string ServerAddress { get; } = "http://localhost:8596";

        public override bool CameraPreviewEnable { get; } = false;

        public override bool LoggingEnable { get; } = false;
    }
}