﻿using System;
using FR.Mobile.Listener;

namespace FR.Mobile.Tests
{
    public class TestExternalEventDistributor : IExternalEventDistributor
    {
        public event EventHandler<byte[]> RawExternalEvent;

        public bool CanInit => true;

        public void Init()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Release()
        {
        }

        public void RaiseRawExternalEvent(byte[] data)
        {
            RawExternalEvent?.Invoke(this, data);
        }
    }
}