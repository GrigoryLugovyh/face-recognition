﻿using System;
using System.Collections.Generic;
using CommonModel.Services;
using FR.Model;

namespace FR.AdvertisingService
{
    public class ServiceInfoProvider : IServiceInfoProvider
    {
        public string GetCurrentService()
        {
            return Service.Advertising;
        }

        public Dictionary<Type, string> TaskDataToServiceMap => new Dictionary<Type, string>();
    }
}