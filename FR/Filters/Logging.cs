﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using CommonModel.IoCContainer;
using FR.Services;

namespace FR.Filters
{
    public class Logging : FrFilterAttribute
    {
        private readonly IApiLoggingService apiLoggingService;

        public Logging()
        {
            apiLoggingService = Container.Resolve<IApiLoggingService>();
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var actionDescriptor = new ApiLoggingService.ActionDescriptor
            {
                RequestHash = actionContext.Request.GetHashCode(),
                ActionName = actionContext.ActionDescriptor.ActionName,
                ControllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName,
                Uri = actionContext.Request.RequestUri,
                Method = actionContext.Request.Method.Method,
                DeviceId = GetHeaderValue(actionContext.Request.Headers, DeviceIdHeaderName),
                SessionId = GetHeaderValue(actionContext.Request.Headers, SessionIdHeaderName)
            };

            actionContext.ActionDescriptor.Properties.AddOrUpdate(ApiLoggingService.LogDescriptorKey, actionDescriptor,
                (o, o1) => actionDescriptor);

            apiLoggingService.LogExecuting(actionDescriptor);

            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            apiLoggingService.LogExecuted(actionExecutedContext.Exception,
                (ApiLoggingService.ActionDescriptor)
                actionExecutedContext.ActionContext.ActionDescriptor.Properties.GetOrAdd(
                    ApiLoggingService.LogDescriptorKey, new ApiLoggingService.ActionDescriptor()),
                actionExecutedContext.Response?.StatusCode);

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}