﻿using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace FR.Filters
{
    public class FrFilterAttribute : ActionFilterAttribute
    {
        protected const string DeviceIdHeaderName = "device-id";
        protected const string SessionIdHeaderName = "session-id";
        protected const string ClientDatetimeHeaderName = "client-datetime";

        protected string GetHeaderValue(HttpHeaders headers, string headerName)
        {
            headers.TryGetValues(headerName, out var headerValues);
            return headerValues?.FirstOrDefault();
        }
    }
}