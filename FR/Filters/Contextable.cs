﻿using System;
using System.Web;
using System.Web.Http.Controllers;
using FR.Controllers;
using FR.Infrastructure.Components;

namespace FR.Filters
{
    public class Contextable : FrFilterAttribute, IClientContext
    {
        public string SessionId { get; set; }

        public string ClientDeviceId { get; set; }

        public DateTime ClientDateTime { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!(actionContext?.ControllerContext?.Controller is IClientContextController clientContextController)
            ) return;

            ClientDeviceId = GetHeaderValue(actionContext.Request.Headers, DeviceIdHeaderName);

            if (string.IsNullOrEmpty(ClientDeviceId))
                throw new HttpException(400, $"\"{DeviceIdHeaderName}\" header is required");

            SessionId = GetHeaderValue(actionContext.Request.Headers, SessionIdHeaderName);

            var clientDatetimeHeader = GetHeaderValue(actionContext.Request.Headers, ClientDatetimeHeaderName);
            DateTime.TryParse(clientDatetimeHeader, out var clientDateTime);
            ClientDateTime = clientDateTime;

            if (ClientDateTime == null || ClientDateTime == DateTime.MinValue)
                throw new HttpException(400, $"\"{ClientDatetimeHeaderName}\" header is required");

            clientContextController.SetClientContext(this);
        }
    }
}