﻿using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace FR.Filters
{
    public class Authorizable : ActionFilterAttribute
    {
        private const string AuthKeyHeaderName = "auth-key";

        private const string AuthKeyHeaderValue = "3e961540-7746-4acf-85ac-db3b6caa94ee";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues(AuthKeyHeaderName, out var authKeyHeaders);

            var keyHeaders = authKeyHeaders as string[] ?? authKeyHeaders?.ToArray();

            if (keyHeaders == null || !keyHeaders.Any())
                throw new HttpException(401, "Auth is required for this action");

            if (keyHeaders.All(value => value != AuthKeyHeaderValue))
                throw new HttpException(401, $"\"{AuthKeyHeaderName}\" is invalid");
        }
    }
}