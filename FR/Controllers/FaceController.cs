﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FR.Filters;
using FR.Infrastructure.Services.Controller;

namespace FR.Controllers
{
    [RoutePrefix("faces")]
    public class FaceController : ClientContextController<IFaceControllerService>
    {
        [HttpPost]
        [Route("")]
        [Contextable]
        public async Task<bool> UploadImage()
        {
            // ReSharper disable once PossibleNullReferenceException
            return await Service.ProcessImage(await (await Request.Content.ReadAsMultipartAsync()).Contents
                .FirstOrDefault()?.ReadAsByteArrayAsync());
        }
    }
}