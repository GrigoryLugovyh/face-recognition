﻿using System;
using FR.Infrastructure.Components;
using FR.Infrastructure.Services.Controller;
using Omu.ValueInjecter;

namespace FR.Controllers
{
    public abstract class ClientContextController<TService> : BaseConstroller<TService>, IClientContext,
        IClientContextController
        where TService : class, IControllerService
    {
        public string SessionId { get; set; }

        public string ClientDeviceId { get; set; }

        public DateTime ClientDateTime { get; set; }

        public void SetClientContext(IClientContext clientContext)
        {
            this.InjectFrom(clientContext);
            Service.InjectFrom(this);
        }
    }
}