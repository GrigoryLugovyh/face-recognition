﻿using System.Threading.Tasks;
using System.Web.Http;
using FR.Filters;
using FR.Infrastructure.Services.Controller;

namespace FR.Controllers
{
    [RoutePrefix("personGroups")]
    public class PersonGroupsController : BaseConstroller<IPersonGroupsControllerService>
    {
        [HttpPost]
        [Route("init")]
        [Authorizable]
        public async Task<bool> Init()
        {
            return await Service.Init();
        }

        [HttpPost]
        [Route("{personGroupId}/train")]
        [Authorizable]
        public async Task<bool> Train(string personGroupId)
        {
            return await Service.Train(personGroupId);
        }

        [HttpGet]
        [Route("{personGroupId}/trainingStatus")]
        [Authorizable]
        public async Task<string> TrainingStatus(string personGroupId)
        {
            return await Service.GetTrainingStatus(personGroupId);
        }
    }
}