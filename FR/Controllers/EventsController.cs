﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using FR.Filters;
using FR.Infrastructure.Services.Controller;
using FR.Model.Dto;

namespace FR.Controllers
{
    [RoutePrefix("events")]
    public class EventsController : ClientContextController<IEventsControllerService>
    {
        [HttpPost]
        [Route("")]
        [Contextable]
        public async Task Post([FromBody] IEnumerable<EventDto> events)
        {
            await Service.SaveEvents(events);
        }
    }
}