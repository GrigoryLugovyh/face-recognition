﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using FR.Filters;
using FR.Infrastructure.Services.Controller;
using FR.Model.Dto;

namespace FR.Controllers
{
    [RoutePrefix("advertising")]
    public class AdvertisingController : ClientContextController<IAdvertisingControllerService>
    {
        [HttpGet]
        [Route("")]
        [Contextable]
        public async Task<IEnumerable<AdvertisingRuleDto>> GetRules()
        {
            return await Service.GetRules();
        }

        [HttpPost]
        [Route("")]
        [Contextable]
        public async Task SetResults([FromBody] IEnumerable<AdvertisingResultDto> results)
        {
            await Service.SetResults(results);
        }
    }
}