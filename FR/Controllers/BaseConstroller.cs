﻿using System.Web.Http;
using CommonModel.IoCContainer;
using FR.Infrastructure.Services.Controller;

namespace FR.Controllers
{
    public abstract class BaseConstroller<TService> : ApiController where TService : class, IControllerService
    {
        private TService service;

        protected virtual TService Service => service ?? (service = Container.Resolve<TService>());
    }
}