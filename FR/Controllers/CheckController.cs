﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using FR.Filters;
using FR.Infrastructure.Services.Controller;
using FR.Model.Dto;

namespace FR.Controllers
{
    [RoutePrefix("checks")]
    public class CheckController : ClientContextController<ICheckControllerService>
    {
        [HttpPost]
        [Route("")]
        [Contextable]
        public async Task SaveCheck([FromBody] CheckDto check)
        {
            if (check?.SendTimeStamp == null || check.Positions == null)
                throw new HttpException(400, "Cash check is invalid");

            await Service.SaveCheck(check);
        }
    }
}