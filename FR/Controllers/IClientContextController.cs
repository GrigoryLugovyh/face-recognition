﻿using FR.Infrastructure.Components;

namespace FR.Controllers
{
    public interface IClientContextController
    {
        void SetClientContext(IClientContext clientContext);
    }
}