﻿using System;
using System.Net;

namespace FR.Services
{
    public interface IApiLoggingService
    {
        void LogExecuting(ApiLoggingService.ActionDescriptor actionDescriptor);

        void LogExecuted(Exception exception, ApiLoggingService.ActionDescriptor actionDescriptor,
            HttpStatusCode? statusCode);
    }
}