﻿using System;
using System.Diagnostics;
using System.Net;
using CommonModel.Extensions;
using CommonModel.Logging;
using log4net;

namespace FR.Services
{
    public class ApiLoggingService : IApiLoggingService
    {
        public const string LogDescriptorKey = "ApiLoggingActionDescriptor";

        private static readonly ILog logger = LogManager.GetLogger("FR.API");
        private readonly ILoggerConfigurator loggerConfigurator;

        public ApiLoggingService(ILoggerConfigurator loggerConfigurator)
        {
            this.loggerConfigurator = loggerConfigurator;
        }

        public void LogExecuting(ActionDescriptor actionDescriptor)
        {
            loggerConfigurator.Configure();

            var contextInfo =
                $"\r\n\t\t\t\t\t\t  DeviceId: {actionDescriptor?.DeviceId.ToStringSafe()}; SessionId: {actionDescriptor?.SessionId?.ToStringSafe()}";
            logger.Info(
                $">>> {actionDescriptor?.RequestHash} {actionDescriptor?.Method} {actionDescriptor?.ControllerName}.{actionDescriptor?.ActionName} {actionDescriptor?.Uri}{contextInfo}");
        }

        public void LogExecuted(Exception exception, ActionDescriptor actionDescriptor, HttpStatusCode? statusCode)
        {
            if (exception != null)
            {
                logger.ErrorFormat($"!!! {actionDescriptor.RequestHash} {exception.ToLong()}");
                return;
            }

            var elapsedMilliseconds = actionDescriptor.Stopwatch.ElapsedMilliseconds;

            logger.Info(
                $"<<< {actionDescriptor.RequestHash} {elapsedMilliseconds}ms {statusCode} {actionDescriptor.ControllerName}.{actionDescriptor.ActionName}");
        }

        public class ActionDescriptor
        {
            public ActionDescriptor()
            {
                Stopwatch.Start();
            }

            public int RequestHash { get; set; }

            public Stopwatch Stopwatch { get; } = new Stopwatch();

            public string ActionName { get; set; }

            public string ControllerName { get; set; }

            public string Method { get; set; }

            public Uri Uri { get; set; }

            public string DeviceId { get; set; }

            public string SessionId { get; set; }
        }
    }
}