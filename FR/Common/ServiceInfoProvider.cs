﻿using System;
using System.Collections.Generic;
using CommonModel.Services;
using FR.Model;

namespace FR.Common
{
    public class ServiceInfoProvider : IServiceInfoProvider
    {
        public string GetCurrentService()
        {
            return Service.Api;
        }

        public Dictionary<Type, string> TaskDataToServiceMap => new Dictionary<Type, string>();
    }
}