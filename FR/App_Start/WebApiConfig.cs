﻿using System.Web.Http;
using FR.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace FR
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            RegisterFormatters(config);
            RegisterRouting(config);
            RegisterFilters(config);
        }

        private static void RegisterRouting(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new {id = RouteParameter.Optional})
                .Constraints["Namespaces"] = "FR.Controllers";
        }

        private static void RegisterFormatters(HttpConfiguration config)
        {
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            // ReSharper disable once UseObjectOrCollectionInitializer
            var jsonFormatterSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            config.Formatters.JsonFormatter.SerializerSettings = jsonFormatterSettings;
        }

        private static void RegisterFilters(HttpConfiguration config)
        {
            config.Filters.Add(new Logging());
        }
    }
}